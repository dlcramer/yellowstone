<?php
/**
 * Loads the plugin if dependencies are met.
 *
 * @package   {{Core_Class}}
 * @author    {{core_author_name}}
 * @license   GPL-2.0+
 * @link
 * @copyright 2015 {{core_author_name}} & CalderaWP LLC
 */


if ( file_exists( {{CORE_PREFIX}}_PATH . 'vendor/autoload.php' ) ){
	//autoload dependencies
	require_once( {{CORE_PREFIX}}_PATH . 'vendor/autoload.php' );

	// initialize plugin
	\calderawp\{{core_prefix}}\core::get_instance();

}else{
	return new WP_Error( '{{core-slug}}--no-dependencies', __( '{{Dependencies for {{Core_Name}} could not be found.', '{{core-slug}}' ) );
}


