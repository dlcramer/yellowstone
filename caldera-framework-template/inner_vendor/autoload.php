<?php

// autoload.php @generated by Composer

require_once __DIR__ . '/composer' . '/autoload_real.php';

return {{Core_Class}}_Autoloader::getLoader();
