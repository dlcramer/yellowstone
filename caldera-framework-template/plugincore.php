<?php
/**
 * @package   {{Core_Class}}
 * @author    {{core_author_name}}
 * @license   GPL-2.0+
 * @link      
 * @copyright 2015 {{core_author_name}} & CalderaWP LLC
 *
 * @wordpress-plugin
 * Plugin Name: {{Core_Name}}
 * Plugin URI:  http://CalderaWP.com
 * Description: {{Core_Description}}
 * Version:     {{core_version}}
 * Author:      {{core_author_name}}
 * Author URI:  {{core_author_uri}}
 * Text Domain: {{core-slug}}
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path: /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define('{{CORE_PREFIX}}_PATH',  plugin_dir_path( __FILE__ ) );
define('{{CORE_PREFIX}}_CORE',  __FILE__ );
define('{{CORE_PREFIX}}_URL',  plugin_dir_url( __FILE__ ) );
define('{{CORE_PREFIX}}_VER',  '{{core_version}}' );



// Load instance
add_action( 'plugins_loaded', '{{core_prefix}}_bootstrap' );
function {{core_prefix}}_bootstrap(){

	if ( is_admin() || defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		include_once {{CORE_PREFIX}}_PATH . 'vendor/calderawp/dismissible-notice/src/functions.php';
	}


	if ( ! version_compare( PHP_VERSION, '5.3.0', '>=' ) ) {
		if ( is_admin() ) {
			
			//BIG nope nope nope!
			$message = __( sprintf( '{{Core_Name}} requires PHP version %1s or later. We strongly recommend PHP 5.5 or later for security and performance reasons. Current version is %2s.', '5.3.0', PHP_VERSION ), '{{core-slug}}' );
			echo caldera_warnings_dismissible_notice( $message, true, 'activate_plugins' );
		}

	}else{
		//bootstrap plugin
		require_once( {{CORE_PREFIX}}_PATH . 'bootstrap.php' );

	}

}
