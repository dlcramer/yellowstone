<?php
/**
 * Main admin interface for selecting items to edit/ creating or deleting items.
 *
 * @package   {{Core_Class}}
 * @author    {{core_author_name}}
 * @license   GPL-2.0+
 * @link
 * @copyright 2015 {{core_author_name}}
 */
?>

<div class="wrap {{core-slug}}-{{core_theme_header}}admin-wrap" id="{{core-slug}}-admin--wrap">
	<div class="{{core-slug}}-main-header{{core_theme_header}}">
		<h1>
			<?php _e( '{{Core_Name}}', '{{core-slug}}' ); ?>
			<span class="{{core-slug}}-version">
				<?php echo {{CORE_PREFIX}}_VER; ?>
			</span>
			<span class="add-new-h2 wp-baldrick" data-modal="new-{{core_item}}" data-modal-height="192" data-modal-width="402" data-modal-buttons='<?php _e( 'Create {{Core_Item}}', '{{core-slug}}' ); ?>|{"data-action":"{{core_prefix}}_create_{{core_item}}","data-before":"{{core_prefix}}_create_new_{{core_item}}", "data-callback": "bds_redirect_to_{{core_item}}"}' data-modal-title="<?php _e('New {{Core_Item}}', '{{core-slug}}') ; ?>" data-request="#new-{{core_item}}-form">
				<?php _e('Add New', '{{core-slug}}') ; ?>
			</span>
			<span class="{{core-slug}}-nav-separator"></span>
			<span class="add-new-h2 wp-baldrick" data-modal="import-{{core_slug}}" data-modal-height="auto" data-modal-width="380" data-modal-buttons='<?php _e( 'Import {{Core_Item}}', '{{core-slug}}' ); ?>|{"id":"{{core_prefix}}_import_init", "data-action":"{{core_prefix}}_create_{{core_slug}}","data-before":"{{core_prefix}}_create_new_{{core_slug}}", "data-callback": "bds_redirect_to_{{core_slug}}"}' data-modal-title="<?php _e('Import {{Core_Item}}', '{{core-slug}}') ; ?>" data-request="{{core_prefix}}_start_importer" data-template="#import-{{core_slug}}-form">
				<?php _e('Import', '{{core-slug}}') ; ?>
			</span>
		</h1>
	</div>

<?php

	${{core_item}}s = \calderawp\{{core_prefix}}\options::get_registry();
	if( empty( ${{core_item}}s ) ){
		${{core_item}}s = array();
	}

	global $wpdb;
	
	foreach( ${{core_item}}s as ${{core_item}}_id => ${{core_item}} ){

?>

	<div class="{{core-slug}}-card-item" id="{{core_item}}-<?php echo ${{core_item}}[ 'id' ]; ?>">
		<span class="dashicons {{core_icon}} {{core-slug}}-card-icon"></span>
		<div class="{{core-slug}}-card-content">
			<h4>
				<?php echo ${{core_item}}[ 'name' ]; ?>
			</h4>
			<div class="description">
				<?php echo ${{core_item}}[ 'slug' ]; ?>
			</div>
			<div class="description">&nbsp;</div>
			<div class="{{core-slug}}-card-actions">
				<div class="row-actions">
					<span class="edit">
						<a href="?page={{core_slug}}&amp;download=<?php echo ${{core_item}}[ 'id' ]; ?>&{{core-slug}}-export=<?php echo wp_create_nonce( '{{core-slug}}' ); ?>" target="_blank"><?php _e('Export', '{{core-slug}}'); ?></a> |
					</span>
					<span class="edit">
						<a href="?page={{core_slug}}&amp;edit=<?php echo ${{core_item}}[ 'id' ]; ?>"><?php _e('Edit', '{{core-slug}}'); ?></a> |
					</span>
					<span class="trash confirm">
						<a href="?page={{core_slug}}&amp;delete=<?php echo ${{core_item}}[ 'id' ]; ?>" data-block="<?php echo ${{core_item}}[ 'id' ]; ?>" class="submitdelete">
							<?php _e('Delete', '{{core-slug}}'); ?>
						</a>
					</span>
				</div>
				<div class="row-actions" style="display:none;">
					<span class="trash">
						<a class="wp-baldrick" style="cursor:pointer;" data-action="{{core_prefix}}_delete_{{core_item}}" data-callback="{{core_prefix}}_remove_deleted" data-block="<?php echo ${{core_item}}['id']; ?>" class="submitdelete"><?php _e('Confirm Delete', '{{core-slug}}'); ?></a> | </span>
					<span class="edit confirm">
						<a href="?page={{core_slug}}&amp;edit=<?php echo ${{core_item}}['id']; ?>">
							<?php _e('Cancel', '{{core-slug}}'); ?>
						</a>
					</span>
				</div>
			</div>
		</div>
	</div>

	<?php } ?>

</div>
<div class="clear"></div>
<script type="text/javascript">
	
	function {{core_prefix}}_create_new_{{core_item}}(el){
		var {{core_item}} 	= jQuery(el),
			name 	= jQuery("#new-{{core_item}}-name"),
			slug 	= jQuery('#new-{{core_item}}-slug')
			imp 	= jQuery('#new-{{core_item}}-import'); 

		if( imp.length ){
			if( !imp.val().length ){
				return false;
			}
			{{core_item}}.data('import', imp.val() );
			return true;
		}

		if( slug.val().length === 0 ){
			name.focus();
			return false;
		}
		if( slug.val().length === 0 ){
			slug.focus();
			return false;
		}

		{{core_item}}.data('name', name.val() ).data('slug', slug.val() );

	}

	function bds_redirect_to_{{core_item}}(obj){
		
		if( obj.data.success ){

			obj.params.trigger.prop('disabled', true).html('<?php _e('Loading {{Core_Item}}', '{{core-slug}}'); ?>');
			window.location = '?page={{core_slug}}&edit=' + obj.data.data.id;

		}else{

			jQuery('#new-block-slug').focus().select();
			
		}
	}
	function {{core_prefix}}_remove_deleted(obj){

		if( obj.data.success ){
			jQuery( '#{{core_item}}-' + obj.data.data.block ).fadeOut(function(){
				jQuery(this).remove();
			});
		}else{
			alert('<?php echo __('Sorry, something went wrong. Try again.', '{{core-slug}}'); ?>');
		}
	}
	function {{core_prefix}}_start_importer(){
		return {};
	}
</script>
<script type="text/html" id="new-{{core_item}}-form">
	<div class="{{core-slug}}-config-group">
		<label>
			<?php _e('{{Core_Item}} Name', '{{core-slug}}'); ?>
		</label>
		<input type="text" name="name" id="new-{{core_item}}-name" data-sync="#new-{{core_item}}-slug" autocomplete="off">
	</div>
	<div class="{{core-slug}}-config-group">
		<label>
			<?php _e('{{Core_Item}} Slug', '{{core-slug}}'); ?>
		</label>
		<input type="text" name="slug" id="new-{{core_item}}-slug" data-format="slug" autocomplete="off">
	</div>

</script>
<script type="text/html" id="import-{{core_slug}}-form">
	<div class="import-tester-config-group">
		<input id="new-{{core-slug}}-import-file" type="file" class="regular-text">
		<input id="new-{{core_item}}-import" value="" name="import" type="hidden">
	</div>
	{{#script}}
		jQuery( function($){

			$('#{{core_prefix}}_import_init').prop('disabled', true).addClass('disabled');

			$('#new-{{core-slug}}-import-file').on('change', function(){
				$('#{{core_prefix}}_import_init').prop('disabled', true).addClass('disabled');
				var input = $(this),
					f = this.files[0],
				contents;

				if (f) {
					var r = new FileReader();
					r.onload = function(e) { 
						contents = e.target.result;
						var data;
						 try{ 
						 	data = JSON.parse( contents );
						 } catch(e){};
						 
						 if( !data || ! data['{{core-slug}}-setup'] ){
						 	alert("<?php echo esc_attr( __('Not a valid {{Core_Item}} export file.', '{{core-slug}}') ); ?>");
						 	input[0].value = null;
							return false;
						 }

						$('#new-{{core_item}}-import').val( contents );
						$('#{{core_prefix}}_import_init').prop('disabled', false).removeClass('disabled');
					}
					r.readAsText(f);
				} else { 
					alert("Failed to load file");
					return false;
				}
			});

		});
	{{/script}}
</script>
