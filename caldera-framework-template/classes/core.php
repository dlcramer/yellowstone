<?php
/**
 * {{Core_Name}}.
 *
 * @package   {{Core_Class}}
 * @author    {{core_author_name}}
 * @license   GPL-2.0+
 * @link      
 * @copyright 2015 {{core_author_name}}
 */
namespace calderawp\{{core_prefix}};

/**
 * Main plugin class.
 *
 * @package {{Core_Class}}
 * @author  {{core_author_name}}
 */
class core {

	/**
	 * The slug for this plugin
	 *
	 * @since {{core_version}}
	 *
	 * @var      string
	 */
	protected $plugin_slug = '{{core-slug}}';

	/**
	 * Holds class instance
	 *
	 * @since {{core_version}}
	 *
	 * @var      object|\calderawp\{{core_prefix}}\core
	 */
	protected static $instance = null;

	/**
	 * Holds the option screen prefix
	 *
	 * @since {{core_version}}
	 *
	 * @var      string
	 */
	protected $plugin_screen_hook_suffix = null;

	/**
	 * Initialize the plugin by setting localization, filters, and administration functions.
	 *
	 * @since {{core_version}}
	 *
	 * @access private
	 */
	private function __construct() {

		// Load plugin text domain
		add_action( 'init', array( $this, 'load_plugin_textdomain' ) );

		// Activate plugin when new blog is added
		add_action( 'wpmu_new_blog', array( $this, 'activate_new_site' ) );

		// Load admin style sheet and JavaScript.
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_stylescripts' ) );

		// Load front style sheet and JavaScript.
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_front_stylescripts' ) );

		{{module_mode_core_actions}}

		//load settings class in admin
		if ( is_admin() ) {
			new settings();
		}

	}


	/**
	 * Return an instance of this class.
	 *
	 * @since {{core_version}}
	 *
	 * @return    object|\calderawp\{{core_prefix}}\core    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;

	}

	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since {{core_version}}
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain( $this->plugin_slug, false, basename( {{CORE_PREFIX}}_PATH ) . '/languages');

	}

	/**
	 * Register and enqueue front-specific style sheet.
	 *
	 * @since {{core_version}}
	 *
	 * @return    null
	 */
	public function enqueue_front_stylescripts() {
		
		{{module_mode_front_style_scripts}}

	}
	
	/**
	 * Register and enqueue admin-specific style sheet.
	 *
	 * @since {{core_version}}
	 *
	 * @return    null
	 */
	public function enqueue_admin_stylescripts() {

		$screen = get_current_screen();

		if( !is_object( $screen ) ){
			return;

		}

		{{module_mode_core_enqueue}}
		
		if( false !== strpos( $screen->base, '{{core_slug}}' ) ){

			{{module_mode_style_scripts}}
		
		}


	}

{{module_mode_core_methods}}

}















