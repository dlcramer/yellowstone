<?php
/**
 * {{Core_Name}} Setting.
 *
 * @package   {{Core_Class}}
 * @author    {{core_author_name}}
 * @license   GPL-2.0+
 * @link
 * @copyright 2015 {{core_author_name}}
 */
namespace calderawp\{{core_prefix}};

/**
 * Settings class
 * @package {{Core_Class}}
 * @author  {{core_author_name}}
 */
class settings extends core{


	/**
	 * Constructor for class
	 *
	 * @since {{core_version}}
	 */
	public function __construct(){

		// add admin page
		add_action( 'admin_menu', array( $this, 'add_settings_pages' ), 25 );

		// save config
		add_action( 'wp_ajax_{{core_prefix}}_save_config', array( $this, 'save_config') );

		// exporter
		add_action( 'init', array( $this, 'check_exporter' ) );

		{{module_mode_settings_actions}}

	}

	/**
	 * builds an export
	 *
	 * @uses "wp_ajax_{{core_prefix}}_check_exporter" hook
	 *
	 * @since 0.0.1
	 */
	public function check_exporter(){


		if( !empty( $_REQUEST['download'] ) && !empty( $_REQUEST['{{core-slug}}-export'] ) && wp_verify_nonce( $_REQUEST['{{core-slug}}-export'], '{{core-slug}}' && options::can( 'export' ) ) ){

			$data = options::get_single( $_REQUEST['download'] );

			header( 'Content-Type: application/json' );
			header( 'Content-Disposition: attachment; filename="{{core-slug}}-export.json"' );
			echo wp_json_encode( $data );
			exit;
			
		}
	}

	/**
	 * Saves a config
	 *
	 * @uses "wp_ajax_{{core_prefix}}_save_config" hook
	 *
	 * @since 0.0.1
	 */
	public function save_config(){

		$can = options::can();
		if ( ! $can ) {
			status_header( 500 );
			wp_die( __( 'Access denied', '{{core-slug}}' ) );
		}

		if( empty( $_POST[ '{{core-slug}}-setup' ] ) || ! wp_verify_nonce( $_POST[ '{{core-slug}}-setup' ], '{{core-slug}}' ) ){
			if( empty( $_POST['config'] ) ){
				return;

			}

		}

		if( ! empty( $_POST[ '{{core-slug}}-setup' ] ) && empty( $_POST[ 'config' ] ) ){
			$config = stripslashes_deep( $_POST['config'] );

			options::update( $config );


			wp_redirect( '?page={{core_slug}}&updated=true' );
			exit;

		}

		if( ! empty( $_POST[ 'config' ] ) ){

			$config = json_decode( stripslashes_deep( $_POST[ 'config' ] ), true );

			if(	wp_verify_nonce( $config['{{core-slug}}-setup'], '{{core-slug}}' ) ){
				options::update( $config );
				wp_send_json_success( $config );

			}

		}

		// nope
		wp_send_json_error( $config );

	}

	/**
	 * Array of "internal" fields not to mess with
	 *
	 * @since 0.0.1
	 *
	 * @return array
	 */
	public function internal_config_fields() {
		return array( '_wp_http_referer', 'id', '_current_tab' );

	}


	/**
	 * Deletes an item
	 *
	 *
	 * @uses 'wp_ajax_{{core_prefix}}_create_{{core_item}}' action
	 *
	 * @since 0.0.1
	 */
	public function delete_{{core_slug}}(){
		$can = options::can();
		if ( ! $can ) {
			status_header( 500 );
			wp_die( __( 'Access denied', '{{core-slug}}' ) );
		}

		$deleted = options::delete( strip_tags( $_POST[ 'block' ] ) );

		if ( $deleted ) {
			wp_send_json_success( $_POST );
		}else{
			wp_send_json_error( $_POST );
		}

	}

	/**
	 * Create a new item
	 *
	 * @uses "wp_ajax_{{core_prefix}}_create_{{core_item}}"  action
	 *
	 * @since 0.0.1
	 */
	public function create_new_{{core_item}}(){

		$can = options::can();
		if ( ! $can ) {
			status_header( 500 );
			wp_die( __( 'Access denied', '{{core-slug}}' ) );
		}


		if( !empty( $_POST['import'] ) ){
			$config = json_decode( stripslashes_deep( $_POST[ 'import' ] ), true );

			if( empty( $config['name'] ) || empty( $config['slug'] ) ){
				wp_send_json_error( $_POST );
			}
			$id = null;
			if( !empty( $config['id'] ) ){
				$id = $config['id'];
			}
			options::create( $config[ 'name' ], $config[ 'slug' ], $id );
			options::update( $config );
			wp_send_json_success( $config );
		}

		$new = options::create( $_POST[ 'name' ], $_POST[ 'slug' ] );

		if ( is_array( $new ) ) {
			wp_send_json_success( $new );

		}else {
			wp_send_json_error( $_POST );

		}

	}


	/**
	 * Add options page
	 *
	 * @since {{core_version}}
	 *
	 * @uses "admin_menu" hook
	 */
	public function add_settings_pages(){
			// This page will be under "Settings"
			{{module_mode_add_menu}}
			add_action( 'admin_print_styles-' . $this->plugin_screen_hook_suffix['{{core_slug}}'], array( $this, 'enqueue_admin_stylescripts' ) );

	}

	/**
	 * Options page callback
	 *
	 * @since {{core_version}}
	 */
	public function create_admin_page(){
		// Set class property        
		$screen = get_current_screen();
		$base = array_search($screen->id, $this->plugin_screen_hook_suffix);
			
		// include main template
		{{module_mode_admin_loader}}

		// php based script include
		if( file_exists( {{CORE_PREFIX}}_PATH .'assets/js/inline-scripts.php' ) ){
			echo "<script type=\"text/javascript\">\r\n";
			include {{CORE_PREFIX}}_PATH .'assets/js/inline-scripts.php';
			echo "</script>\r\n";
		}

	}



{{module_mode_settings_methods}}	
}

