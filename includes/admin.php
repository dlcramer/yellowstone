<div class="wrap">
	<div class="caldera-main-header">
		<h2>
			<?php _e( 'Caldera Yellowstone', 'caldera-yellowstone' ); ?> <span class="caldera-version"><?php echo CYE_VER; ?></span>
			<span class="add-new-h2 wp-baldrick" data-modal="new-yellowstone_element" data-modal-height="200" data-modal-width="402" data-modal-buttons='<?php _e( 'Create Yellowstone Element', 'caldera-yellowstone' ); ?>|{"data-action":"cye_create_yellowstone_element","data-before":"cye_create_new_yellowstone_element", "data-callback": "bds_redirect_to_yellowstone_element"}' data-modal-title="<?php _e('New Yellowstone Element', 'caldera-yellowstone') ; ?>" data-request="#new-yellowstone_element-form"><?php _e('Add New', 'caldera-yellowstone') ; ?></span>
		</h2>
	</div>

<?php

	$yellowstone_elements = get_option('_yellowstone_elements');
	if( empty( $yellowstone_elements ) ){
		$yellowstone_elements = array();
	}
	global $wpdb;
	foreach( $yellowstone_elements as $yellowstone_element_id => $yellowstone_element ){
		$yellowstone_element = get_option( $yellowstone_element_id );
		if( empty( $yellowstone_element ) ){
			//continue;
		}
?>

	<div class="caldera-card-item" id="yellowstone_element-<?php echo $yellowstone_element['id']; ?>">
		<span class="dashicons <?php if( isset( $yellowstone_element['dashicon'] ) ){ echo $yellowstone_element['dashicon']; } ?> caldera-card-icon" <?php if( !empty( $yellowstone_element['primary_color'] ) ){ ?>style="color:<?php echo $yellowstone_element['primary_color']; ?>;"<?php } ?>></span>
		<div class="caldera-card-content">
			<h4><?php echo $yellowstone_element['name']; ?></h4>
			<div class="description"><?php echo $yellowstone_element['slug']; ?></div>
			<div class="description"><?php if( isset($yellowstone_element['description'])){ echo $yellowstone_element['description']; } ?></div>
			<div class="caldera-card-actions">
				<div class="row-actions">
					<span class="edit"><a href="?page=caldera_yellowstone&amp;edit=<?php echo $yellowstone_element['id']; ?>">Edit</a> | </span>
					<span class="trash confirm"><a href="?page=caldera_yellowstone&amp;delete=<?php echo $yellowstone_element['id']; ?>" data-block="<?php echo $yellowstone_element['id']; ?>" class="submitdelete">Delete</a></span>
				</div>
				<div class="row-actions" style="display:none;">
					<span class="trash"><a class="wp-baldrick" style="cursor:pointer;" data-action="cye_delete_yellowstone_element" data-callback="cye_remove_deleted" data-block="<?php echo $yellowstone_element['id']; ?>" class="submitdelete">Confirm Delete</a> | </span>
					<span class="edit confirm"><a href="?page=caldera_yellowstone&amp;edit=<?php echo $yellowstone_element['id']; ?>">Cancel</a></span>
				</div>
			</div>
		</div>
		<?php if( !empty($yellowstone_element['name']) && !empty($yellowstone_element['slug']) && !empty($yellowstone_element['description']) && !empty($yellowstone_element['prefix']) && !empty($yellowstone_element['item_name']) && !empty($yellowstone_element['store_name']) ) { ?>
		<button type="button" id="builder-<?php echo $yellowstone_element_id; ?>" data-item="<?php echo $yellowstone_element_id; ?>" class="button button-small wp-baldrick" data-action="cye_build_plugin" data-active-class="none" data-load-class="button-primary" data-load-element="#builder-<?php echo $yellowstone_element_id; ?>" style="padding: 0px 4px; margin: 3px 0px; position: absolute; left: 10px; bottom: 6px;"><span class="dashicons dashicons-hammer" style="padding: 0px; margin: 0px; line-height: 23px;"></span></button>
		<?php } ?>
	</div>

	<?php } ?>

</div>
<div class="clear"></div>
<script type="text/javascript">
	
	function cye_create_new_yellowstone_element(el){
		var yellowstone_element 	= jQuery(el),
			name 	= jQuery("#new-yellowstone_element-name"),
			slug 	= jQuery('#new-yellowstone_element-slug');

		if( slug.val().length === 0 ){
			name.focus();
			return false;
		}
		if( slug.val().length === 0 ){
			slug.focus();
			return false;
		}

		yellowstone_element.data('name', name.val() ).data('slug', slug.val() ); 

	}

	function bds_redirect_to_yellowstone_element(obj){
		
		if( obj.data.success ){

			obj.params.trigger.prop('disabled', true).html('<?php _e('Loading Plugin', 'caldera-yellowstone'); ?>');
			window.location = '?page=caldera_yellowstone&edit=' + obj.data.data.id;

		}else{

			jQuery('#new-block-slug').focus().select();
			
		}
	}
	function cye_remove_deleted(obj){

		if( obj.data.success ){
			jQuery( '#yellowstone_element-' + obj.data.data.block ).fadeOut(function(){
				jQuery(this).remove();
			});
		}else{
			alert('<?php echo __('Sorry, something went wrong. Try again.', 'caldera-yellowstone'); ?>');
		}


	}
</script>
<script type="text/html" id="new-yellowstone_element-form">
	<div class="caldera-config-group">
		<label><?php _e('Plugin Name', 'caldera-yellowstone'); ?></label>
		<input type="text" name="name" id="new-yellowstone_element-name" data-sync="#new-yellowstone_element-slug" autocomplete="off">
	</div>
	<div class="caldera-config-group">
		<label><?php _e('Plugin Slug', 'caldera-yellowstone'); ?></label>
		<input type="text" name="slug" id="new-yellowstone_element-slug" data-format="slug" autocomplete="off">
	</div>

</script>