		<h4><label><small><input type="checkbox" class="wp-baldrick" data-request="cye_get_default_setting" data-event="change" data-script="toggle-tgm" name="tgm_enabled" value="1" {{#if tgm_enabled}}checked="checked"{{/if}} id="yellowstone_element-enable_tgm"></small> <?php _e( 'TGM Plugin Activation', 'caldera-yellowstone' ); ?></label></h4>
		
		{{#if tgm_enabled}}
			<button class="button button-small wp-baldrick" data-request="cye_get_default_setting" data-script="new-tgm" type="button" ><?php _e( 'Add Plugin', 'caldera-yellowstone' ); ?></button>
			<hr>
			{{#each tgm/require}}
			<div class="caldera-tab-item caldera-wrap-item" style="width:380px; display:block;">

				<input type="hidden" name="tgm[require][{{_id}}][_id]" value="{{_id}}">
				<div class="caldera-config-group" style="width: 228px; float: left;">
					<label for="yellowstone_element-package" style="width: 100%;"><?php _e( 'Package', 'caldera-yellowstone' ); ?></label>
					<input type="text" name="tgm[require][{{_id}}][package]" value="{{package}}" style="width: 100%">
				</div>
				<div class="caldera-config-group" style="float: left; width: 80px; margin-left: 11px;">
					<label for="yellowstone_element-version" style="width: 100%;"><?php _e( 'Version', 'caldera-yellowstone' ); ?></label>
					<input type="text" name="tgm[require][{{_id}}][version]" value="{{version}}" style="width: 100%">
				</div>

							
				<button type="button" class="button button-small" style="padding: 0px; margin: 3px 0px; position: absolute; left: 14px; top: 6px;" data-remove-parent=".caldera-wrap-item"><span class="dashicons dashicons-no-alt" style="padding: 0px; margin: 0px; line-height: 23px;"></span></button>			
			
			</div>
			{{/each}}
		{{/if}}