
		<h4><?php _e( 'General Settings', 'caldera-yellowstone' ); ?></h4>
		<div class="caldera-config-group">
			<label for="yellowstone_element-name"><?php _e( 'Plugin Name', 'caldera-yellowstone' ); ?></label>
			<input type="text" name="name" value="{{name}}" data-sync="#yellowstone_element-name-title" id="yellowstone_element-name" style="width: 220px;" required>
		</div>
		
		<div class="caldera-config-group">
			<label for="yellowstone_element-short_name"><?php _e( 'Short Name', 'caldera-yellowstone' ); ?></label>
			<input type="text" name="short_name" value="{{short_name}}" id="yellowstone_element-short_name" style="width: 220px;" required>
		</div>

		<div class="caldera-config-group">
			<label for="yellowstone_element-slug"><?php _e( 'Plugin Slug', 'caldera-yellowstone' ); ?></label>
			<input type="text" name="slug" value="{{slug}}" data-format="slug" data-sync=".caldera-subline" data-master="#yellowstone_element-name" id="yellowstone_element-slug" required style="width: 220px;">
		</div>

		<div class="caldera-config-group">
			<label for="yellowstone_element-version"><?php _e( 'Plugin Version', 'caldera-yellowstone' ); ?></label>
			<input type="text" name="version" value="{{#if version}}{{version}}{{else}}0.0.1{{/if}}" data-master="#yellowstone_element-name" id="yellowstone_element-version" required style="width: 220px;">
		</div>

		<div class="caldera-config-group">
			<label for="yellowstone_element-description"><?php _e( 'Plugin Description', 'caldera-yellowstone' ); ?></label>
			<input type="text" name="description" value="{{description}}" data-master="#yellowstone_element-name" id="yellowstone_element-description" required style="width: 220px;">
		</div>

		<div class="caldera-config-group">
			<label for="yellowstone_element-mode"><?php _e( 'Config Mode', 'caldera-yellowstone' ); ?></label>
			<select name="mode" id="yellowstone_element-mode" data-live-sync="true" required style="width: 220px;">
				<option value="single" {{#is mode value="single"}}selected="selected"{{/is}}><?php _e('Single', 'caldera-yellowstone' ); ?></option>
				<option value="multi" {{#is mode value="multi"}}selected="selected"{{/is}}><?php _e('Multi', 'caldera-yellowstone' ); ?></option>
			</select>
		</div>
		<div class="caldera-config-group"{{#is mode value="single"}} style="display:none;"{{/is}}{{#is mode value="post_type"}} style="display:none;"{{/is}}>
			<label for="yellowstone_element-card_size"><?php _e( 'Item Card Width', 'caldera-yellowstone' ); ?></label>
			<input type="text" name="card_size" value="{{#if card_size}}{{card_size}}{{else}}250px{{/if}}" style="width: 220px;" required>
		</div>

		<div class="caldera-config-group">
			<label for="yellowstone_element-menu_type"><?php _e( 'Menu Type', 'caldera-yellowstone' ); ?></label>
			<select data-live-sync="true" name="menu_type" id="yellowstone_element-menu_type" required style="width: 220px;">
				<option value="sub" {{#is menu_type value="sub"}}selected="selected"{{/is}}><?php _e('Sub Item', 'caldera-yellowstone' ); ?></option>
				<option value="parent" {{#is menu_type value="parent"}}selected="selected"{{/is}}><?php _e('Parent Item', 'caldera-yellowstone' ); ?></option>
			</select>
		</div>
		<div class="caldera-config-group"{{#is menu_type value="parent"}} style="display:none;"{{/is}}>
			<label for="yellowstone_element-menu_location"><?php _e( 'Menu Location', 'caldera-yellowstone' ); ?></label>
			<input type="text" name="menu_location" value="{{#if menu_location}}{{menu_location}}{{else}}options-general.php{{/if}}" style="width: 220px;">
		</div>
		<div class="caldera-config-group">
			<label for="yellowstone_element-prefix"><?php _e( 'Prefix', 'caldera-yellowstone' ); ?></label>
			<input type="text" name="prefix" value="{{prefix}}" data-format="slug" data-master="#yellowstone_element-name" id="yellowstone_element-prefix" required style="width: 220px;">
		</div>
				
		<div class="caldera-config-group">
			<label for="yellowstone_element-item_name"><?php _e( 'Item Name (Singluar)', 'caldera-yellowstone' ); ?></label>
			<input type="text" name="item_name" value="{{#if item_name}}{{item_name}}{{else}}{{name}}{{/if}}" id="yellowstone_element-item_name" required style="width: 220px;">
		</div>

		<div class="caldera-config-group">
			<label for="yellowstone_element-store_name"><?php _e( 'Storage Name', 'caldera-yellowstone' ); ?></label>
			<input type="text" name="store_name" data-format="slug" value="{{#if store_name}}{{store_name}}{{else}}{{slug}}{{/if}}" id="yellowstone_element-store_name" required style="width: 220px;">
		</div>

		<div class="caldera-config-group">
			<label for="yellowstone_element-shortcode"><?php _e( 'Shortcode', 'caldera-yellowstone' ); ?></label>
			<label style="width:auto;"><input type="checkbox" name="shortcode" value="1"" {{#if shortcode}}checked="checked"{{/if}}" id="yellowstone_element-shortcode"> <?php _e( 'Include Shortcode Inserter', 'caldera-yellowstone' ); ?></label>
		</div>


		<div class="caldera-config-group">
			<label for="yellowstone_element-enable_license"><?php _e( 'Licensing', 'caldera-yellowstone' ); ?></label>
			
			<label style="width:auto;"><input type="checkbox" name="license_enabled" value="1" data-live-sync="true" {{#if license_enabled}}checked="checked"{{/if}} id="yellowstone_element-enable_license"> <?php _e( 'Enable License Management', 'caldera-yellowstone' ); ?></label>
		</div>


		<div class="caldera-config-group">
			<label for="yellowstone_element-theme"><?php _e( 'Theme', 'caldera-yellowstone' ); ?></label>
			<select data-live-sync="true" name="theme" id="yellowstone_element-theme" required style="width: 220px;">
				<option value="caldera" {{#is theme value="caldera"}}selected="selected"{{/is}}><?php _e('Caldera', 'caldera-yellowstone' ); ?></option>
				<option value="brickroad" {{#is theme value="brickroad"}}selected="selected"{{/is}}><?php _e('Brickroad', 'caldera-yellowstone' ); ?></option>
				<option value="wordpress" {{#is theme value="wordpress"}}selected="selected"{{/is}}><?php _e('WordPress', 'caldera-yellowstone' ); ?></option>
			</select>
		</div>
		<div class="caldera-config-group"{{#is theme value="wordpress"}} style="display:none;"{{/is}}>
			<label for="yellowstone_element-primary_color"><?php _e( 'Primary Color', 'caldera-yellowstone' ); ?></label>
			<input class="color-field" type="text" name="primary_color" value="{{#if primary_color}}{{primary_color}}{{else}}rgb(175, 175, 0){{/if}}" id="yellowstone_element-primary_color" required style="width: 85px;">
		</div>

		<div class="caldera-config-group">
			<label for="yellowstone_element-dashicon"><?php _e( 'Dashicon', 'caldera-yellowstone' ); ?></label>
			<input type="text" name="dashicon" value="{{#if dashicon}}{{dashicon}}{{else}}dashicons-admin-generic{{/if}}" id="yellowstone_element-dashicon" data-live-sync="true" required style="width: 187px;"> <button style="padding: 3px;" class="button dashicons-picker" type="button" data-target="#yellowstone_element-dashicon" ><span class="dashicons {{#if dashicon}}{{dashicon}}{{else}}dashicons-admin-generic{{/if}}"></span></button>
		</div>

		<div class="caldera-config-group">
			<label for="yellowstone_element-plugin_author_name"><?php _e( 'Plugin Author Name', 'caldera-yellowstone' ); ?></label>
			<input type="text" name="core_author_name" value="{{#if core_author_name}}{{core_author_name}}{{else}}David Cramer{{/if}}" id="yellowstone_element-author-author-name" required style="width: 220px;">
		</div>

		<div class="caldera-config-group">
			<label for="yellowstone_element-plugin_author_uri"><?php _e( 'Plugin Author URI', 'caldera-yellowstone' ); ?></label>
			<input type="text" name="core_author_uri" value="{{#if core_author_uri}}{{core_author_uri}}{{else}}https://CalderaWP.com{{/if}}" id="yellowstone_element-plugin_author_uri" required style="width: 220px;">


		</div>

		<div class="caldera-config-group">
			<label for="yellowstone_element-save_button"><?php _e( 'Save Button Text', 'caldera-yellowstone' ); ?></label>
			<input type="text" name="save_button" value="{{#if save_button}}{{save_button}}{{else}}Save Changes{{/if}}" id="yellowstone_element-save_button" required style="width: 220px;">
		</div>


		<button class="button button-small wp-baldrick" data-request="cye_get_default_setting" data-script="add-admin-page" type="button" ><?php _e( 'Add Admin Page', 'caldera-yellowstone' ); ?></button>
		<hr>
		{{#each admin_page}}
		{{#if _id}}
		<div class="caldera-admin-pages">
			<input type="hidden" name="admin_page[{{_id}}][_id]" value="{{_id}}">
			<div class="caldera-config-group">
				<label for="yellowstone_element-admin_page-{{_id}}"><?php _e( 'Admin Page Name', 'caldera-yellowstone' ); ?></label>
				<input type="text" name="admin_page[{{_id}}][name]" value="{{name}}" data-live-sync="true" data-sync="#yellowstone_element-admin_page-{{_id}}-slug-tmp" id="yellowstone_element-admin_page-{{_id}}" required style="width: 220px;">
				{{#unless slug}}<input type="hidden" name="admin_page[{{_id}}][slug]" value="{{slug}}" data-format="key" id="yellowstone_element-admin_page-{{_id}}-slug-tmp">{{/unless}}
				<button type="button" class="button button-small" style="" data-remove-parent=".caldera-admin-pages"><span class="dashicons dashicons-no-alt" style="padding: 0px; margin: 0px; line-height: 23px;"></span></button>
			</div>
		</div>
		{{/if}}
		{{/each}}


