		<?php $current_user = wp_get_current_user(); ?>
		<h4><?php _e( 'Composer', 'caldera-yellowstone' ); ?></h4>
        <input type="hidden" name="composer_enabled" value="1">
		

			<div class="caldera-config-group">
				<label for="yellowstone_element-short_name"><?php _e( 'Author', 'caldera-yellowstone' ); ?></label>
				<input type="text" name="composer[author][name]" value="{{#if composer/author/name}}{{composer/author/name}}{{else}}<?php echo $current_user->display_name; ?>{{/if}}" id="yellowstone_element-short_name" style="width: 220px;">
			</div>
			<div class="caldera-config-group">
				<label for="yellowstone_element-slug"><?php _e( 'Homepage', 'caldera-yellowstone' ); ?></label>
				<input type="text" name="composer[author][homepage]" value="{{#if composer/author/homepage}}{{composer/author/homepage}}{{else}}<?php echo $current_user->user_url; ?>{{/if}}" required style="width: 220px;">
			</div>
			<div class="caldera-config-group">
				<label for="yellowstone_element-description"><?php _e( 'Role', 'caldera-yellowstone' ); ?></label>
				<input type="text" name="composer[author][role]" value="{{#if composer/author/role}}{{composer/author/role}}{{else}}<?php _e('Developer', 'caldera-yellowstone'); ?>{{/if}}" required style="width: 220px;">
			</div>
			<br>
			<button class="button button-small wp-baldrick" data-request="cye_get_default_setting" data-script="new-composer" type="button" ><?php _e( 'Add Package', 'caldera-yellowstone' ); ?></button>
			<hr>
			{{#each composer/require}}
			<div class="caldera-tab-item caldera-wrap-item" style="width:380px; display:block;">

				<input type="hidden" name="composer[require][{{_id}}][_id]" value="{{_id}}">
				<div class="caldera-config-group" style="width: 228px; float: left;">
					<label for="yellowstone_element-package" style="width: 100%;"><?php _e( 'Package', 'caldera-yellowstone' ); ?></label>
					<input type="text" name="composer[require][{{_id}}][package]" value="{{package}}" style="width: 100%">
				</div>
				<div class="caldera-config-group" style="float: left; width: 80px; margin-left: 11px;">
					<label for="yellowstone_element-version" style="width: 100%;"><?php _e( 'Version', 'caldera-yellowstone' ); ?></label>
					<input type="text" name="composer[require][{{_id}}][version]" value="{{version}}" style="width: 100%">
				</div>

							
				<button type="button" class="button button-small" style="padding: 0px; margin: 3px 0px; position: absolute; left: 14px; top: 6px;" data-remove-parent=".caldera-wrap-item"><span class="dashicons dashicons-no-alt" style="padding: 0px; margin: 0px; line-height: 23px;"></span></button>			
			
			</div>

        {{/each}}
