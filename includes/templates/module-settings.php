		<h4><?php _e( 'Modules', 'caldera-yellowstone' ); ?></h4>
		
		{{#each module}}
		<div class="caldera-config-group">
			<input type="hidden" name="module[{{slug}}][name]" value="{{name}}">
			<input type="hidden" name="module[{{slug}}][slug]" value="{{slug}}">
			<label style="display: inline-block; margin: 0px;"><input type="checkbox" data-live-sync="true" name="module[{{slug}}][enabled]" value="1" {{#if enabled}}checked="checked"{{/if}}> {{name}}</label>
		</div>
		{{/each}}