<div class="caldera-main-header">
	<h2>
		<span id="yellowstone_element-name-title">{{name}}</span> <span class="caldera-subline">{{slug}}</span>
		<span class="add-new-h2 wp-baldrick" data-action="cye_save_config" data-load-element="#caldera-save-indicator" data-before="cye_get_config_object" ><?php _e('Save Changes', 'caldera-yellowstone') ; ?></span>
		<a class="add-new-h2" href="<?php echo admin_url( '?page=caldera_yellowstone'  ); ?>"><?php _e( 'Close', 'caldera-yellowstone') ; ?></a>
		{{#if version}}
		<span id="builder" data-item="<?php echo $yellowstone_element['id']; ?>" class="add-new-h2 wp-baldrick" data-before="cye_get_config_object" data-action="cye_build_plugin" data-load-element="#caldera-save-indicator,#builder" data-active-class="none" data-load-class="loading building" data-load-element="#builder-<?php echo $yellowstone_element['id']; ?>"><span class="dashicons dashicons-hammer" style="margin: 0px; padding: 0px; line-height: 26px;"></span></span>
		{{/if}}
	</h2>
	<ul class="caldera-header-tabs">
		

		<?php /*<li class="caldera-nav-tab {{#is _current_tab value="#caldera-panel-modules"}} active{{/is}}"><a href="#caldera-panel-modules"><?php _e('Modules', 'caldera-yellowstone') ; ?></a></li> */ ?>

		
		<li id="caldera-save-indicator"><span style="float: none; margin: 16px 0px -5px 10px;" class="spinner"></span></li>
	</ul>
	<span class="wp-baldrick" id="caldera-field-sync" data-event="refresh" data-target="#caldera-main-canvas" data-callback="cye_canvas_init" data-type="json" data-request="#caldera-live-config" data-template="#main-ui-template"></span>
</div>
<div class="caldera-sub-header">
	<ul class="caldera-sub-tabs caldera-nav-tabs">
		<li class="caldera-nav-tab {{#is _current_tab value="#caldera-panel-general"}} active{{/is}}"><a href="#caldera-panel-general"><?php _e('General', 'caldera-yellowstone') ; ?></a></li>
	
		<!-- <li class="caldera-nav-tab"><a href="#caldera-panel-tgm"><?php _e( 'TGM', 'caldera-yellowstone') ; ?></a></li> -->

        <li class="caldera-nav-tab {{#is _current_tab value="#caldera-panel-tabs"}} active{{/is}}"><a href="#caldera-panel-tabs"><?php _e('Admin Tabs', 'caldera-yellowstone') ; ?></a></li>
        <li class="caldera-nav-tab {{#is _current_tab value="#caldera-panel-composer"}} active{{/is}}"><a href="#caldera-panel-composer"><?php _e('Composer', 'caldera-yellowstone') ; ?></a></li>
        {{#if license_enabled}}
        <li class="caldera-nav-tab {{#is _current_tab value="#caldera-panel-license"}} active{{/is}}"><a href="#caldera-panel-license"><?php _e('Licensing', 'caldera-yellowstone') ; ?></a></li>
        {{/if}}
		{{#each admin_page}}
			{{#if name}}        
        		<li class="caldera-nav-tab {{#is ../../_current_tab value=_id}} active{{/is}}"><a href="{{_id}}">{{name}}</a></li>
        	{{/if}}
        {{/each}}
		<?php /*{{#each module}}
		{{#if enabled}}<li class="caldera-nav-tab"><a href="#caldera-panel-module-{{slug}}">{{name}}</a></li>{{/if}}
		{{/each}}*/ ?>
	</ul>
</div>
<form id="caldera-main-form" action="?page=caldera_yellowstone" method="POST">
	<input type="hidden" value="{{id}}" name="id" id="yellowstone_element-id">
	<input type="hidden" value="{{_current_tab}}" name="_current_tab" id="caldera-active-tab">

	<div id="caldera-panel-general" class="caldera-editor-panel" {{#is _current_tab value="#caldera-panel-general"}}{{else}} style="display:none;" {{/is}}>
		<?php
		// pull in the general settings template
		include CYE_PATH . 'includes/templates/general-settings.php';
		?>
	</div>

	<div id="caldera-panel-tabs" class="caldera-editor-panel" {{#is _current_tab value="#caldera-panel-tabs"}}{{else}} style="display:none;" {{/is}}>
		<?php
		// pull in the tab settings template
		include CYE_PATH . 'includes/templates/tab-settings.php';
		?>
	</div>
	<?php /*
	<div id="caldera-panel-modules" class="caldera-editor-panel" {{#is _current_tab value="#caldera-panel-modules"}}{{else}} style="display:none;" {{/is}}>
		<?php
		// pull in the module settings template
		include CYE_PATH . 'includes/templates/module-settings.php';
		?>
	</div>*/ ?>

	<div id="caldera-panel-composer" class="caldera-editor-panel" {{#is _current_tab value="#caldera-panel-composer"}}{{else}} style="display:none;" {{/is}}>
		<?php
		// pull in the composer settings template
		include CYE_PATH . 'includes/templates/composer-settings.php';
		?>
	</div>
    <div id="caldera-panel-tgm" class="caldera-editor-panel" {{#is _current_tab value="#caldera-panel-tgm"}}{{else}} style="display:none;" {{/is}}>
    <?php
    // pull in the tgm settings template
    include CYE_PATH . 'includes/templates/tgm-settings.php';
    ?>
    </div>
    <div id="caldera-panel-license" class="caldera-editor-panel" {{#is _current_tab value="#caldera-panel-license"}}{{else}} style="display:none;" {{/is}}>
    <?php
    // pull in the license settings template
    include CYE_PATH . 'includes/templates/license-settings.php';
    ?>
    </div>

	{{#each admin_page}}
		{{#if name}}
			<div id="{{_id}}" class="caldera-editor-panel" {{#is ../../_current_tab value=_id}}{{else}} style="display:none;" {{/is}}>
			<?php
			// pull in the license settings template
				include CYE_PATH . 'includes/templates/admins-settings.php';
			?>
			</div>
	    {{/if}}
    {{/each}}

	<?php
	/*
	foreach( $yellowstone_element['module'] as $module ){
		if( file_exists( CYE_PATH . 'module-templates/' . $module['slug'] .'/main-ui.php' ) ){
		?>
		<div id="caldera-panel-module-<?php echo $module['slug']; ?>" class="caldera-editor-panel" {{#is _current_tab value="#caldera-panel-module-<?php echo $module['slug']; ?>"}}{{else}} style="display:none;" {{/is}}>
			<?php
			// pull in the module template
			include CYE_PATH . 'module-templates/' . $module['slug'] .'/main-ui.php';
			?>
		</div>
		<?php
		}
	}*/
	?>

</form>
