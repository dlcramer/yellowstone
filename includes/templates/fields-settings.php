		<h4><?php _e( 'Fields', 'caldera-yellowstone' ); ?></h4>
		

		<ul class="yellowstone_element-panel-controls" style="float: left;margin-top: 2px;width: 20px;">
			<li style="margin-bottom:0;">
				<span style="color: #888;cursor: pointer;display: inline-block;margin: 4px 0;" class="dashicons dashicons-admin-page"></span>
			</li>
		</ul>	
		<div class="yellowstone_element-wrapper">

			<div class="yellowstone_element-group" id="ID-EPBVEQIK3" style="opacity: 1;">
				<span data-request="ce_toggle_group_show" class="yellowstone_element-group-config panel-toggle-show yellowstone_element-trigger dashicons dashicons-arrow-up _tisBound" id="baldrick_trigger_1416119771238"></span>
				<span style="display:none;" data-request="ce_toggle_group_show" class="yellowstone_element-group-config panel-toggle-show yellowstone_element-trigger dashicons dashicons-arrow-down _tisBound" id="baldrick_trigger_1416119771240"></span>

				<span data-group="ID-EPBVEQIK3" data-callback="ce_reset_variable_sortables" data-template="#yellowstone_element-panel-item-tmpl" data-target-insert="append" data-target="#items-theme_colors" data-request="ce_add_variable_item" class="yellowstone_element-group-config yellowstone_element-trigger group-item-add dashicons dashicons-plus-alt _tisBound" id="baldrick_trigger_1416119771241"></span>
				<h3>
					<span style="float:left;" class="yellowstone_element-group-handle dashicons dashicons-menu"></span> 

					<span style="float:left;" data-config="#yellowstone_element-group-config-ID-EPBVEQIK3" data-request="ce_toggle_group_config" class="yellowstone_element-group-config yellowstone_element-trigger dashicons dashicons-admin-generic _tisBound" id="baldrick_trigger_1416119771242"></span>

					<span class="label_ID-EPBVEQIK3">Colors</span></h3>
					<div id="yellowstone_element-group-config-ID-EPBVEQIK3" class="yellowstone_element-group-config">
						<label>Label</label>
						<input type="text" value="Colors" data-sync="label_ID-EPBVEQIK3" class="ID-EPBVEQIK3 yellowstone_element-group-label yellowstone_element-trigger-text-change" name="variable_groups[ID-EPBVEQIK3][label]">
						<label>Description</label>
						<input type="text" value="" class="ID-EPBVEQIK3" name="variable_groups[ID-EPBVEQIK3][description]">
						<label>Slug</label>
						<input type="text" value="theme_colors" data-format="slug" class="ID-EPBVEQIK3" name="variable_groups[ID-EPBVEQIK3][slug]">
						<button style="margin: 6px 2px 2px;" class="yellowstone_element-trigger button button-small right _tisBound" data-request="ce_delete_variable_group" data-id="ID-EPBVEQIK3" type="button" id="baldrick_trigger_1416119771243">Delete Group</button>
						<div class="clear"></div>
					</div>
					<div data-group="ID-EPBVEQIK3" id="items-theme_colors" class="yellowstone_element-holder ui-sortable">

						<div data-id="ID-W0AOO64YX" class="yellowstone_element-item highlight">
							<span data-id="ID-W0AOO64YX" data-before="ce_show_variable_config_panel" class="yellowstone_element-group-config dashicons dashicons-edit yellowstone_element-trigger _tisBound" id="baldrick_trigger_1416119771244"></span>
							<h4 class="ID-W0AOO64YX"><span class="label_ID-W0AOO64YX">Primary</span> <small class="yellowstone_element-panel-caption slug_ID-W0AOO64YX">primary_color</small></h4>
							<input type="hidden" value="ID-EPBVEQIK3" name="variables[ID-W0AOO64YX][group]" class="variable-group-id">

						</div>

						<div data-id="ID-KC8LKSJKN" class="yellowstone_element-item">
							<span data-id="ID-KC8LKSJKN" data-before="ce_show_variable_config_panel" class="yellowstone_element-group-config dashicons dashicons-edit yellowstone_element-trigger _tisBound" id="baldrick_trigger_1416119771246"></span>
							<h4 class="ID-KC8LKSJKN"><span class="label_ID-KC8LKSJKN">Primary Hover</span> <small class="yellowstone_element-panel-caption slug_ID-KC8LKSJKN">primary_color_hover</small></h4>
							<input type="hidden" value="ID-EPBVEQIK3" name="variables[ID-KC8LKSJKN][group]" class="variable-group-id">

						</div>
					</div>		
				</div>

			</div>