		<h4><?php _e( 'Admin page tabs', 'caldera-yellowstone' ); ?></h4>
		
		<div {{#is mode value="multi"}}{{else}}style="display:none;"{{/is}}>
			<div class="caldera-config-group">
				<label><?php _e( 'General Name', 'caldera-yellowstone' ); ?></label>
				<input type="text" name="general_settings[name]" value="{{#if general_settings/name}}{{general_settings/name}}{{else}}<?php _e( 'General', 'caldera-yellowstone' ); ?>{{/if}}" required style="width: 220px;">
			</div>
			<div class="caldera-config-group">
				<label><?php _e( 'General Description', 'caldera-yellowstone' ); ?></label>
				<input type="text" name="general_settings[description]" value="{{#if general_settings/description}}{{general_settings/description}}{{else}}<?php _e( 'General Settings', 'caldera-yellowstone' ); ?>{{/if}}" required style="width: 220px;">
			</div>
			<div class="caldera-config-group">
				<label><?php _e( 'Location', 'caldera-yellowstone' ); ?></label>
				<select name="general_settings[location]">
					<option value="sub" {{#is general_settings/location value="sub"}}selected="selected"{{/is}}><?php _e('Lower', 'caldera-yellowstone' ); ?></option>
					<option value="main" {{#is general_settings/location value="main"}}selected="selected"{{/is}}><?php _e('Main', 'caldera-yellowstone' ); ?></option>
				</select>
			</div>
			<br>
		</div>
		<button class="button button-small wp-baldrick" data-request="cye_get_default_setting" data-script="new-tab" type="button" ><?php _e( 'Add Tab', 'caldera-yellowstone' ); ?></button>
		<hr>
		
			<div class="caldera-split-panel" style="position:relative;">
				<div class="caldera-side-list">
				{{#each tab}}
					<div class="caldera-tab-item caldera-card-item" style="display:block;">

						<input type="hidden" name="tab[{{_id}}][_id]" value="{{_id}}">
						<div class="caldera-card-content">
							<div class="caldera-config-group">
								<label style="width:90px;"><?php _e( 'Name', 'caldera-yellowstone' ); ?></label>
								<input type="text" name="tab[{{_id}}][name]" value="{{name}}" data-sync="#tab_title_{{_id}}" required style="width: 238px;">
							</div>
							<div class="caldera-config-group">
								<label style="width:90px;"><?php _e( 'Description', 'caldera-yellowstone' ); ?></label>
								<input type="text" name="tab[{{_id}}][description]" value="{{description}}" data-sync="#tab_desc_{{_id}}" required style="width: 238px;">
							</div>
							<div class="caldera-config-group" style="width: 85px; float: left;">
								<label style="width:85px;"><?php _e( 'Slug', 'caldera-yellowstone' ); ?></label>
								<input type="text" name="tab[{{_id}}][slug]" value="{{slug}}" data-sync="#tab_slug_{{_id}}" data-format="slug" required style="width: 100%;">
							</div>
							<div class="caldera-config-group" style="float: left; margin-left: 8px; width: 104px;">
								<label style="width: 100%;"><?php _e( 'Location', 'caldera-yellowstone' ); ?></label>
								<select name="tab[{{_id}}][location]" style="width: 100%">
									<option value="sub" {{#is location value="sub"}}selected="selected"{{/is}}><?php _e('Lower', 'caldera-yellowstone' ); ?></option>
									<option value="main" {{#is location value="main"}}selected="selected"{{/is}}><?php _e('Main', 'caldera-yellowstone' ); ?></option>
								</select>
							</div>
							<div class="caldera-config-group" style="width: 126px; float: left; margin-left: 8px;">
								<label style="width: 100%;"><?php _e( 'Type', 'caldera-yellowstone' ); ?></label>
								<select name="tab[{{_id}}][type]" data-live-sync="true" style="width: 100%">
									<option value="blank" {{#is type value="blank"}}selected="selected"{{/is}}><?php _e('Blank Template', 'caldera-yellowstone' ); ?></option>
									<option value="code-editor" {{#is type value="code-editor"}}selected="selected"{{/is}}><?php _e('Code Editor', 'caldera-yellowstone' ); ?></option>
									<option value="config-panel" {{#is type value="config-panel"}}selected="selected"{{/is}}><?php _e('Config Panel', 'caldera-yellowstone' ); ?></option>
									<option value="grouped-config" {{#is type value="grouped-config"}}selected="selected"{{/is}}><?php _e('Grouped Config', 'caldera-yellowstone' ); ?></option>									
									<option value="image-gallery" {{#is type value="image-gallery"}}selected="selected"{{/is}}><?php _e('Image Gallery', 'caldera-yellowstone' ); ?></option>									
									<option value="fieldgroups-panel" {{#is type value="fieldgroups-panel"}}selected="selected"{{/is}}><?php _e('Fields & Groups Panel', 'caldera-yellowstone' ); ?></option>
									<option value="grid" {{#is type value="grid"}}selected="selected"{{/is}}><?php _e('Layout Grid', 'caldera-yellowstone' ); ?></option>
									<option value="map" {{#is type value="map"}}selected="selected"{{/is}}><?php _e('Map', 'caldera-yellowstone' ); ?></option>
									<option value="query-filter" {{#is type value="query-filter"}}selected="selected"{{/is}}><?php _e('Query Filter', 'caldera-yellowstone' ); ?></option>
									<option value="search-form" {{#is type value="search-form"}}selected="selected"{{/is}}><?php _e('Search Form', 'caldera-yellowstone' ); ?></option>
									<option value="visual-editor" {{#is type value="visual-editor"}}selected="selected"{{/is}}><?php _e('Visual Editor', 'caldera-yellowstone' ); ?></option>
									<option value="code-editor" {{#is type value="code-editor"}}selected="selected"{{/is}}><?php _e('Code Editor', 'caldera-yellowstone' ); ?></option>
									<option value="listing" {{#is type value="listing"}}selected="selected"{{/is}}><?php _e('List Panel Template', 'caldera-yellowstone' ); ?></option>
									<option value="db-query" {{#is type value="db-query"}}selected="selected"{{/is}}><?php _e('DB Query', 'caldera-yellowstone' ); ?></option>
									<option value="db-connector" {{#is type value="db-connector"}}selected="selected"{{/is}}><?php _e('DB Connector', 'caldera-yellowstone' ); ?></option>
								</select>
							</div>
							<div class="clear"></div>
							<button type="button" class="button button-small wp-baldrick" data-request="cye_get_default_setting" data-id="{{_id}}" data-script="remove-tab-panel"style="padding: 0px; margin: 3px 0px; position: absolute; left: 14px; bottom: 12px;"><span class="dashicons dashicons-no-alt" style="padding: 0px; margin: 0px; line-height: 23px;"></span></button>
							<label class="button button-small{{#is ../tab_config_open value="@key"}} button-primary{{/is}}" style="padding: 0px; margin: 3px 0px; position: absolute; left: 14px; top: 6px;" for="edit_{{_id}}_tab"><span class="dashicons dashicons-edit" style="padding: 0px; margin: 0px; line-height: 23px;"></span></label>
							<input id="edit_{{_id}}_tab" type="radio" name="tab_config_open" value="{{_id}}" {{#is ../tab_config_open value="@key"}}checked="checked"{{/is}} data-live-sync="true" style="display:none;">
						</div>
					</div>
				{{/each}}
				</div>
				
				<div class="caldera-main-list">
				{{#each tab}}
					<div class="caldera-card-item" style="display:{{#is ../tab_config_open value="@key"}}block{{else}}none{{/is}};">
						<span class="dashicons dashicons-admin-generic caldera-card-icon" style="color:#a1a1a1;"></span>
						<div class="caldera-card-content">
							<h4 id="tab_title_{{_id}}">{{name}}</h4>
							<div id="tab_desc_{{_id}}" class="description">{{description}}</div>
							<hr>
							{{#is type value="code-editor"}}
							<div class="caldera-config-group">
								<label style="width: 100px;"><?php _e( 'Editor Mode', 'caldera-yellowstone' ); ?></label>
								<input type="text" name="tab[{{_id}}][mode]" value="{{#if mode}}{{mode}}{{else}}text/html{{/if}}" required style="width: 224px;">
							</div>
							{{/is}}
							{{#is type value="image-gallery"}}
	
								<div class="caldera-config-group">
									<label style="width: 130px;"><?php _e( 'Modal Title', 'caldera-yellowstone' ); ?></label>
									<input type="text" name="tab[{{_id}}][title]" value="{{#if title}}{{title}}{{else}}Select Images{{/if}}" required style="width: 194px;">
								</div>
								<div class="caldera-config-group">
									<label style="width: 130px;"><?php _e( 'Add Text', 'caldera-yellowstone' ); ?></label>
									<input type="text" name="tab[{{_id}}][add]" value="{{#if add}}{{add}}{{else}}Add Images{{/if}}" required style="width: 194px;">
								</div>						
								<div class="caldera-config-group">
									<label style="width: 130px;"><?php _e( 'Remove Confirm', 'caldera-yellowstone' ); ?></label>
									<input type="text" name="tab[{{_id}}][confirm]" value="{{#if confirm}}{{confirm}}{{/if}}" style="width: 194px;">
								</div>
								<div class="caldera-config-group">
									<label style="width: 130px;"><?php _e( 'No Images Text', 'caldera-yellowstone' ); ?></label>
									<input type="text" name="tab[{{_id}}][no_images]" value="{{#if no_images}}{{no_images}}{{else}}No Images Selected{{/if}}" required style="width: 194px;">
								</div>	
								<div class="caldera-config-group">
									<label style="width: 130px;"><?php _e( 'Tile Width', 'caldera-yellowstone' ); ?></label>
									<input type="number" name="tab[{{_id}}][width]" value="{{#if width}}{{width}}{{else}}150{{/if}}" required style="width: 80px;">px
								</div>
								<div class="caldera-config-group">
									<label style="width: 130px;"><?php _e( 'Tile Height', 'caldera-yellowstone' ); ?></label>
									<input type="number" name="tab[{{_id}}][height]" value="{{#if height}}{{height}}{{else}}150{{/if}}" required style="width: 80px;">px
								</div>	
								<div class="caldera-config-group">
									<label style="width: 130px;"><?php _e( 'Tile Padding', 'caldera-yellowstone' ); ?></label>
									<input type="number" name="tab[{{_id}}][padding]" value="{{#if padding}}{{padding}}{{else}}12{{/if}}" required style="width: 80px;">px
								</div>
								<div class="caldera-config-group">
									<label style="width: 130px;"><?php _e( 'Tile Spacing', 'caldera-yellowstone' ); ?></label>
									<input type="number" name="tab[{{_id}}][margin]" value="{{#if margin}}{{margin}}{{else}}6{{/if}}" required style="width: 80px;">px
								</div>
								<div class="caldera-config-group">
									<label style="width: 130px;"><?php _e( 'Tile Radius', 'caldera-yellowstone' ); ?></label>
									<input type="number" name="tab[{{_id}}][radius]" value="{{#if radius}}{{radius}}{{else}}3{{/if}}" required style="width: 80px;">px
								</div>
								<div class="caldera-config-group">
									<label style="width: 130px;"><?php _e( 'Tile Background', 'caldera-yellowstone' ); ?></label>
									<input type="text" class="color-field" name="tab[{{_id}}][tile_color]" value="{{#if tile_color}}{{tile_color}}{{else}}#ffffff{{/if}}">
								</div>								
								<div class="caldera-config-group">
									<label style="width: 130px;"><?php _e( 'Panel Padding', 'caldera-yellowstone' ); ?></label>
									<input type="number" name="tab[{{_id}}][box_padding]" value="{{#if box_padding}}{{box_padding}}{{else}}12{{/if}}" required style="width: 80px;">px
								</div>	
																							<div class="caldera-config-group">
									<label style="width: 130px;"><?php _e( 'Icon Padding', 'caldera-yellowstone' ); ?></label>
									<input type="number" name="tab[{{_id}}][icon_padding]" value="{{#if icon_padding}}{{icon_padding}}{{else}}2{{/if}}" required style="width: 80px;">px
								</div>	
								<div class="caldera-config-group">
									<label style="width: 130px;"><?php _e( 'Icon Radius', 'caldera-yellowstone' ); ?></label>
									<input type="number" name="tab[{{_id}}][icon_radius]" value="{{#if icon_radius}}{{icon_radius}}{{else}}3{{/if}}" required style="width: 80px;">px
								</div>	
								<div class="caldera-config-group">
									<label style="width: 130px;"><?php _e( 'Border Width', 'caldera-yellowstone' ); ?></label>
									<input type="number" name="tab[{{_id}}][border_width]" value="{{#if border_width}}{{border_width}}{{else}}1{{/if}}" required style="width: 80px;">px
								</div>
								<div class="caldera-config-group">
									<label style="width: 130px;"><?php _e( 'Border Color', 'caldera-yellowstone' ); ?></label>
									<input type="text" class="color-field" name="tab[{{_id}}][border_color]" value="{{#if border_color}}{{border_color}}{{else}}#cfcfcf{{/if}}">
								</div>
								<div class="caldera-config-group">
									<label style="width: 130px;"><?php _e( 'Image Radius', 'caldera-yellowstone' ); ?></label>
									<input type="number" name="tab[{{_id}}][image_radius]" value="{{#if image_radius}}{{image_radius}}{{else}}3{{/if}}" required style="width: 80px;">px
								</div>								
								<div class="caldera-config-group">
									<label style="width: 130px;"><?php _e( 'Image Container', 'caldera-yellowstone' ); ?></label>
									<select name="tab[{{_id}}][background_size]" data-live-sync="true" style="width: 194px">
										<option value="cover" {{#is background_size value="cover"}}selected="selected"{{/is}}><?php _e('Cover', 'caldera-yellowstone' ); ?></option>
										<option value="contain" {{#is background_size value="contain"}}selected="selected"{{/is}}><?php _e('Contain', 'caldera-yellowstone' ); ?></option>
									</select>
								</div>
								<div class="caldera-config-group">
									<label style="width: 130px;"><?php _e( 'Image Background', 'caldera-yellowstone' ); ?></label>
									<input type="text" class="color-field" name="tab[{{_id}}][contain_color]" value="{{#if contain_color}}{{contain_color}}{{else}}#333333{{/if}}">
								</div>

								{{#unless field}}
								<p class="description"><?php _e( 'No fields setup yet.', 'caldera-yellowstone' ); ?></p>
								{{/unless}}
								{{#each field}}
								<div class="caldera-card-sub-item">
									
									<div class="caldera-config-group">
										<label for="{{../id}}-{{_id}}-name" style="width: 100px;"><?php _e( 'Name', 'caldera-yellowstone' ); ?></label>
										<input type="text" name="tab[{{../_id}}][field][{{_id}}][name]" value="{{name}}" id="{{../id}}-{{_id}}-name" required style="width: 174px;" data-sync="#{{../id}}-{{_id}}-slug">
									</div>
									<div class="caldera-config-group">
										<label for="{{../id}}-{{_id}}-slug" style="width: 100px;"><?php _e( 'Slug', 'caldera-yellowstone' ); ?></label>
										<input type="text" name="tab[{{../_id}}][field][{{_id}}][slug]" value="{{slug}}" id="{{../id}}-{{_id}}-slug" data-format="slug" data-master="#{{../id}}-{{_id}}-name" required style="width: 174px;">
									</div>
									<div class="caldera-config-group">
										<label for="{{../id}}-{{_id}}-description" style="width: 100px;"><?php _e( 'Description', 'caldera-yellowstone' ); ?></label>
										<input type="text" name="tab[{{../_id}}][field][{{_id}}][description]" value="{{description}}" id="{{../id}}-{{_id}}-description" style="width: 174px;">
									</div>
									<div class="caldera-config-group">
										<label for="{{../id}}-{{_id}}-required" style="width: 100px;">&nbsp;</label>
										<label style="display: inline-block; margin: 0px;"><input type="checkbox" name="tab[{{../_id}}][field][{{_id}}][required]" value="1" {{#if required}}checked="checked"{{/if}}> <?php _e('Required', 'caldera-yellowstone'); ?></label>								
									</div>
									<div class="caldera-config-group">
										<label for="{{../id}}-{{_id}}-type" style="width: 100px;"><?php _e( 'Type', 'caldera-yellowstone' ); ?></label>
										<select data-live-sync="true" name="tab[{{../_id}}][field][{{_id}}][type]" style="width: 174px;">
											<option value="text" {{#is type value="text"}}selected="selected"{{/is}}><?php _e('Text', 'caldera-yellowstone' ); ?></option>
											<option value="color" {{#is type value="color"}}selected="selected"{{/is}}><?php _e('Color Picker', 'caldera-yellowstone' ); ?></option>
											<option value="range-slider" {{#is type value="range-slider"}}selected="selected"{{/is}}><?php _e('Range Slider', 'caldera-yellowstone' ); ?></option>
											<option value="textarea" {{#is type value="textarea"}}selected="selected"{{/is}}><?php _e('Paragraph', 'caldera-yellowstone' ); ?></option>
											<option value="post-type-selector" {{#is type value="post-type-selector"}}selected="selected"{{/is}}><?php _e('Post Type Selector (single)', 'caldera-yellowstone' ); ?></option>
											<option value="post-type-selector-multi" {{#is type value="post-type-selector-multi"}}selected="selected"{{/is}}><?php _e('Post Type Selector (Multiple)', 'caldera-yellowstone' ); ?></option>
											<option value="select2" {{#is type value="select2"}}selected="selected"{{/is}}><?php _e('Select2', 'caldera-yellowstone' ); ?></option>
											<option value="roles" {{#is type value="roles"}}selected="selected"{{/is}}><?php _e('User Roles Checklist', 'caldera-yellowstone' ); ?></option>
											<option value="visual-editor" {{#is type value="visual-editor"}}selected="selected"{{/is}}><?php _e('Visual Editor', 'caldera-yellowstone' ); ?></option>
											<option value="code-editor" {{#is type value="code-editor"}}selected="selected"{{/is}}><?php _e('Code Editor', 'caldera-yellowstone' ); ?></option>
										</select>				
									</div>

									<div style="display:{{#is type value="range-slider"}}block{{else}}none{{/is}};">
										<div class="caldera-config-group">
											<label for="{{../id}}-{{_id}}-range_min_value" style="width: 100px;"><?php _e( 'Min Value', 'caldera-yellowstone' ); ?></label>
											<input type="text" name="tab[{{../_id}}][field][{{_id}}][range_min_value]" value="{{range_min_value}}" id="{{../id}}-{{_id}}-range_min_value" style="width: 174px;">
										</div>
										<div class="caldera-config-group">
											<label for="{{../id}}-{{_id}}-range_max_value" style="width: 100px;"><?php _e( 'Max Value', 'caldera-yellowstone' ); ?></label>
											<input type="text" name="tab[{{../_id}}][field][{{_id}}][range_max_value]" value="{{range_max_value}}" id="{{../id}}-{{_id}}-range_max_value" style="width: 174px;">
										</div>
										<div class="caldera-config-group">
											<label for="{{../id}}-{{_id}}-range_suffix" style="width: 100px;"><?php _e( 'Value Suffix', 'caldera-yellowstone' ); ?></label>
											<input type="text" name="tab[{{../_id}}][field][{{_id}}][range_suffix]" value="{{range_suffix}}" id="{{../id}}-{{_id}}-range_suffix" style="width: 174px;">
										</div>
										<div class="caldera-config-group">
											<label for="{{../id}}-{{_id}}-range_step" style="width: 100px;"><?php _e( 'Step', 'caldera-yellowstone' ); ?></label>
											<input type="text" name="tab[{{../_id}}][field][{{_id}}][range_step]" value="{{range_step}}" id="{{../id}}-{{_id}}-range_step" style="width: 174px;">
										</div>
									</div>


									<button type="button" class="button button-small" style="padding: 0px; margin: 3px 0px; position: absolute; right: 8px; top: 6px;" data-remove-parent=".caldera-card-sub-item"><span class="dashicons dashicons-no-alt" style="padding: 0px; margin: 0px; line-height: 23px;"></span></button>
								</div>
								{{/each}}
								<hr>
								<button class="button button-small wp-baldrick" data-request="cye_get_default_setting" data-id="{{_id}}" data-script="add-config-panel-field" type="button" style=""><?php _e( 'Add Field', 'caldera-yellowstone' ); ?></button>								
							{{/is}}
							{{#is type value="config-panel"}}
							<button class="button button-small wp-baldrick" data-request="cye_get_default_setting" data-id="{{_id}}" data-script="add-config-panel-field" type="button" style="position: absolute; top: 12px; right: 12px;"><?php _e( 'Add Field', 'caldera-yellowstone' ); ?></button>
							<label style="display: block; margin: 0px 0px 8px;"><input type="checkbox" name="tab[{{../_id}}][is_repeatable]" data-live-sync="true" value="1" {{#if is_repeatable}}checked="checked"{{/if}}> <?php _e('Repeatable Group', 'caldera-yellowstone'); ?></label>
							
							<div {{#unless is_repeatable}}style="display:none;"{{/unless}}>
								<div class="caldera-config-group">
									<label for="{{../id}}-{{_id}}-add_text" style="width: 100px;"><?php _e( 'Add Text', 'caldera-yellowstone' ); ?></label>
									<input type="text" name="tab[{{../_id}}][add_text]" value="{{#if add_text}}{{add_text}}{{else}}<?php _e( 'Add New', 'caldera-yellowstone' ); ?>{{/if}}" id="{{../id}}-{{_id}}-add_text" required style="width: 228px;">
								</div>
								<div class="caldera-config-group">
									<label for="{{../id}}-{{_id}}-empty_text" style="width: 100px;"><?php _e( 'Empty Text', 'caldera-yellowstone' ); ?></label>
									<input type="text" name="tab[{{../_id}}][empty_text]" value="{{#if empty_text}}{{empty_text}}{{else}}<?php _e( 'No Items', 'caldera-yellowstone' ); ?>{{/if}}" id="{{../id}}-{{_id}}-group_empty" required style="width: 228px;">
								</div>
								<div class="caldera-config-group">
									<label for="{{../id}}-{{_id}}-dashicon" style="width: 100px;"><?php _e( 'Dashicon', 'caldera-yellowstone' ); ?></label>
									<input type="text" data-live-sync="true" name="tab[{{../_id}}][dashicon]" value="{{#if dashicon}}{{dashicon}}{{else}}dashicons-admin-generic{{/if}}" id="{{../id}}-{{_id}}-dashicon" required style="width: 199px;"><button style="padding: 3px;" class="button dashicons-picker" type="button" data-target="#{{../id}}-{{_id}}-dashicon" > <span class="dashicons {{#if dashicon}}{{dashicon}}{{else}}dashicons-admin-generic{{/if}}"></span></button>

								</div>
							</div>
							{{#unless field}}
							<p class="description"><?php _e( 'No fields setup yet.', 'caldera-yellowstone' ); ?></p>
							{{/unless}}
							{{#each field}}
							<div class="caldera-card-sub-item">
								<input type="hidden" name="tab[{{../_id}}][field][{{_id}}][_id]" value="{{_id}}">
								<div class="caldera-config-group">
									<label for="{{../id}}-{{_id}}-name" style="width: 100px;"><?php _e( 'Name', 'caldera-yellowstone' ); ?></label>
									<input type="text" name="tab[{{../_id}}][field][{{_id}}][name]" value="{{name}}" id="{{../id}}-{{_id}}-name" required style="width: 174px;">
								</div>
								<div class="caldera-config-group">
									<label for="{{../id}}-{{_id}}-slug" style="width: 100px;"><?php _e( 'Slug', 'caldera-yellowstone' ); ?></label>
									<input type="text" name="tab[{{../_id}}][field][{{_id}}][slug]" value="{{slug}}" id="{{../id}}-{{_id}}-slug" data-format="slug" data-master="#{{../id}}-{{_id}}-name" required style="width: 174px;">
								</div>
								<div class="caldera-config-group">
									<label for="{{../id}}-{{_id}}-description" style="width: 100px;"><?php _e( 'Description', 'caldera-yellowstone' ); ?></label>
									<input type="text" name="tab[{{../_id}}][field][{{_id}}][description]" value="{{description}}" id="{{../id}}-{{_id}}-description" style="width: 174px;">
								</div>
								<div class="caldera-config-group">
									<label for="{{../id}}-{{_id}}-required" style="width: 100px;">&nbsp;</label>
									<label style="display: inline-block; margin: 0px;"><input type="checkbox" name="tab[{{../_id}}][field][{{_id}}][required]" value="1" {{#if required}}checked="checked"{{/if}}> <?php _e('Required', 'caldera-yellowstone'); ?></label>								
								</div>
								<div class="caldera-config-group">
									<label for="{{../id}}-{{_id}}-type" style="width: 100px;"><?php _e( 'Type', 'caldera-yellowstone' ); ?></label>
									<select data-live-sync="true" name="tab[{{../_id}}][field][{{_id}}][type]" style="width: 174px;">
										<option value="text" {{#is type value="text"}}selected="selected"{{/is}}><?php _e('Text', 'caldera-yellowstone' ); ?></option>
										<option value="color" {{#is type value="color"}}selected="selected"{{/is}}><?php _e('Color Picker', 'caldera-yellowstone' ); ?></option>
										<option value="image-picker" {{#is type value="image-picker"}}selected="selected"{{/is}}><?php _e('Image Picker', 'caldera-yellowstone' ); ?></option>
										<option value="range-slider" {{#is type value="range-slider"}}selected="selected"{{/is}}><?php _e('Range Slider', 'caldera-yellowstone' ); ?></option>
										<option value="textarea" {{#is type value="textarea"}}selected="selected"{{/is}}><?php _e('Paragraph', 'caldera-yellowstone' ); ?></option>
										<option value="post-type-selector" {{#is type value="post-type-selector"}}selected="selected"{{/is}}><?php _e('Post Type Selector (single)', 'caldera-yellowstone' ); ?></option>
										<option value="post-type-selector-multi" {{#is type value="post-type-selector-multi"}}selected="selected"{{/is}}><?php _e('Post Type Selector (Multiple)', 'caldera-yellowstone' ); ?></option>
										<option value="select2" {{#is type value="select2"}}selected="selected"{{/is}}><?php _e('Select2', 'caldera-yellowstone' ); ?></option>
										<option value="roles" {{#is type value="roles"}}selected="selected"{{/is}}><?php _e('User Roles Checklist', 'caldera-yellowstone' ); ?></option>
										<option value="visual-editor" {{#is type value="visual-editor"}}selected="selected"{{/is}}><?php _e('Visual Editor', 'caldera-yellowstone' ); ?></option>
										<option value="code-editor" {{#is type value="code-editor"}}selected="selected"{{/is}}><?php _e('Code Editor', 'caldera-yellowstone' ); ?></option>
									</select>				
								</div>

								<div style="display:{{#is type value="range-slider"}}block{{else}}none{{/is}};">
									<div class="caldera-config-group">
										<label for="{{../id}}-{{_id}}-range_min_value" style="width: 100px;"><?php _e( 'Min Value', 'caldera-yellowstone' ); ?></label>
										<input type="text" name="tab[{{../_id}}][field][{{_id}}][range_min_value]" value="{{range_min_value}}" id="{{../id}}-{{_id}}-range_min_value" style="width: 174px;">
									</div>
									<div class="caldera-config-group">
										<label for="{{../id}}-{{_id}}-range_max_value" style="width: 100px;"><?php _e( 'Max Value', 'caldera-yellowstone' ); ?></label>
										<input type="text" name="tab[{{../_id}}][field][{{_id}}][range_max_value]" value="{{range_max_value}}" id="{{../id}}-{{_id}}-range_max_value" style="width: 174px;">
									</div>
									<div class="caldera-config-group">
										<label for="{{../id}}-{{_id}}-range_suffix" style="width: 100px;"><?php _e( 'Value Suffix', 'caldera-yellowstone' ); ?></label>
										<input type="text" name="tab[{{../_id}}][field][{{_id}}][range_suffix]" value="{{range_suffix}}" id="{{../id}}-{{_id}}-range_suffix" style="width: 174px;">
									</div>
									<div class="caldera-config-group">
										<label for="{{../id}}-{{_id}}-range_step" style="width: 100px;"><?php _e( 'Step', 'caldera-yellowstone' ); ?></label>
										<input type="text" name="tab[{{../_id}}][field][{{_id}}][range_step]" value="{{range_step}}" id="{{../id}}-{{_id}}-range_step" style="width: 174px;">
									</div>
								</div>


								<button type="button" class="button button-small" style="padding: 0px; margin: 3px 0px; position: absolute; right: 8px; top: 6px;" data-remove-parent=".caldera-card-sub-item"><span class="dashicons dashicons-no-alt" style="padding: 0px; margin: 0px; line-height: 23px;"></span></button>
							</div>
							{{/each}}
							{{/is}}

							{{#is type value="fieldgroups-panel"}}
							<label style="display: block; margin: 0px 0px 8px;"><input type="checkbox" name="tab[{{../_id}}][allow_repeat]" data-live-sync="true" value="1" {{#if allow_repeat}}checked="checked"{{/if}}> <?php _e('Allow Repeatable Groups', 'caldera-yellowstone'); ?></label>

							<div class="caldera-config-group">
								<label for="{{../id}}-{{_id}}-group_type" style="width: 100px;"><?php _e( 'Type Label', 'caldera-yellowstone' ); ?></label>
								<input type="text" name="tab[{{../_id}}][group_type]" value="{{#if group_type}}{{group_type}}{{else}}<?php _e( 'Group', 'caldera-yellowstone' ); ?>{{/if}}" id="{{../id}}-{{_id}}-group_type" required style="width: 228px;">
							</div>
							<div class="caldera-config-group">
								<label for="{{../id}}-{{_id}}-delete_text" style="width: 100px;"><?php _e( 'Delete Text', 'caldera-yellowstone' ); ?></label>
								<input type="text" name="tab[{{../_id}}][delete_text]" value="{{#if delete_text}}{{delete_text}}{{else}}<?php _e( 'Delete', 'caldera-yellowstone' ); ?>{{/if}}" id="{{../id}}-{{_id}}-group_empty" required style="width: 228px;">
							</div>
							<div class="caldera-config-group">
								<label for="{{../id}}-{{_id}}-delete_confirm" style="width: 100px;"><?php _e( 'Delete Confirm', 'caldera-yellowstone' ); ?></label>
								<input type="text" name="tab[{{../_id}}][delete_confirm]" value="{{#if delete_confirm}}{{delete_confirm}}{{else}}<?php _e( 'Are you sure you want to remove this group?', 'caldera-yellowstone' ); ?>{{/if}}" id="{{../id}}-{{_id}}-delete_confirm" required style="width: 228px;">
							</div>

							{{/is}}

							{{#is type value="blank"}}
								<?php _e('Blank template has no config options', 'caldera-yellowstone'); ?>
							{{/is}}
							{{#is type value="query-filter"}}
								<?php _e('Query filter has no config options', 'caldera-yellowstone'); ?>
							{{/is}}
							{{#is type value="search-form"}}
																
								<div class="caldera-config-group">
									<label for="{{../id}}-{{_id}}-query-bind" style="width: 100px;"><?php _e( 'Bind to Query', 'caldera-yellowstone' ); ?></label>
									<select name="tab[{{../_id}}][query_bind]" style="width: 174px;">
										{{#each @root/tab}}
										{{#is type value="query-filter"}}
										<option value="{{slug}}" {{#is ../../query_bind value=slug}}selected="selected"{{/is}}>{{name}}</option>
										{{/is}}
										{{/each}}
									</select>				
								</div>
							{{/is}}

							
							{{#is type value="grid"}}
								<button class="button button-small wp-baldrick" data-request="cye_get_default_setting" data-id="{{_id}}" data-script="add-config-panel-element" type="button" style="position: absolute; top: 12px; right: 12px;"><?php _e( 'Add Element', 'caldera-yellowstone' ); ?></button>
								
									<div class="caldera-config-group">
										<label for="{{../id}}-{{_id}}-delete_confirm" style="width: 100px;"><?php _e( 'Delete Confirm', 'caldera-yellowstone' ); ?></label>
										<input type="text" name="tab[{{../_id}}][delete_confirm]" value="{{#if delete_confirm}}{{delete_confirm}}{{/if}}" id="{{../id}}-{{_id}}-delete_confirm" style="width: 228px;">
										<p class="description"><?php _e( 'Confirm message for removing a row.', 'caldera-yellowstone' ); ?></p>
									</div>


								{{#each element}}
								<div class="caldera-card-sub-item">
									<input type="hidden" name="tab[{{../_id}}][element][{{_id}}][_id]" value="{{_id}}">
									<div class="caldera-config-group">										
										<label for="{{../id}}-{{_id}}-grid_item" style="width: 100px;"><?php _e( 'Item Name', 'caldera-yellowstone' ); ?></label>
										<input type="text" name="tab[{{../_id}}][element][{{_id}}][grid_item]" value="{{grid_item}}" data-sync="#{{../id}}-{{_id}}-grid_item_slug" id="{{../id}}-{{_id}}-grid_item" style="width: 168px;" required>
									</div>

									<div class="caldera-config-group">										
										<label for="{{../id}}-{{_id}}-grid_item" style="width: 100px;"><?php _e( 'Item Slug', 'caldera-yellowstone' ); ?></label>
										<input type="text" name="tab[{{../_id}}][element][{{_id}}][grid_item_slug]" value="{{grid_item_slug}}" id="{{../id}}-{{_id}}-grid_item_slug" style="width: 168px;" data-master="#{{../id}}-{{_id}}-grid_item" data-format="slug" required>
									</div>

									<div class="caldera-config-group">
										<label for="{{../id}}-{{_id}}-delete_confirm" style="width: 100px;"><?php _e( 'Delete Confirm', 'caldera-yellowstone' ); ?></label>
										<input type="text" name="tab[{{../_id}}][element][{{_id}}][delete_confirm]" value="{{#if delete_confirm}}{{delete_confirm}}{{/if}}" id="{{../id}}-{{_id}}-delete_confirm" style="width: 168px;">
									</div>

									<div class="caldera-config-group">
										<label for="{{../id}}-{{_id}}-insert_text" style="width: 100px;"><?php _e( 'Insert Text', 'caldera-yellowstone' ); ?></label>
										<input type="text" name="tab[{{../_id}}][element][{{_id}}][insert_text]" value="{{#if insert_text}}{{insert_text}}{{else}}<?php _e( 'Insert', 'caldera-yellowstone' ); ?>{{/if}}" id="{{../id}}-{{_id}}-insert_text" style="width: 168px;">
									</div>
									<div class="caldera-config-group">
										<label for="{{../id}}-{{_id}}-update_text" style="width: 100px;"><?php _e( 'Update Text', 'caldera-yellowstone' ); ?></label>
										<input type="text" name="tab[{{../_id}}][element][{{_id}}][update_text]" value="{{#if update_text}}{{update_text}}{{else}}<?php _e( 'Update', 'caldera-yellowstone' ); ?>{{/if}}" id="{{../id}}-{{_id}}-update_text" style="width: 168px;">
									</div>
									
									<label style="display: block; margin: 0 0 8px 104px;"><input type="checkbox" name="tab[{{../_id}}][element][{{_id}}][code_box]" value="1" {{#if code_box}}checked="checked"{{/if}}> <?php _e('Code Editor', 'caldera-yellowstone'); ?></label>
									
									<button type="button" class="button button-small" style="padding: 0px; margin: 3px 0px; position: absolute; right: 8px; top: 6px;" data-remove-parent=".caldera-card-sub-item"><span class="dashicons dashicons-no-alt" style="padding: 0px; margin: 0px; line-height: 23px;"></span></button>

								</div>
								{{/each}}

							{{/is}}
							{{#is type value="map"}}
								<?php _e('map template has no config options', 'caldera-yellowstone'); ?>
							{{/is}}

							{{#is type value="grouped-config"}}

								<div class="caldera-config-group">
									<label for="{{../id}}-{{_id}}-group_kind" style="width: 100px;"><?php _e( 'Kind', 'caldera-yellowstone' ); ?></label>
									<input type="text" name="tab[{{../_id}}][group_kind]" value="{{#if group_kind}}{{group_kind}}{{else}}Group{{/if}}" id="{{../id}}-{{_id}}-group_kind" style="width: 228px;">
									<p class="description"><?php _e( 'Group kind. e.g Group, Category etc.', 'caldera-yellowstone' ); ?></p>
								</div>
								<div class="caldera-config-group">
									<label for="{{../id}}-{{_id}}-group_kind_plural" style="width: 100px;"><?php _e( 'Kind Plural', 'caldera-yellowstone' ); ?></label>
									<input type="text" name="tab[{{../_id}}][group_kind_plural]" value="{{#if group_kind_plural}}{{group_kind_plural}}{{else}}Groups{{/if}}" id="{{../id}}-{{_id}}-group_kind_plural" style="width: 228px;">
									<p class="description"><?php _e( 'Group kind. e.g Groups, Categories etc.', 'caldera-yellowstone' ); ?></p>
								</div>
								<div class="caldera-config-group">
									<label for="{{../id}}-{{_id}}-delete_confirm" style="width: 100px;"><?php _e( 'Delete Confirm', 'caldera-yellowstone' ); ?></label>
									<input type="text" name="tab[{{../_id}}][delete_confirm]" value="{{#if delete_confirm}}{{delete_confirm}}{{/if}}" id="{{../id}}-{{_id}}-delete_confirm" style="width: 228px;">
									<p class="description"><?php _e( 'Confirm message for removing a group.', 'caldera-yellowstone' ); ?></p>
								</div>

								{{#unless field}}
								<p class="description"><?php _e( 'No fields setup yet.', 'caldera-yellowstone' ); ?></p>
								{{/unless}}
								{{#each field}}
								<div class="caldera-card-sub-item">
									<input type="hidden" name="tab[{{../_id}}][field][{{_id}}][_id]" value="{{_id}}">
									<div class="caldera-config-group">
										<label for="{{../id}}-{{_id}}-name" style="width: 100px;"><?php _e( 'Name', 'caldera-yellowstone' ); ?></label>
										<input type="text" name="tab[{{../_id}}][field][{{_id}}][name]" value="{{name}}" id="{{../id}}-{{_id}}-name" required style="width: 174px;" data-sync="#{{../id}}-{{_id}}-slug">
									</div>
									<div class="caldera-config-group">
										<label for="{{../id}}-{{_id}}-slug" style="width: 100px;"><?php _e( 'Slug', 'caldera-yellowstone' ); ?></label>
										<input type="text" name="tab[{{../_id}}][field][{{_id}}][slug]" value="{{slug}}" id="{{../id}}-{{_id}}-slug" data-format="slug" data-master="#{{../id}}-{{_id}}-name" required style="width: 174px;">
									</div>
									<div class="caldera-config-group">
										<label for="{{../id}}-{{_id}}-description" style="width: 100px;"><?php _e( 'Description', 'caldera-yellowstone' ); ?></label>
										<input type="text" name="tab[{{../_id}}][field][{{_id}}][description]" value="{{description}}" id="{{../id}}-{{_id}}-description" style="width: 174px;">
									</div>
									<div class="caldera-config-group">
										<label for="{{../id}}-{{_id}}-required" style="width: 100px;">&nbsp;</label>
										<label style="display: inline-block; margin: 0px;"><input type="checkbox" name="tab[{{../_id}}][field][{{_id}}][required]" value="1" {{#if required}}checked="checked"{{/if}}> <?php _e('Required', 'caldera-yellowstone'); ?></label>								
									</div>
									<div class="caldera-config-group">
										<label for="{{../id}}-{{_id}}-type" style="width: 100px;"><?php _e( 'Type', 'caldera-yellowstone' ); ?></label>
										<select data-live-sync="true" name="tab[{{../_id}}][field][{{_id}}][type]" style="width: 174px;">
											<option value="text" {{#is type value="text"}}selected="selected"{{/is}}><?php _e('Text', 'caldera-yellowstone' ); ?></option>
											<option value="color" {{#is type value="color"}}selected="selected"{{/is}}><?php _e('Color Picker', 'caldera-yellowstone' ); ?></option>
											<option value="image-picker" {{#is type value="image-picker"}}selected="selected"{{/is}}><?php _e('Image Picker', 'caldera-yellowstone' ); ?></option>
											<option value="range-slider" {{#is type value="range-slider"}}selected="selected"{{/is}}><?php _e('Range Slider', 'caldera-yellowstone' ); ?></option>
											<option value="textarea" {{#is type value="textarea"}}selected="selected"{{/is}}><?php _e('Paragraph', 'caldera-yellowstone' ); ?></option>
											<option value="post-type-selector" {{#is type value="post-type-selector"}}selected="selected"{{/is}}><?php _e('Post Type Selector (single)', 'caldera-yellowstone' ); ?></option>
											<option value="post-type-selector-multi" {{#is type value="post-type-selector-multi"}}selected="selected"{{/is}}><?php _e('Post Type Selector (Multiple)', 'caldera-yellowstone' ); ?></option>
											<option value="select2" {{#is type value="select2"}}selected="selected"{{/is}}><?php _e('Select2', 'caldera-yellowstone' ); ?></option>
											<option value="roles" {{#is type value="roles"}}selected="selected"{{/is}}><?php _e('User Roles Checklist', 'caldera-yellowstone' ); ?></option>
											<option value="visual-editor" {{#is type value="visual-editor"}}selected="selected"{{/is}}><?php _e('Visual Editor', 'caldera-yellowstone' ); ?></option>
											<option value="code-editor" {{#is type value="code-editor"}}selected="selected"{{/is}}><?php _e('Code Editor', 'caldera-yellowstone' ); ?></option>
										</select>				
									</div>

									<div style="display:{{#is type value="range-slider"}}block{{else}}none{{/is}};">
										<div class="caldera-config-group">
											<label for="{{../id}}-{{_id}}-range_min_value" style="width: 100px;"><?php _e( 'Min Value', 'caldera-yellowstone' ); ?></label>
											<input type="text" name="tab[{{../_id}}][field][{{_id}}][range_min_value]" value="{{range_min_value}}" id="{{../id}}-{{_id}}-range_min_value" style="width: 174px;">
										</div>
										<div class="caldera-config-group">
											<label for="{{../id}}-{{_id}}-range_max_value" style="width: 100px;"><?php _e( 'Max Value', 'caldera-yellowstone' ); ?></label>
											<input type="text" name="tab[{{../_id}}][field][{{_id}}][range_max_value]" value="{{range_max_value}}" id="{{../id}}-{{_id}}-range_max_value" style="width: 174px;">
										</div>
										<div class="caldera-config-group">
											<label for="{{../id}}-{{_id}}-range_suffix" style="width: 100px;"><?php _e( 'Value Suffix', 'caldera-yellowstone' ); ?></label>
											<input type="text" name="tab[{{../_id}}][field][{{_id}}][range_suffix]" value="{{range_suffix}}" id="{{../id}}-{{_id}}-range_suffix" style="width: 174px;">
										</div>
										<div class="caldera-config-group">
											<label for="{{../id}}-{{_id}}-range_step" style="width: 100px;"><?php _e( 'Step', 'caldera-yellowstone' ); ?></label>
											<input type="text" name="tab[{{../_id}}][field][{{_id}}][range_step]" value="{{range_step}}" id="{{../id}}-{{_id}}-range_step" style="width: 174px;">
										</div>
									</div>


									<button type="button" class="button button-small" style="padding: 0px; margin: 3px 0px; position: absolute; right: 8px; top: 6px;" data-remove-parent=".caldera-card-sub-item"><span class="dashicons dashicons-no-alt" style="padding: 0px; margin: 0px; line-height: 23px;"></span></button>
								</div>
								{{/each}}
								<hr>
								<button class="button button-small wp-baldrick" data-request="cye_get_default_setting" data-id="{{_id}}" data-script="add-config-panel-field" type="button" style=""><?php _e( 'Add Field', 'caldera-yellowstone' ); ?></button>
							{{/is}}							


						</div>
					</div>
				{{/each}}
			</div>

		</div>