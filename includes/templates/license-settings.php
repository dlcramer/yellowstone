		<h4><?php _e( 'License Management', 'caldera-yellowstone' ); ?></h4>
		
		{{#if license_enabled}}

        <div class="caldera-config-group">
            <label for="yellowstone_element-store_url"><?php _e( 'Store URL', 'caldera-yellowstone' ); ?></label>
            <input type="text" name="store_url" value="{{#unless store_url}}https://calderawp.com{{else}}{{store_url}}{{/unless}}" id="yellowstone_element-store_url" style="width: 220px;" required>
        </div>
        <div class="caldera-config-group">
            <label for="yellowstone_element-license_type"><?php _e( 'License Type', 'caldera-yellowstone' ); ?></label>
            <label style="width: auto;"><input style="width: auto;" type="radio" name="license_type" value="edd" {{#unless license_type}}checked="checked"{{else}}{{#is license_type value="edd"}}checked="checked"{{/is}}{{/unless}} id="yellowstone_element-license_type_edd" style="width: 220px;" required> EDD</label>&nbsp;&nbsp;
            <label style="width: auto;"><input style="width: auto;" type="radio" name="license_type" value="foo" {{#is license_type value="foo"}}checked="checked"{{/is}} id="yellowstone_element-license_type_edd" style="width: 220px;" required> Foo</label>
        </div>
		{{/if}}