<?php

$yellowstone_element = get_option( $_GET['edit'] );

// modules : disabled for now till i figure out the best way to work with them.
/*$yellowstone_element['module'] = array_merge( array(
	'metabox'	=>	array(
		'name'	=>	__('Metabox', 'caldera-yellowstone'),
		'slug'	=>	'metabox'
	)
), $yellowstone_element['module'] );

$yellowstone_element['module'] = apply_filters( 'caldera_yellowstone_get_modules', $yellowstone_element['module'] );
*/

?>
<div class="wrap" id="caldera-main-canvas">
	<span class="wp-baldrick spinner" style="float: none; display: block;" data-target="#caldera-main-canvas" data-callback="cye_canvas_init" data-type="json" data-request="#caldera-live-config" data-event="nope" data-template="#main-ui-template" data-autoload="true">asdasdas</span>
</div>

<div class="clear"></div>

<input type="hidden" class="clear" autocomplete="off" id="caldera-live-config" style="width:100%;" value="<?php echo esc_attr( json_encode($yellowstone_element) ); ?>">

<script type="text/html" id="main-ui-template">
	<?php
	// pull in the join table card template
	include CYE_PATH . 'includes/templates/main-ui.php';
	?>	
</script>

<script type="text/html" id="caldera-grid-template">
	{{#each row}}
		<div class="row">
		{{#each column}}
			<div class="col-xs-{{width}}">
				<div class="row-tools-bar">
					<span class="row-control-icon dashicons dashicons-plus-alt" style="font-size: 14px; display: inline;"></span>
				</div>			
				<div class="layout-column">
					<div>width:{{width}}</div>
					<div>{{content}}</div>
				</div>
			</div>
		{{/each}}
		</div>

	{{/each}}

</script>













