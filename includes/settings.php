<?php
class Settings_Caldera_Yellowstone extends Caldera_Yellowstone{


	/**
	 * Start up
	 */
	public function __construct(){
		
		// register admin page
		add_action( 'admin_menu', array( $this, 'add_settings_pages' ), 25 );
		// creat new yellowstone_element
		add_action( 'wp_ajax_cye_create_yellowstone_element', array( $this, 'create_new_yellowstone_element') );
		// save config
		add_action( 'wp_ajax_cye_save_config', array( $this, 'save_config') );
		// delete
		add_action( 'wp_ajax_cye_delete_yellowstone_element', array( $this, 'delete_yellowstone_element') );
		// builder
		add_action( 'wp_ajax_cye_build_plugin', array( $this, 'build_new_plugin') );



	}
	
	
	/**
	 * output 
	 */
	public function build_new_plugin( $build = null ){
		

		/* Export Parts ( for new method... later )
			
			files
				- admin ui page
					- admin main ui
					- admin tab panel
			actions
			methods

		*/
		if( empty( $build ) ){
			$build = $_REQUEST['item'];
		}

		$item = get_option( $build );

		$exclude_files 		= array();
		$additional_files 	= array();
		$added_functions	= array();
		$init_functions 	= array();
		$template_partials 	= array();
		$core_methods 		= array();
		$core_actions 		= array();
		$core_enqueue 		= array();
		
		$mode_main_nav	 	= file_get_contents( CYE_PATH . 'module-templates/mode/mode-main-nav-' . $item['theme'] . '.php' );

		// setup menu
		$mode_add_menu				= file_get_contents( CYE_PATH . 'module-templates/mode/mode-menu-' . $item['menu_type'] . '-location.php' );

		// Composer Module
		$composer_module_autoload = null;

		if( !empty( $item['composer'] ) ){
			// this is stupid of me. its json make an array then encode to json .sigh. lazy me.
			$composer_config = json_decode( file_get_contents( CYE_PATH . 'module-templates/composer/composer.json' ), true );

			if( !empty( $item['composer']['require'] ) ){
				// add in autoload				
				$composer_module_autoload = "//autoload dependencies\r\nif ( file_exists( {{CORE_PREFIX}}_PATH . 'vendor/autoload.php' ) )\r\nrequire_once( {{CORE_PREFIX}}_PATH . 'vendor/autoload.php' );";

				foreach( $item['composer']['require'] as $require ){

					// add file and lines
					$composer_config['require'][$require['package']] = $require['version'];

				}
			}

			// add author
			if( !empty( $item['composer']['author'] ) ){

				$composer_config['authors'] = array(
					$item['composer']['author']
				);

			}

			// include composer.json
			$additional_files[] = array(
				'src'	=>	json_encode( $composer_config, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES ),
				'dst'	=> 	WP_PLUGIN_DIR . '/cy_' . $item['slug'] . '/composer.json',
			);

		}
		// add replaces to item
		$item['module']['composer'] = array(
			'{{module_composer_autoload}}'	=>	$composer_module_autoload
		);


		


		$admin_pages = array(
			'_base' => array()
		);
		if( !empty( $item['tab'] ) ){
			$admin_pages['_base']['tab'] = $item['tab'];
		}
		if( !empty( $item['admin_page'] ) ){
			foreach( $item['admin_page'] as $admin_page ){
				if( !empty( $admin_page['tab'] ) ){
					$key = $admin_page['slug'];
					$admin_pages[ $key ]['tab'] = $admin_page['tab'];
					
					$page_file = file_get_contents( CYE_PATH . 'module-templates/mode/additions/admin-page.php' );
					$page_ui = file_get_contents( CYE_PATH . 'caldera-framework-template/includes/templates/main-ui.php' );

					$item['module']['admin_page_' . $key ] = array(
						'{{module_admin_loader}}' 	=>	"'" . $key . "'",
						'{{module_admin_key}}' 		=>	$key,
					);

					foreach ($item['module']['admin_page_' . $key ] as $replace_key => $replace_value) {
						$page_file = str_replace( $replace_key, $replace_value, $page_file );
						$page_ui = str_replace( $replace_key, $replace_value, $page_ui );
					}

					// setup menu
					$mode_add_menu_add	= file_get_contents( CYE_PATH . 'module-templates/mode/mode-menu-sub-location.php' );
					$mode_add_menu_add = str_replace( '{{Core_Name}}', '{{Core_Name}} - ' . $admin_page['name'], $mode_add_menu_add );
					$mode_add_menu_add = str_replace( '{{Core_Short_Name}}', $admin_page['name'], $mode_add_menu_add );

					$mode_add_menu_add = str_replace( '{{core_slug}}', '{{core_slug}}-' . $key, $mode_add_menu_add );
					if( $item['menu_type'] == 'parent' ){
						$mode_add_menu_add = str_replace( '{{core_menu_location}}', '{{core_slug}}', $mode_add_menu_add );
					}
					//}
					$mode_add_menu .= $mode_add_menu_add;					


					$additional_files[ $key . '_page' ] = array(
						'src'	=>	str_replace( '{{module_', '{{module_' . $key .'-' ,$page_file ),
						'dst'	=> 	WP_PLUGIN_DIR . '/cy_' . $item['slug'] . '/' . 'includes/' . $key . '.php',
					);
					$additional_files[ $key . '_ui' ] = array(
						'src'	=>	str_replace( '{{module_', '{{module_' . $key .'-' , $page_ui ),
						'dst'	=> 	WP_PLUGIN_DIR . '/cy_' . $item['slug'] . '/' . 'includes/templates/' . $key . '-main-ui.php',
					);

				}
			}
		}

		foreach( $admin_pages as $admin_prefix=>$admin_page_item ){


			$prefix = null;
			if( $admin_prefix != '_base' ){
				$prefix = $admin_prefix . '-';
				$item['mode'] = 'single';
			}

			// mode module
			if( $item['mode'] == 'multi' ){
				$mode_edit_footer			= null;
				$mode_edit_title 			= file_get_contents( CYE_PATH . 'module-templates/mode/mode-multi-title.php' );			
				$mode_single_save_button 	= null;
				$mode_admin_loader			= file_get_contents( CYE_PATH . 'module-templates/mode/mode-multi-admin-loader.php' );
				$mode_edit_loader			= 'strip_tags( $_GET[\'edit\'] )';
				$mode_settings_methods		= file_get_contents( CYE_PATH . 'module-templates/mode/mode-multi-settings-methods.php' );
				$mode_settings_actions		= file_get_contents( CYE_PATH . 'module-templates/mode/mode-multi-settings-actions.php' );
				$mode_save_config			= file_get_contents( CYE_PATH . 'module-templates/mode/mode-multi-save-config.php' );
				$mode_config_form_start		= file_get_contents( CYE_PATH . 'module-templates/mode/mode-multi-form-start.php' );
				$mode_style_scripts			= file_get_contents( CYE_PATH . 'module-templates/mode/mode-multi-style-scripts.php' );
				$mode_general_settings		= file_get_contents( CYE_PATH . 'module-templates/mode/mode-multi-general-settings.php' );
			}elseif( $item['mode'] == 'single' ){
				$mode_edit_footer			= file_get_contents( CYE_PATH . 'module-templates/mode/mode-single-footer.php' );
				$mode_edit_title 			= file_get_contents( CYE_PATH . 'module-templates/mode/mode-single-title.php' );
				$mode_single_save_button	= file_get_contents( CYE_PATH . 'module-templates/mode/mode-single-save-button.php' );
				$mode_admin_loader			= file_get_contents( CYE_PATH . 'module-templates/mode/mode-single-admin-loader.php' );
				$mode_edit_loader			= "'{{core_object}}'";
				$mode_settings_methods		= null;
				$mode_settings_actions		= null;
				$mode_save_config			= file_get_contents( CYE_PATH . 'module-templates/mode/mode-single-save-config.php' );
				$mode_config_form_start		= file_get_contents( CYE_PATH . 'module-templates/mode/mode-single-form-start.php' );
				$mode_style_scripts			= file_get_contents( CYE_PATH . 'module-templates/mode/mode-single-style-scripts.php' );
				$mode_general_settings		= file_get_contents( CYE_PATH . 'module-templates/mode/mode-single-general-settings.php' );
				if( $admin_prefix == '_base' ){
					$exclude_files[]			= CYE_PATH . "caldera-framework-template/includes/admin.php";
					$exclude_files[]			= CYE_PATH . "caldera-framework-template/includes/templates/general-settings.php";
				}else{
					$mode_config_form_start = str_replace('{{core_object}}', $admin_prefix, $mode_config_form_start );
					$mode_edit_title = str_replace( '{{Core_Name}}', '{{Core_Name}} - ' . $admin_page['name'], $mode_edit_title );
				}
			}			
			// Tabs Module
			$tab_module_nav 		= null;
			$tab_module_sub_nav 	= null;
			$tab_module_panel 		= null;
			$tab_module_sub_base 	= null;

			if( $item['mode'] == 'multi' ){ // only use general settings for multi items
				$tab_module_panel 		.= file_get_contents( CYE_PATH . 'module-templates/tabs/tab-panel-multi-general-template.php' ); // include the general
				if( $item['general_settings']['location'] == 'sub'){
					if( null === $tab_module_sub_base ){
						$tab_module_sub_base = file_get_contents( CYE_PATH . 'module-templates/tabs/tab-sub-nav-base-template-' . $item['theme'] . '.php' );
					}
					$tab_module_sub_nav 	.= str_replace('{{tab_name}}', '{{module_' . $prefix . 'general_name}}', str_replace('{{tab_slug}}', 'general', file_get_contents( CYE_PATH . 'module-templates/tabs/tab-sub-nav-template-' . $item['theme'] . '.php' ) ) );
				}elseif( $item['general_settings']['location'] == 'main'){
					$tab_module_nav 		.= str_replace('{{tab_name}}', '{{module_' . $prefix . 'general_name}}', str_replace('{{tab_slug}}', 'general', file_get_contents( CYE_PATH . 'module-templates/tabs/tab-nav-template-' . $item['theme'] . '.php' ) ) );
				}
			}else{
				$item['general_settings']['name'] 			= null;
				$item['general_settings']['description'] 	= null;
			}


			if( !empty( $admin_page_item['tab'] ) ){

				/// gets teh assets list
				include CYE_PATH . 'module-templates/tabs/tab_module.php';

				foreach( $admin_page_item['tab'] as $tab ){
					
					// add module templates
					if( $tab['location'] == 'main' ){
						$tab_module_nav 		.= file_get_contents( CYE_PATH . 'module-templates/tabs/tab-nav-template-' . $item['theme'] . '.php' );

					}elseif( $tab['location'] == 'sub' ){
						if( null === $tab_module_sub_base ){
							$tab_module_sub_base = file_get_contents( CYE_PATH . 'module-templates/tabs/tab-sub-nav-base-template-' . $item['theme'] . '.php' );
						}
						$tab_module_sub_nav 	.= file_get_contents( CYE_PATH . 'module-templates/tabs/tab-sub-nav-template-' . $item['theme'] . '.php' );
					}
					$tab_module_panel 		.= file_get_contents( CYE_PATH . 'module-templates/tabs/tab-panel-template.php' );
					// panel template is singular so only 1 per tab
					$tab_module_template 	= file_get_contents( CYE_PATH . 'module-templates/tabs/tab-' . $tab['type'] . '-template.php' );
					
					// check for tab assets
					if( isset( $tab['type'] ) && isset( $assets['tabs'][$tab['type']] ) ){
						// init funcitons						
						if( !empty( $assets['tabs'][$tab['type']]['init_functions'] ) && file_exists( CYE_PATH . 'module-templates/tabs/assets/' . $assets['tabs'][$tab['type']]['init_functions'] ) ){
							$init_functions[] = str_replace('{{tab_slug}}', $tab['slug'], file_get_contents( CYE_PATH . 'module-templates/tabs/assets/' . $assets['tabs'][$tab['type']]['init_functions'] ));
						}
						// template partials						
						if( !empty( $assets['tabs'][$tab['type']]['template_partials'] ) ){

							foreach( $assets['tabs'][$tab['type']]['template_partials'] as $partial_key=>$partial_file ){
								if( file_exists( CYE_PATH . 'module-templates/tabs/assets/' . $partial_file ) ){								
									$partial_template = '<script type="text/html" data-handlebars-partial="' . $tab['slug'] . '-' . $partial_key .'">' . "\r\n";
									$partial_template .= str_replace('{{tab_slug}}', $tab['slug'], file_get_contents( CYE_PATH . 'module-templates/tabs/assets/' . $partial_file ));
									$partial_template .= "\r\n</script>\r\n";
									$template_partials[] = $partial_template;
								}
							}

						}
						
						// core methods funcitons	
						if( !empty( $assets['tabs'][$tab['type']]['core_methods'] ) && file_exists( CYE_PATH . 'module-templates/tabs/assets/' . $assets['tabs'][$tab['type']]['core_methods'] ) ){
							$core_methods[ $tab['type'] ] = file_get_contents( CYE_PATH . 'module-templates/tabs/assets/' . $assets['tabs'][$tab['type']]['core_methods'] );
						}						
						// functions 
						if( isset( $assets['tabs'][$tab['type']]['functions'] ) ){
							foreach( $assets['tabs'][$tab['type']]['functions'] as $function ){
								if( !in_array( $function, $added_functions) ){
									$added_functions[] = $function;
									$mode_style_scripts .= "\r\n\t\t\t" . $function . ";";
								}
							}
						}

						// scripts
						if( isset( $assets['tabs'][$tab['type']]['scripts'] ) ){
							foreach( $assets['tabs'][$tab['type']]['scripts'] as $script ){
								if( file_exists( CYE_PATH . 'module-templates/tabs/assets/' . $script ) && !isset( $additional_files[$script] ) ){
									$additional_files[$script] = array(
										'src'	=>	CYE_PATH . 'module-templates/tabs/assets/' . $script,
										'dst'	=> 	WP_PLUGIN_DIR . '/cy_' . $item['slug'] . '/' . 'assets/js/' . $script,
									);
									$mode_style_scripts .= "\r\n\t\t\twp_enqueue_script( '{{core_slug}}-" . strtok( $script, '.' ) . "-" . $tab['type'] . "', {{CORE_PREFIX}}_URL . '/assets/js/" . $script . "', array( 'jquery' ) , false );\r\n";
								}
							}
						}
						// styles
						if( isset( $assets['tabs'][$tab['type']]['styles'] ) ){
							foreach( $assets['tabs'][$tab['type']]['styles'] as $style ){
								if( file_exists( CYE_PATH . 'module-templates/tabs/assets/' . $style ) && !isset( $additional_files[$style] ) ){
									$additional_files[$style] = array(
										'src'	=>	CYE_PATH . 'module-templates/tabs/assets/' . $style,
										'dst'	=> 	WP_PLUGIN_DIR . '/cy_' . $item['slug'] . '/' . 'assets/css/' . $style,
									);
									$mode_style_scripts .= "\r\n\t\t\twp_enqueue_style( '{{core_slug}}-" . strtok( $style, '.' ) . "-" . $tab['type'] . "', {{CORE_PREFIX}}_URL . '/assets/css/" . $style . "' );";
								}
							}
						}
					}
					// config tab fields
					if( !empty( $tab['field'] ) ){
						
						// repeatable?
						if( !empty( $tab['is_repeatable'] ) ){
							$tab_module_template 	= file_get_contents( CYE_PATH . 'module-templates/tabs/tab-' . $tab['type'] . '-repeatable-template.php' );
						}

						$tab_panel_fields = null;

						foreach( $tab['field'] as $field ){
							// select2
							if( isset( $field['type'] ) && ( $field['type'] == 'post-type-selector' || $field['type'] == 'post-type-selector-multi' || $field['type'] == 'select2' ) ){
								$has_select2 = true;
							}

							// check for field assets
							if( isset( $field['type'] ) && isset( $assets['fields'][$field['type']] ) ){

								// functions 
								if( isset( $assets['fields'][$field['type']]['functions'] ) ){
									foreach( $assets['fields'][$field['type']]['functions'] as $function ){
										if( !in_array( $function, $added_functions) ){
											$added_functions[] = $function;
											$mode_style_scripts .= "\r\n\t\t\t" . $function . ";";
										}
									}
								}

								// scripts
								if( isset( $assets['fields'][$field['type']]['scripts'] ) ){
									foreach( $assets['fields'][$field['type']]['scripts'] as $script ){
										if( file_exists( CYE_PATH . 'module-templates/tabs/assets/' . $script ) && !isset( $additional_files[$script] ) ){
											$additional_files[$script] = array(
												'src'	=>	CYE_PATH . 'module-templates/tabs/assets/' . $script,
												'dst'	=> 	WP_PLUGIN_DIR . '/cy_' . $item['slug'] . '/' . 'assets/js/' . $script,
											);
											$mode_style_scripts .= "\r\n\t\t\twp_enqueue_script( '{{core_slug}}-" . strtok( $script, '.' ) . "-" . $field['type'] . "', {{CORE_PREFIX}}_URL . '/assets/js/" . $script . "', array( 'jquery' ) , false );\r\n";
										}
									}
								}
								// styles
								if( isset( $assets['fields'][$field['type']]['styles'] ) ){
									foreach( $assets['fields'][$field['type']]['styles'] as $style ){
										if( file_exists( CYE_PATH . 'module-templates/tabs/assets/' . $style ) && !isset( $additional_files[$style] ) ){
											$additional_files[$style] = array(
												'src'	=>	CYE_PATH . 'module-templates/tabs/assets/' . $style,
												'dst'	=> 	WP_PLUGIN_DIR . '/cy_' . $item['slug'] . '/' . 'assets/css/' . $style,
											);
											$mode_style_scripts .= "\r\n\t\t\twp_enqueue_style( '{{core_slug}}-" . strtok( $style, '.' ) . "-" . $field['type'] . "', {{CORE_PREFIX}}_URL . '/assets/css/" . $style . "' );";
										}
									}
								}
							}


							$tab_panel_fields 	.= file_get_contents( CYE_PATH . 'module-templates/tabs/tab-config-field-' . $field['type'] . '.php' );
							if( !empty( $tab['is_repeatable'] ) ){							
					
								$tab_panel_fields 	= str_replace('{{module_tab_field_id}}', "{{core-slug}}-{{tab_slug}}-{{module_tab_field_slug}}-{{_id}}", $tab_panel_fields);
								$tab_panel_fields 	= str_replace('{{module_tab_field_input_name}}', "{{tab_slug}}[{{_id}}][{{module_tab_field_slug}}]", $tab_panel_fields);
								$tab_panel_fields 	= str_replace('{{module_tab_field_value}}', "{{module_tab_field_slug}}", $tab_panel_fields);
								$tab_module_template 	= str_replace('{{module_tab_field_add_text}}', $tab['add_text'], $tab_module_template);
								$tab_module_template 	= str_replace('{{module_tab_field_empty_text}}', $tab['empty_text'], $tab_module_template);
								$tab_module_template 	= str_replace('{{module_tab_field_dashicon}}', $tab['dashicon'], $tab_module_template);
								$tab_panel_fields 	= str_replace('{{module_tab_field_path}}', "{{module_tab_field_slug}}", $tab_panel_fields);
								
							}else{
								

								if( $tab['type'] == 'grouped-config' || $tab['type'] == 'image-gallery' ){
									$tab_panel_fields 	= str_replace('{{module_tab_field_id}}', "{{core-slug}}-{{tab_slug}}-{{module_tab_field_slug}}-{{_id}}", $tab_panel_fields);
									$tab_panel_fields 	= str_replace('{{module_tab_field_input_name}}', "{{:name}}[config][{{module_tab_field_slug}}]", $tab_panel_fields);
									$tab_panel_fields 	= str_replace('{{module_tab_field_value}}', "config/{{module_tab_field_slug}}", $tab_panel_fields);
									$tab_panel_fields 	= str_replace('{{module_tab_field_path}}', "config/{{module_tab_field_slug}}", $tab_panel_fields);
								}else{
									$tab_panel_fields 	= str_replace('{{module_tab_field_id}}', "{{core-slug}}-{{tab_slug}}-{{module_tab_field_slug}}", $tab_panel_fields);
									$tab_panel_fields 	= str_replace('{{module_tab_field_input_name}}', "{{tab_slug}}[{{module_tab_field_slug}}]", $tab_panel_fields);
									$tab_panel_fields 	= str_replace('{{module_tab_field_value}}', "{{tab_slug}}/{{module_tab_field_slug}}", $tab_panel_fields);
									$tab_panel_fields 	= str_replace('{{module_tab_field_path}}', "{{module_tab_field_slug}}", $tab_panel_fields);
								}
							}				
							
							$tab_panel_fields 	= str_replace('{{module_tab_field_path}}', "{{tab_slug}}/{{module_tab_field_slug}}", $tab_panel_fields);

							if( !empty( $field['required'] ) ){
								$tab_panel_fields 	= str_replace('{{module_tab_field_required}}', 'required="required"', $tab_panel_fields);
							}else{
								$tab_panel_fields 	= str_replace('{{module_tab_field_required}}', null, $tab_panel_fields);
							}
							
							if( !empty( $field['description'] ) ){
								$tab_panel_fields 	= str_replace('{{module_tab_field_description}}', '<p class="description" style="margin-left: 190px;"> ' . $field['description'] . '</p>', $tab_panel_fields);
							}else{
								$tab_panel_fields 	= str_replace('{{module_tab_field_description}}', null, $tab_panel_fields);
							}
							// general replaces
							foreach( $field as $field_key=>$field_value ){
								$tab_panel_fields 	= str_replace('{{module_tab_field_' . $field_key . '}}', $field_value, $tab_panel_fields);
							}

						
						}
						// add fields to panel
						$tab_module_template 	=	str_replace('{{module_tab_config_panel_fields}}', $tab_panel_fields, $tab_module_template);
					}

					// groups and fields - repeatable
					if( $tab['type'] == 'fieldgroups-panel' ){

						if( !empty( $tab['allow_repeat'] ) ){
							$repeatable_groups_setting 	= file_get_contents( CYE_PATH . 'module-templates/tabs/tab-groups-repeatable-template.php' );
						}else{
							$repeatable_groups_setting = '';
						}

						$tab_module_template 	= str_replace('{{tab_delete_confirm}}', addslashes( $tab['delete_confirm'] ), $tab_module_template);
						$tab_module_template 	= str_replace('{{module_tab_allow_repeat}}', $repeatable_groups_setting, $tab_module_template);		
					}



					// tab type grid
					if( $tab['type'] == 'grid' ){
						$hasGrid = true;

						// kill row message
						$tab_module_template 	= str_replace('{{tab_delete_confirm}}', addslashes( $tab['delete_confirm'] ), $tab_module_template);
						// loop em
						// tab grid element def
						$element_trigger = file_get_contents( CYE_PATH . 'module-templates/tabs/tab-grid-element-trigger.php' );
						$element_modal_template = file_get_contents( CYE_PATH . 'module-templates/tabs/tab-grid-element-modal-template.php' );
						$element_modal_template_code_box = file_get_contents( CYE_PATH . 'module-templates/tabs/tab-grid-element-code-editor.php' );
						$element_template = file_get_contents( CYE_PATH . 'module-templates/tabs/tab-grid-element-template.php' );
						$element_template_code_box = file_get_contents( CYE_PATH . 'module-templates/tabs/tab-grid-element-template-code-box.php' );
						$element_layout_template = file_get_contents( CYE_PATH . 'module-templates/tabs/tab-grid-element-layout-template.php' );
						$element_layout_template_code_box = file_get_contents( CYE_PATH . 'module-templates/tabs/tab-grid-element-layout-template-code-box.php' );
						//{{tab_grid_element_templates}}
						$element_trigger_lines = '';
						$element_modal_template_lines = '';
						$element_template_lines = '';
						$element_layout_template_lines = '';
						if( !empty( $tab['element'] ) ){

							foreach( $tab['element'] as $element_item ){



								$element_trigger_lines .= $element_trigger;
								if( !empty( $element_item['code_box'] ) ){
									$element_modal_template_lines .= $element_modal_template_code_box;
									$element_layout_template_lines .= $element_layout_template_code_box;
									$element_template_lines .= $element_template_code_box;
								}else{
									$element_modal_template_lines .= $element_modal_template;
									$element_layout_template_lines .= $element_layout_template;
									$element_template_lines .= $element_template;
								}


								

								$element_slug = str_replace( '-', '_', sanitize_file_name( $element_item['grid_item_slug'] ) );
								// triggers
								$element_trigger_lines 	= str_replace('{{tab_item_title}}', addslashes( $element_item['grid_item'] ), $element_trigger_lines);
								$element_trigger_lines 	= str_replace('{{tab_item_slug}}', addslashes( $element_slug ), $element_trigger_lines);
								$element_trigger_lines 	= str_replace('{{tab_insert_text}}', addslashes( $element_item['insert_text'] ), $element_trigger_lines);
								$element_trigger_lines 	= str_replace('{{tab_update_text}}', addslashes( $element_item['update_text'] ), $element_trigger_lines);
								$element_trigger_lines 	= str_replace('{{tab_delete_confirm}}', addslashes( $element_item['delete_confirm'] ), $element_trigger_lines);
								// modals
								$element_modal_template_lines 	= str_replace('{{tab_item_title}}', addslashes( $element_item['grid_item'] ), $element_modal_template_lines);
								$element_modal_template_lines 	= str_replace('{{tab_item_slug}}', addslashes( $element_slug ), $element_modal_template_lines);
								$element_modal_template_lines 	= str_replace('{{tab_insert_text}}', addslashes( $element_item['insert_text'] ), $element_modal_template_lines);
								$element_modal_template_lines 	= str_replace('{{tab_update_text}}', addslashes( $element_item['update_text'] ), $element_modal_template_lines);
								$element_modal_template_lines 	= str_replace('{{tab_delete_confirm}}', addslashes( $element_item['delete_confirm'] ), $element_modal_template_lines);
								// template
								$element_template_lines 	= str_replace('{{tab_item_title}}', addslashes( $element_item['grid_item'] ), $element_template_lines);
								$element_template_lines 	= str_replace('{{tab_item_slug}}', addslashes( $element_slug ), $element_template_lines);
								$element_template_lines 	= str_replace('{{tab_insert_text}}', addslashes( $element_item['insert_text'] ), $element_template_lines);
								$element_template_lines 	= str_replace('{{tab_update_text}}', addslashes( $element_item['update_text'] ), $element_template_lines);
								$element_template_lines 	= str_replace('{{tab_delete_confirm}}', addslashes( $element_item['delete_confirm'] ), $element_template_lines);
								// layout template
								$element_layout_template_lines 	= str_replace('{{tab_item_title}}', addslashes( $element_item['grid_item'] ), $element_layout_template_lines);
								$element_layout_template_lines 	= str_replace('{{tab_item_slug}}', addslashes( $element_slug ), $element_layout_template_lines);
								$element_layout_template_lines 	= str_replace('{{tab_insert_text}}', addslashes( $element_item['insert_text'] ), $element_layout_template_lines);
								$element_layout_template_lines 	= str_replace('{{tab_update_text}}', addslashes( $element_item['update_text'] ), $element_layout_template_lines);
								$element_layout_template_lines 	= str_replace('{{tab_delete_confirm}}', addslashes( $element_item['delete_confirm'] ), $element_layout_template_lines);

							}

							// add lines to tab
							$tab_module_template 	= str_replace('{{tab_grid_elements}}', $element_trigger_lines, $tab_module_template);
							$tab_module_template 	= str_replace('{{tab_grid_element_modal_templates}}', $element_modal_template_lines, $tab_module_template);
							$tab_module_template 	= str_replace('{{tab_grid_element_templates}}', $element_template_lines, $tab_module_template);
							$tab_module_template 	= str_replace('{{tab_grid_element_layout_templates}}', $element_layout_template_lines, $tab_module_template);
							// set selector height
							//$tab_module_template 	= str_replace('{{tab_element_selector_height}}', 55, $tab_module_template);
							$tab_module_template 	= str_replace('{{tab_element_selector_height}}', ( 67 + ( count( $tab['element'] ) * 44 ) ), $tab_module_template);

						}
						
						
					}

					// grouped config
					if( $tab['type'] == 'grouped-config' ){
						$hasGroupConfig = true;
					}
					// search form support
					if( $tab['type'] == 'search-form' ){
						$hassearch_form = true;
						// set binding
						$tab_module_template 	= str_replace('{{query_filter_binding}}', $tab['query_bind'], $tab_module_template);
					}
					// query filter
					if( $tab['type'] == 'query-filter' || !empty( $has_select2 ) ){
						if( $tab['type'] == 'query-filter' ){
							$hasquery_filter = true;
						}
	                    // select 2 script
	                    $additional_files[] = array(
	                        'src'	=>	CYE_PATH . 'module-templates/tabs/assets/select2.min.js',
	                        'dst'	=> 	WP_PLUGIN_DIR . '/cy_' . $item['slug'] . '/' . 'assets/js/select2.min.js',
	                    );
	                    $mode_style_scripts .= "\r\n\t\t\twp_enqueue_script( '{{core_slug}}-select2', {{CORE_PREFIX}}_URL . '/assets/js/select2.min.js', array( 'jquery' ) , false );\r\n";
	                    // select 2 style
	                    $additional_files[] = array(
	                        'src'	=>	CYE_PATH . 'module-templates/tabs/assets/ccselect2.png',
	                        'dst'	=> 	WP_PLUGIN_DIR . '/cy_' . $item['slug'] . '/' . 'assets/css/ccselect2.png',
	                    );
	                    $additional_files[] = array(
	                        'src'	=>	CYE_PATH . 'module-templates/tabs/assets/ccselect2x2.png',
	                        'dst'	=> 	WP_PLUGIN_DIR . '/cy_' . $item['slug'] . '/' . 'assets/css/ccselect2x2.png',
	                    );
	                    $additional_files[] = array(
	                        'src'	=>	CYE_PATH . 'module-templates/tabs/assets/ccselect2-spinner.png',
	                        'dst'	=> 	WP_PLUGIN_DIR . '/cy_' . $item['slug'] . '/' . 'assets/css/ccselect2-spinner.png',
	                    );
	                    $additional_files[] = array(
	                        'src'	=>	CYE_PATH . 'module-templates/tabs/assets/select2.css',
	                        'dst'	=> 	WP_PLUGIN_DIR . '/cy_' . $item['slug'] . '/' . 'assets/css/select2.css',
	                    );
	                    $mode_style_scripts .= "\r\n\t\t\twp_enqueue_style( '{{core_slug}}-select2', {{CORE_PREFIX}}_URL . '/assets/css/select2.css' );";
					}


					// lable added modules
					foreach( $tab as $tab_key => $tab_value ){
						if( is_array( $tab_value ) ){	
							continue;
						}
						$tab_module_nav 	= str_replace('{{tab_' . $tab_key . '}}', $tab_value, $tab_module_nav);
						$tab_module_sub_nav 	= str_replace('{{tab_' . $tab_key . '}}', $tab_value, $tab_module_sub_nav);
						$tab_module_panel 	= str_replace('{{tab_' . $tab_key . '}}', $tab_value, $tab_module_panel);
						$tab_module_template 	= str_replace('{{tab_' . $tab_key . '}}', $tab_value, $tab_module_template);
					}

					// add files
					$additional_files[] = array(
						'src'	=>	$tab_module_template,
						'dst'	=> 	WP_PLUGIN_DIR . '/cy_' . $item['slug'] . '/' . 'includes/templates/' . $prefix . $tab['slug'] . '-panel.php',
					);
					
				}
			}
		
			$tab_module_nav 		= str_replace('{{module_', '{{module_' . $prefix , $tab_module_nav );
			$tab_module_sub_nav 	= str_replace('{{module_', '{{module_' . $prefix , $tab_module_sub_nav );
			$tab_module_panel 		= str_replace('{{module_', '{{module_' . $prefix , $tab_module_panel );
			$tab_module_sub_base 	= str_replace('{{module_', '{{module_' . $prefix , $tab_module_sub_base );
			$tab_module_panel 		= str_replace('{{admin_prefix}}', $prefix , $tab_module_panel );

			
			// add image-picker styles
			/*if( !empty( $hasImagePicker ) ){

				$additional_files[] = array(
					'src'	=>	CYE_PATH . 'module-templates/tabs/field_scripts/image-picker.js',
					'dst'	=> 	WP_PLUGIN_DIR . '/cy_' . $item['slug'] . '/' . 'assets/js/image-picker.js',
				);

				$mode_style_scripts .= "\r\n\t\t\twp_enqueue_media();";
				$mode_style_scripts .= "\r\n\t\t\twp_enqueue_script( '{{core_slug}}-image-picker-script', {{CORE_PREFIX}}_URL . '/assets/js/image-picker.js', array( 'jquery' ) , false );\r\n";

			}*/	
			// module types replaces
			$item['module'][$prefix . 'mode'] = array(
				'{{module_'. $prefix . 'mode_edit_footer}}'		=>	$mode_edit_footer,
				'{{module_'. $prefix . 'mode_title}}'				=>	$mode_edit_title,
				'{{module_'. $prefix . 'mode_main_nav}}'			=>	str_replace('{{module_', '{{module_' . $prefix , $mode_main_nav ),
				'{{module_'. $prefix . 'mode_single_save}}'		=>	$mode_single_save_button,
				'{{module_'. $prefix . 'mode_admin_loader}}'		=>	$mode_admin_loader,
				'{{module_'. $prefix . 'mode_edit_loader}}'		=>	$mode_edit_loader,
				'{{module_'. $prefix . 'mode_settings_methods}}'	=>	$mode_settings_methods,
				'{{module_'. $prefix . 'mode_settings_actions}}'	=>	$mode_settings_actions,
				'{{module_'. $prefix . 'mode_save_config}}'		=>	$mode_save_config,
				'{{module_'. $prefix . 'mode_form_start}}'		=>	$mode_config_form_start,
				'{{module_'. $prefix . 'mode_add_menu}}'			=>	str_replace('{{module_', '{{module_' . $prefix , $mode_add_menu ),
				'{{module_'. $prefix . 'mode_general_settings}}'	=>	$mode_general_settings,
				'{{mode_theme_style}}'								=>	file_get_contents( CYE_PATH . 'module-templates/mode/theme-' . $item['theme'] . '-styles.css' ),
				'{{tab_init_functions}}'							=>	implode("\r\n", $init_functions ),
				'{{tab_template_partials}}'							=>	implode("\r\n", $template_partials ),


			);

			// replace texts
			$item['module'][$prefix . 'tab'] = array(
				'{{module_'. $prefix . 'tab_nav}}' 					=> $tab_module_nav,
				'{{module_'. $prefix . 'tab_sub_nav_base}}' 			=> $tab_module_sub_base,
				'{{module_'. $prefix . 'tab_sub_nav}}' 				=> $tab_module_sub_nav,
				'{{module_'. $prefix . 'tab_panel}}' 					=> $tab_module_panel,
				'{{module_'. $prefix . 'general_name}}'				=> $item['general_settings']['name'],
				'{{module_'. $prefix . 'general_description}}'		=> $item['general_settings']['description'],
			);
			if( !empty( $tab_module_sub_base ) ){
				$item['module'][$prefix . 'tab']['{{module_'. $prefix . 'tab_has_sub_class}}'] = 'has-sub-nav';
			}else{
				$item['module'][$prefix . 'tab']['{{module_'. $prefix . 'tab_has_sub_class}}'] = '';
			}

		}

		// aditionals
		$item['module']['mode']['{{module_mode_style_scripts}}']	=	$mode_style_scripts;
		$item['module']['mode']['{{module_mode_front_style_scripts}}']	=	'';
		$item['module']['mode']['{{module_mode_core_methods}}'] = implode('\r\n', $core_methods );
		$item['module']['mode']['{{module_mode_core_actions}}'] = implode('\r\n', $core_actions );
		$item['module']['mode']['{{module_mode_core_enqueue}}'] = implode('\r\n', $core_enqueue );

		// has shortcodes?
		if( !empty( $item['shortcode'] ) ){
			$item['module']['mode']['{{module_mode_core_methods}}'] .= file_get_contents( CYE_PATH . 'module-templates/mode/mode-shortcode-methods.php' );
			$item['module']['mode']['{{module_mode_core_actions}}'] .= file_get_contents( CYE_PATH . 'module-templates/mode/mode-shortcode-actions.php' );
			$item['module']['mode']['{{module_mode_core_enqueue}}'] .= file_get_contents( CYE_PATH . 'module-templates/mode/mode-shortcode-enqueue.php' );
			// add files
			$additional_files[] = array(
				'src'	=>	CYE_PATH . 'module-templates/mode/additions/insert-shortcode-' . $item['mode'] . '.php',
				'dst'	=> 	WP_PLUGIN_DIR . '/cy_' . $item['slug'] . '/' . 'includes/insert-shortcode.php',
			);
			$additional_files[] = array(
				'src'	=>	CYE_PATH . 'module-templates/mode/additions/shortcode-insert.js',
				'dst'	=> 	WP_PLUGIN_DIR . '/cy_' . $item['slug'] . '/' . 'assets/js/shortcode-insert.js',
			);

		}

		// modify style for grouped config
		if( !empty( $hasGroupConfig ) ){
			$item['module']['mode']['{{mode_theme_style}}'] .= file_get_contents( CYE_PATH . 'module-templates/tabs/grouped-config.css' );
		}
		// modify style for query filter
		if( !empty( $hasquery_filter ) ){
			$item['module']['mode']['{{mode_theme_style}}'] .= file_get_contents( CYE_PATH . 'module-templates/tabs/query-filter.css' );
			$item['module']['mode']['{{module_mode_core_methods}}'] .= file_get_contents( CYE_PATH . 'module-templates/mode/mode-query-filter-methods.php' );
		}
		// modify style for search form
		if( !empty( $hassearch_form ) ){
			
			//$item['module']['mode']['{{module_mode_style_scripts}}'] .= "\r\n\t\t\twp_enqueue_script( 'jquery-ui-autocomplete' );";
			$item['module']['mode']['{{module_mode_settings_methods}}'] .= file_get_contents( CYE_PATH . 'module-templates/mode/mode-search-form-methods.php' );
			$item['module']['mode']['{{module_mode_settings_actions}}'] .= file_get_contents( CYE_PATH . 'module-templates/mode/mode-search-form-actions.php' );

		}
		
		// add grid styles
		if( !empty( $hasGrid ) ){
			$additional_files[] = array(
				'src'	=>	CYE_PATH . 'module-templates/tabs/grid.css',
				'dst'	=> 	WP_PLUGIN_DIR . '/cy_' . $item['slug'] . '/' . 'assets/css/grid.css',
			);
			$additional_files[] = array(
				'src'	=>	CYE_PATH . 'module-templates/tabs/grid-front.css',
				'dst'	=> 	WP_PLUGIN_DIR . '/cy_' . $item['slug'] . '/' . 'assets/css/grid-front.css',
			);
			$additional_files[] = array(
				'src'	=>	CYE_PATH . 'module-templates/tabs/grid.js',
				'dst'	=> 	WP_PLUGIN_DIR . '/cy_' . $item['slug'] . '/' . 'assets/js/grid.js',
			);
			$item['module']['mode']['{{module_mode_core_methods}}'] .= file_get_contents( CYE_PATH . 'module-templates/mode/mode-grid-render-methods.php' );
			$item['module']['mode']['{{module_mode_style_scripts}}'] .= "\r\n\t\t\twp_enqueue_style( '{{core-slug}}-core-grid', {{CORE_PREFIX}}_URL . '/assets/css/grid.css' );";
			$item['module']['mode']['{{module_mode_style_scripts}}'] .= "\r\n\t\t\twp_enqueue_script( '{{core_slug}}-core-grid-script', {{CORE_PREFIX}}_URL . '/assets/js/grid.js', array( 'jquery' ) , false );\r\n";
			$item['module']['mode']['{{module_mode_front_style_scripts}}'] .= "\r\n\t\t\twp_enqueue_style( '{{core-slug}}-front-grid', {{CORE_PREFIX}}_URL . '/assets/css/grid-front.css' );";
		}



		// licneing module
		if( !empty( $item['license_enabled'] ) ){

			// do license additions


			$item['module']['mode']['{{module_mode_core_actions}}'] .= file_get_contents( CYE_PATH . 'module-templates/mode/mode-license-enabled.php' );
			// replaces
			$item['module']['mode']['{{license_url}}'] = $item['store_url'];
			$item['module']['mode']['{{license_type}}'] = $item['license_type'];


		}

		
		$item['_exclude_files'] = $exclude_files;

		// copy main base template
		$this->recurse_copy( CYE_PATH . "caldera-framework-template", WP_PLUGIN_DIR . '/cy_' . $item['slug'], $item ); // prefixed with cy_ to prevent overwriting plugins
		

		if( !empty( $additional_files ) ){
			foreach( $additional_files as $module_file ){
				// copy module files
				$this->copy_filtered_file( $module_file['src'], $module_file['dst'], $item ); // prefixed with cy_ to prevent overwriting plugins
			}
		}
		if( !empty( $_REQUEST['item'] ) ){
			die;
		}

	}
	
	private function recurse_copy($src,$dst, $item) {
	    $dir = opendir($src); 
	    @mkdir($dst); 
	    while(false !== ( $file = readdir($dir)) ) {

	    	if( in_array( $src . '/' . $file, $item['_exclude_files'] ) || basename( $src ) === '.git' ){
	    		continue;
	    	}
	        if (( $file != '.' ) && ( $file != '..' )) { 
	            if ( is_dir($src . '/' . $file) ) {
	            	if( $file == 'inner_vendor' ){
	                	$this->recurse_copy($src . '/' . $file, $dst . '/vendor', $item);
	                }else{
	                	$this->recurse_copy($src . '/' . $file, $dst . '/' . $file, $item);
	                }
	            } 
	            else {
	            	$info = pathinfo( $file );

	            	if( in_array( $info['extension'], array('css','js','html','htm','po','php','txt', 'json') ) ){

	            		if( false !== strpos( $file , 'core-slug' ) ){
	            			$this->copy_filtered_file( $src . '/' . $file, $dst . '/' . str_replace( 'core-slug', str_replace( '_', '-', $item['slug'] ), $file ) , $item );
	            		}else{
	            			$this->copy_filtered_file( $src . '/' . $file, $dst . '/' . $file, $item );
	            		}

	            		

					}else{
						copy($src . '/' . $file,$dst . '/' . $file);
					}
	            } 
	        } 
	    } 
	    closedir($dir);
	}

	private function copy_filtered_file($src, $dst, $item){
		if( file_exists( $src ) ){
			// get content
			$content = file_get_contents( $src );
		}else{
			$content = $src;
		}

		// add in modules
		if( !empty( $item['module'] ) ){

			foreach( $item['module'] as $module ){

				foreach( (array) $module as $module_tag=>$module_code ){
					$content = str_replace( $module_tag, $module_code, $content);
				}

			}
		}

    	$renames = array(
    		"theme"				=>  array( "{{core_theme_header}}" => 'as_is' ),
    		"name"				=>	array( "{{Core_Name}}" => 'camel_space', "{{Core_Name_Space}}" => 'camel_underscore' ),
    		"short_name"		=>	array( "{{Core_Short_Name}}" => 'camel_space' ),
    		"dashicon"			=>	array( "{{core_icon}}" => 'as_is' ),
    		"primary_color"		=>	array( "{{core_color}}" => 'as_is' ),
    		"secondary_color"	=>	array( "{{core_secondary_color}}" => 'as_is' ),
			"slug"				=>	array( "{{Core_Class}}" => 'camel_underscore',"{{core-slug}}"=>'lower_dash',"{{core_slug}}" => 'lower_underscore', "{{core_item}}" => 'lower_underscore' ),
			"description"		=>	array( "{{Core_Description}}" => 'as_is' ),
			"prefix"			=>	array( "{{core_prefix}}" => 'lower_underscore', "{{CORE_PREFIX}}" => 'upper_underscore' ),
			"item_name"			=>	array( "{{Core_Item}}" => 'camel_space' ),
			"store_name"		=>	array( "{{core_object}}" => 'lower_underscore', "{{Core_Object}}" => 'camel_space' ),
			"save_button"		=>	array( "{{core_save_button}}" => 'as_is' ),
			"menu_location" 	=>	array( "{{core_menu_location}}" => 'as_is' ),
			"version"			=>	array( "{{core_version}}" => 'as_is' ),
		    "core_author_name"  =>  array( "{{core_author_name}}" => 'as_is' ),
		    "core_author_uri"	=>  array( "{{core_author_uri}}" => 'as_is' ),
		    "card_size"			=>	array( "{{core_card_size}}" => 'as_is' ),
		);

		foreach( $item as $field=>$replace ){
			if( !empty( $renames[$field] ) ){
				foreach( $renames[$field] as $string => $type ){

					switch ($type) {
						case 'camel_space':
							$replace = ucwords( str_replace('_', ' ', $replace ) );
							break;
						
						case 'camel_underscore':
							$replace = str_replace( ' ', '_', ucwords( str_replace( '_', ' ', $replace ) ) );
							break;
						case 'lower_dash':
							$replace = str_replace( '_', '-', sanitize_key( $replace ) );
							break;
						
						case 'lower_underscore':
							$replace = str_replace( '-', '_', sanitize_key( $replace ) );
							break;
						
						case 'upper_underscore':
							$replace = str_replace( '-', '_', strtoupper( sanitize_key( $replace ) ) );
							break;
					}

					$content = str_replace($string, $replace, $content);
					
				}
			}
		}
		$pointer = fopen($dst, 'w+');
		if( $pointer ){
			fwrite( $pointer, $content );
			fclose( $pointer );	
		}
	}
	/**
	 * Deletes a block
	 */
	public function delete_yellowstone_element(){

		$search_blocks = get_option( '_yellowstone_elements' );
		if( isset( $search_blocks[ $_POST['block'] ] ) ){
			delete_option( $search_blocks[$_POST['block']]['id'] );

			unset( $search_blocks[ $_POST['block'] ] );
			update_option( '_yellowstone_elements', $search_blocks );

			wp_send_json_success( $_POST );
		}
		
		wp_send_json_error( $_POST );

	}


	/**
	 * loads connection and returens tables
	 */
	public function save_config(){
		
		if( !empty( $_POST['config'] ) ){
			$yellowstone_elements = get_option( '_yellowstone_elements' );
			$config = json_decode( stripslashes_deep( $_POST['config'] ), true );

			if( isset( $config['id'] ) && !empty( $yellowstone_elements[ $config['id'] ] ) ){
				$updated_registery = array(
					'id'	=>	$config['id'],
					'name'	=>	$config['name'],
					'slug'	=>	$config['slug']
				);
				// add search form to registery
				if( !empty( $config['search_form'] ) ){
					$updated_registery['search_form'] = $config['search_form'];
				}
				
				$yellowstone_elements[$config['id']] = $updated_registery;
				update_option( '_yellowstone_elements', $yellowstone_elements );
			}

			update_option( $config['id'], $config );

			wp_send_json_success( $config );

		}

		// nope
		wp_send_json_error( $config );

	}

	/**
	 * create new yellowstone_element
	 */
	public function create_new_yellowstone_element(){
		
		$yellowstone_elements = get_option('_yellowstone_elements');
		if( empty( $yellowstone_elements ) ){
			$yellowstone_elements = array();
		}

		$yellowstone_element_id = uniqid('CYE').rand(100,999);
		if( !isset( $yellowstone_elements[ $yellowstone_element_id ] ) ){
			$new_yellowstone_element = array(
				'id'		=>	$yellowstone_element_id,
				'name'		=>	$_POST['name'],
				'slug'		=>	$_POST['slug'],
				'_current_tab' => '#caldera-panel-general'
			);
			update_option( $yellowstone_element_id, $new_yellowstone_element );
			$yellowstone_elements[ $yellowstone_element_id ] = $new_yellowstone_element;
		}

		update_option( '_yellowstone_elements', $yellowstone_elements );

		// end
		wp_send_json_success( $new_yellowstone_element );
	}

	/**
	 * Add options page
	 */
	public function add_settings_pages(){
		// This page will be under "Settings"
		
	
			$this->plugin_screen_hook_suffix['caldera_yellowstone'] =  add_menu_page( __( 'Caldera Yellowstone', $this->plugin_slug ), __( 'Yellowstone', $this->plugin_slug ), 'manage_options', 'caldera_yellowstone', array( $this, 'create_admin_page' ), 'dashicons-menu' );
			add_action( 'admin_print_styles-' . $this->plugin_screen_hook_suffix['caldera_yellowstone'], array( $this, 'enqueue_admin_stylescripts' ) );


	}


	/**
	 * Options page callback
	 */
	public function create_admin_page(){
		// Set class property        
		$screen = get_current_screen();
		$base = array_search($screen->id, $this->plugin_screen_hook_suffix);
			
		// include main template
		if( !empty( $_GET['build'] ) ){

			echo $this->build_new_plugin( $_GET['build'] );

		}elseif( !empty( $_GET['edit'] ) ){
			include CYE_PATH .'includes/edit.php';
		}else{
			include CYE_PATH .'includes/admin.php';
		}
		
		// php based script include
		if(file_exists( CYE_PATH .'assets/js/inline-scripts.php')){
			echo "<script type=\"text/javascript\">\r\n";
				include  CYE_PATH .'assets/js/inline-scripts.php';
			echo "</script>\r\n";
		}

	}

}

if( is_admin() )
	$settings_yellowstone_elements = new Settings_Caldera_Yellowstone();
