var caldera_canvas = false,
	cye_get_config_object,
	cye_record_change,
	cye_canvas_init,
	cye_get_default_setting,
	cye_code_editor,
	init_magic_tags,
	cye_rebuild_magics	,
	config_object = {},
	magic_tags = [];


jQuery( function($){

	init_magic_tags = function(){
		//init magic tags
		var magicfields = jQuery('.magic-tag-enabled');

		magicfields.each(function(k,v){
			var input = jQuery(v);
			
			if(input.hasClass('magic-tag-init-bound')){
				var currentwrapper = input.parent().find('.magic-tag-init');
				if(!input.is(':visible')){
					currentwrapper.hide();
				}else{
					currentwrapper.show();
				}
				return;			
			}
			var magictag = jQuery('<span class="dashicons dashicons-editor-code magic-tag-init"></span>'),
				wrapper = jQuery('<span style="position:relative;display:inline-block; width:100%;"></span>');

			if(input.is('input')){
				magictag.css('borderBottom', 'none');
			}

			if(input.hasClass('caldera-conditional-value-field')){
				wrapper.width('auto');
			}

			//input.wrap(wrapper);
			magictag.insertAfter(input);
			input.addClass('magic-tag-init-bound');
			if(!input.is(':visible')){
				magictag.hide();
			}else{
				magictag.show();
			}
		});

	}

	// internal function declarationas
	cye_get_config_object = function(el){

		var clicked 	= $(el),
			config 		= $('#caldera-live-config').val(),
			required 	= $('[required]'),
			clean		= true;

		for( var input = 0; input < required.length; input++ ){
			if( required[input].value.length <= 0 && $( required[input] ).is(':visible') ){
				$( required[input] ).addClass('caldera-input-error');
				clean = false;
			}else{
				$( required[input] ).removeClass('caldera-input-error');
			}
		}
		if( clean ){
			caldera_canvas = config;
		}
		clicked.data( 'config', config );

		return clean;
	}

	cye_record_change = function(){
		// hook and rebuild the fields list
		jQuery('#yellowstone_element-id').trigger('change');
		jQuery('#caldera-field-sync').trigger('refresh');
	}
	
	cye_canvas_init = function(){
		
		if( !caldera_canvas ){
			// bind changes
			jQuery('#caldera-main-canvas').on('keydown keyup change','input, select, textarea', function(e) {
				config_object = jQuery('#caldera-main-form').formJSON(); // perhaps load into memory to keep it live.
				jQuery('#caldera-live-config').val( JSON.stringify( config_object ) ).trigger('change');
			});
			// bind editor
			cye_init_editor();
			caldera_canvas = jQuery('#caldera-live-config').val();
			config_object = JSON.parse( caldera_canvas ); // perhaps load into memory to keep it live.
		}
		
		$('.color-field').wpColorPicker({
			change: function(obj){
				$('#yellowstone_element-id').trigger('change');
			}
		});
		
		jQuery('.dashicons-picker').dashiconsPicker();
		
		// rebuild tags
		cye_rebuild_magics();
	}
	cye_get_default_setting = function(obj){

		var id = 'node_' + Math.round(Math.random() * 99887766) + '_' + Math.round(Math.random() * 99887766),
			new_object = {},
			//config_object = JSON.parse( jQuery('#caldera-live-config').val() ), // perhaps load into memory to keep it live.
			trigger = ( obj.trigger ? obj.trigger : obj.params.trigger ),
			sub_id = ( trigger.data('group') ? trigger.data('group') : 'node_' + Math.round(Math.random() * 99887766) + '_' + Math.round(Math.random() * 99887766) );

		// add simple node
		if( trigger.data('addNode') ){
			// new node? add one
			if( !config_object[trigger.data('addNode')] ){
				config_object[trigger.data('addNode')] = {};
			}
			// add a new id to node
			config_object[trigger.data('addNode')][id] = {
				"_id" : id
			};
		}
		// remove simple node (all)
		if( trigger.data('removeNode') ){
			// new node? add one
			if( config_object[trigger.data('removeNode')] ){
				delete config_object[trigger.data('removeNode')];
			}

		}

		
		switch( trigger.data('script') ){
			
			case "remove-tab-panel":
				// remove the node
				if( config_object.tab[trigger.data('id')] ){
					delete config_object.tab[trigger.data('id')];
				}

				break;
			case "remove-admin-tab-panel":
				// remove the node
				if( config_object.admin_page[trigger.data('parent')].tab[trigger.data('id')] ){
					delete config_object.admin_page[trigger.data('parent')].tab[trigger.data('id')];
				}

				break;

			case "add-config-panel-field":
				// no fields, add one.

				if( !config_object.tab[trigger.data('id')].field ){
					config_object.tab[trigger.data('id')].field = {};
				}

				config_object.tab[trigger.data('id')].field[id] = { '_id' : id };

				break;

			case "add-admin-config-panel-field":
				// no fields, add one.

				if( !config_object.admin_page[trigger.data('parent')].tab[trigger.data('id')].field ){
					config_object.admin_page[trigger.data('parent')].tab[trigger.data('id')].field = {};
				}

				config_object.admin_page[trigger.data('parent')].tab[trigger.data('id')].field[id] = { '_id' : id };

				break;

			case "add-config-panel-element":
				// no fields, add one.

				if( !config_object.tab[trigger.data('id')].element ){
					config_object.tab[trigger.data('id')].element = {};
				}

				config_object.tab[trigger.data('id')].element[id] = { '_id' : id };

				break;
			case "add-admin-config-panel-element":
				// no fields, add one.

				if( !config_object.admin_page[trigger.data('parent')].tab[trigger.data('id')].element ){
					config_object.admin_page[trigger.data('parent')].tab[trigger.data('id')].element = {};
				}

				config_object.admin_page[trigger.data('parent')].tab[trigger.data('id')].element[id] = { '_id' : id };

				break;

			case "add-admin-page":
				// no fields, add one.
				if( !config_object.admin_page ){
					config_object.admin_page = {};
				}

				config_object.admin_page[id] = { '_id' : id };

				break;
			case "add-admin-tab-panel":
				// add admin page tab
				if( !config_object.admin_page[trigger.data('id')].tab ){
					config_object.admin_page[trigger.data('id')].tab = {};
				}

				config_object.admin_page[trigger.data('id')].tab[id] = { '_id' : id };

				break;

			case "new-tab":
				// no tab, add one.
				if( !config_object.tab ){
					config_object.tab = {};
				}

				config_object.tab[id] = { '_id' : id };

				break;
			case "toggle-tgm":
				// no tgm, add one.
				if( !config_object.tgm_enabled ){
					config_object.tgm_enabled = true;
				}else{
					delete config_object.tgm_enabled;
				}

				break;
			case "new-tgm":
				// no tgm, add one.
				if( !config_object.tgm ){
					config_object.tgm = {};
				}
				// no require, add one.
				if( !config_object.tgm.require ){
					config_object.tgm.require = {};
				}

				config_object.tgm.require[id] = { '_id' : id };

				break;
			case "toggle-composer":
				// no composer, add one.
				if( !config_object.composer_enabled ){
					config_object.composer_enabled = true;
				}else{
					delete config_object.composer_enabled;
				}

				break;
			case "new-composer":
				// no composer, add one.
				if( !config_object.composer ){
					config_object.composer = {};
				}
				// no require, add one.
				if( !config_object.composer.require ){
					config_object.composer.require = {};
				}

				config_object.composer.require[id] = { '_id' : id };

				break;
		}

		//console.log( config_object );

		jQuery('#caldera-live-config').val( JSON.stringify( config_object ) );
		jQuery('#caldera-field-sync').trigger('refresh');
	}
	// sutocomplete category
	$.widget( "custom.catcomplete", $.ui.autocomplete, {
		_create: function() {
			this._super();
			this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
		},
		_renderMenu: function( ul, items ) {
			var that = this,
			currentCategory = "";
			$.each( items, function( index, item ) {
				var li;
				if ( item.category != currentCategory ) {
					ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
					currentCategory = item.category;
				}
				li = that._renderItemData( ul, item );
				if ( item.category ) {
					li.attr( "aria-label", item.category + " : " + item.label );
				}
			});
		}
	});
	cye_rebuild_magics = function(){

		function split( val ) {
			return val.split( / \s*/ );
		}
		function extractLast( term ) {
			return split( term ).pop();
		}
		$( ".magic-tag-enabled" ).bind( "keydown", function( event ) {
			if ( event.keyCode === $.ui.keyCode.TAB && $( this ).catcomplete( "instance" ).menu.active ) {
				event.preventDefault();
			}
		}).catcomplete({
			minLength: 0,
			source: function( request, response ) {
				// delegate back to autocomplete, but extract the last term
				magic_tags = [];
				var category = '';
				// Primary Fields
				if( config_object.table && config_object.fields && this.element.data('exclude') !== 'fields' ){
					category = $('#caldera-label-field').text();
					for( var f in config_object.fields ){
						//console.log( f );
						magic_tags.push( { label: '{' + config_object.table + '.' + f + '}', category: category } );
					}
				}
				// Join Fields
				if( config_object.join ){
					category = $('#caldera-label-join').text();
					for( var j in config_object.join ){
						if( config_object.join[j].table && config_object.join[j].fields && this.element.data('exclude') !== 'fields' ){
							for( f in config_object.join[j].fields ){
								magic_tags.push( { label: '{' + config_object.join[j].table + '.' + f + '}', category: category }  );
							}							
						}
						
						//magic_tags.push( config_object.table + '.' + f );
					}
				}
				// Search form fields
				if( config_object.search_form && config_object.form_fields ){
					category = $('#caldera-label-search').text();
					for( f in config_object.form_fields ){
						magic_tags.push( { label: '%' + config_object.form_fields[f] + '%', category: category }  );
					}							
				}
				// variables
				if( config_object.variable  ){
					category = $('#caldera-label-variable').text();
					for( f in config_object.variable ){
						if( config_object.variable[f].slug.length && this.element.data('id') !== f ){
							magic_tags.push( { label: '{variable:' + config_object.variable[f].slug + '}', category: category }  );
						}
					}							
				}
				// Search form fields
				if( config_object.search_form && config_object.form_fields ){
					// set internal tags
					var system_tags = [
						'ip',
						'user:id',
						'user:user_login',
						'user:first_name',
						'user:last_name',
						'user:user_email',
						'get:*',
						'post:*',
						'request:*',
						'post_meta:*',
						'embed_post:ID',
						'embed_post:post_title',
						'embed_post:permalink',
						'embed_post:post_date',
						'date:Y-m-d H:i:s',
						'date:Y/m/d',
						'date:Y/d/m'
					];					
					category = $('#caldera-label-tags').text();
					for( f = 0; f < system_tags.length; f++ ){
						magic_tags.push( { label: '{' + system_tags[f] + '}', category: category }  );
					}							
				}
				

				response( $.ui.autocomplete.filter( magic_tags, extractLast( request.term ) ) );
			},
			focus: function() {
				// prevent value inserted on focus
				return false;
			},
			select: function( event, ui ) {
				var terms = split( this.value );
				// remove the current input
				terms.pop();
				// add the selected item
				terms.push( ui.item.value );
				// add placeholder to get the comma-and-space at the end
				//terms.push( "" );
				this.value = terms.join( " " );
				return false;
			}
		});
	}	

	// trash 
	$(document).on('click', '.caldera-card-actions .confirm a', function(e){
		e.preventDefault();
		var parent = $(this).closest('.caldera-card-content');
			actions = parent.find('.row-actions');

		actions.slideToggle(300);
	});

	// bind slugs
	$(document).on('keyup change', '[data-format="slug"]', function(e){

		var input = $(this);

		if( input.data('master') && input.prop('required') && this.value.length <= 0 && e.type === "change" ){
			this.value = $(input.data('master')).val().replace(/[^a-z0-9]/gi, '_').toLowerCase();
			if( this.value.length ){
				input.trigger('change');
			}
			return;
		}

		this.value = this.value.replace(/[^a-z0-9]/gi, '_').toLowerCase();
	});
	// bind keys
	$(document).on('keyup change', '[data-format="key"]', function(e){

		var input = $(this);

		if( input.data('master') && input.prop('required') && this.value.length <= 0 && e.type === "change" ){
			this.value = $(input.data('master')).val().replace(/[^a-z0-9]/gi, '-').toLowerCase();
			if( this.value.length ){
				input.trigger('change');
			}
			return;
		}

		this.value = this.value.replace(/[^a-z0-9]/gi, '-').toLowerCase();
	});
	
	// bind label update
	$(document).on('keyup change', '[data-sync]', function(){
		var input = $(this),
			sync = $(input.data('sync'));
		if( sync.is('input') ){
			sync.val( input.val() ).trigger('change');
		}else{
			sync.text(input.val());
		}
	});

	// bind tabs
	$(document).on('click', '.caldera-nav-tab > a', function(e){
		
		e.preventDefault();
		var clicked = $(this),
			tab_id = clicked.attr('href'),
			required 	= $('[required]'),
			clean		= true,
			tab;

		if( $( tab_id ).length ){
			tab = $( tab_id );
		}else{
			tab = $( '#' + tab_id );
		}

		for( var input = 0; input < required.length; input++ ){
			if( required[input].value.length <= 0 && $( required[input] ).is(':visible') ){
				$( required[input] ).addClass('caldera-input-error');
				clean = false;
			}else{
				$( required[input] ).removeClass('caldera-input-error');
			}
		}
		if( !clean ){
			return;
		}

		$('.caldera-nav-tab.active').removeClass('active');
		clicked.parent().addClass('active');

		$('.caldera-editor-panel').hide();
		tab.show();
		
		if( cye_code_editor ){
			cye_code_editor.toTextArea();
			cye_code_editor = false;
		}

		if( tab_id === '#caldera-panel-template' ){
			cye_init_editor();
			cye_code_editor.refresh();
			cye_code_editor.focus();
		}

		jQuery('#caldera-active-tab').val(tab_id).trigger('change');

	});

	// row remover global neeto
	$(document).on('click', '[data-remove-parent]', function(e){
		var clicked = $(this),
			parent = clicked.closest(clicked.data('removeParent')).remove();
		
		cye_record_change();
	});
	
	// init tags
	$('body').on('click', '.magic-tag-init', function(e){
		var clicked = $(this),
			input = clicked.prev();

		input.focus().trigger('init.magic');

	});

	// initialize live sync rebuild
	$(document).on('change', '[data-live-sync]', function(e){
		cye_record_change();
	});

	// initialise baldrick triggers
	$('.wp-baldrick').baldrick({
		request     : ajaxurl,
		method      : 'POST'
	});


	window.onbeforeunload = function(e) {

		if( caldera_canvas && caldera_canvas !== jQuery('#caldera-live-config').val() ){
			return true;
		}
	};


});







function cye_init_editor(){
	if( !jQuery('#caldera-code-editor').length ){
		return;
	}
	// custom modes
	var mustache = function(stream, state) {

		var ch;

		if (stream.match("{{")) {
			while ((ch = stream.next()) != null){
				if (ch == "}" && stream.next() == "}") break;
			}
			stream.eat("}");
			return "mustache";
		}
		/*
		if (stream.match("{")) {
			while ((ch = stream.next()) != null)
				if (ch == "}") break;
			stream.eat("}");
			return "mustacheinternal";
		}*/
		if (stream.match("%")) {
			while ((ch = stream.next()) != null)
				if (ch == "%") break;
			stream.eat("%");
			return "command";
		}

		/*
		if (stream.match("[[")) {
			while ((ch = stream.next()) != null)
				if (ch == "]" && stream.next() == "]") break;
			stream.eat("]");
			return "include";
		}*/
		while (stream.next() != null && 
			//!stream.match("{", false) && 
			!stream.match("{{", false) && 
			!stream.match("%", false) ) {}
			return null;
	};

	var options = {
		lineNumbers: true,
		matchBrackets: true,
		tabSize: 2,
		indentUnit: 2,
		indentWithTabs: true,
		enterMode: "keep",
		tabMode: "shift",
		lineWrapping: true,
		extraKeys: {"Ctrl-Space": "autocomplete"},
		};
	// base mode

	CodeMirror.defineMode("mustache", function(config, parserConfig) {
		var mustacheOverlay = {
			token: mustache
		};
		return CodeMirror.overlayMode(CodeMirror.getMode(config, parserConfig.backdrop || 'text/html' ), mustacheOverlay);
	});
	options.mode = "mustache";

	cye_code_editor = CodeMirror.fromTextArea(document.getElementById("caldera-code-editor"), options);
	cye_code_editor.on('keyup', tagFields);
	cye_code_editor.on('blur', function(cm){
		cm.save();
		jQuery( cm.getInputField() ).trigger('change');
	});

	return cye_code_editor;

}
(function() {
	"use strict";

	if( typeof CodeMirror === 'undefined' || caldera_canvas === false ){
		return;
	}

	var Pos         = CodeMirror.Pos;

	function getFields(cm, options) {

		var cur = cm.getCursor(), token = cm.getTokenAt(cur),
		result = [],
		fields = options.fields;

		if( cm.getMode().name === 'sqlmustache' ){
			options.mode = 'sqlmustache';
		}
		switch (options.mode){
			case 'mustache':
			var wrap = {start: "{{", end: "}}"},
			prefix = token.string.slice(2);
			break;
			case 'command':
			var wrap = {start: "%", end: "%"},
			prefix = token.string.slice(1);
			break;
			default:
			var wrap = {start: "", end: "}}"},
			prefix = token.string;
			break;
		}
		for( var field in fields){			
			if (field.indexOf(prefix) == 0 || prefix === '{' || fields[field].indexOf(prefix) == 0){
				if(prefix === '{'){
					wrap.start = '{';
				}
				result.push({text: wrap.start + field + wrap.end, displayText: fields[field]});
			}
		};

		return {
			list: result,
			from: Pos(cur.line, token.start),
			to: Pos(cur.line, token.end)
		};
	}
	CodeMirror.registerHelper("hint", "elementfield", getFields);
})();

function find_if_in_wrapper( open_entry, close_entry, cm ){
	in_entry = false;
	if( open_entry.findPrevious() ){
		

		// is entry. check if closed
		var open_pos  = open_entry.from();

		if( close_entry.findPrevious() ){
			// if closed after open then not in			
			var close_pos = close_entry.from();
			if( open_pos.line > close_pos.line ){
				// open is after close - on entry				
				in_entry = open_pos
			}else if( open_pos.line === close_pos.line ){
				// smame line - what point?
				if( open_pos.ch > close_pos.ch ){
					//after close - in entry
					in_entry = open_pos;
				}
			}else{
				open_entry 	= cm.getSearchCursor('{{#each ', open_pos);

				return find_if_in_wrapper( open_entry, close_entry, cm )
			}

		}else{
			in_entry = open_pos;
		}

	}

	// set the parent
	if( in_entry ){
		// find what tag is open
		var close_tag 	= cm.getSearchCursor( '}}', in_entry );
		if( close_tag.findNext() ){
			var close_pos	= close_tag.from();
				start_tag	= open_entry.to();
			
			in_entry = cm.getRange( start_tag, close_pos );

		}

	}

	return in_entry;
}

function tagFields(cm, e) {
	if( e.keyCode === 8 ){
		return; // no backspace.
	}
	//console.log( cm );
	var cur = cm.getCursor();

	// test search 
	var open_entry 	= cm.getSearchCursor('{{#each ', cur);
	var close_entry = cm.getSearchCursor('{{/each}}', cur);
	var open_if 	= cm.getSearchCursor('{{#if ', cur);
	var close_if 	= cm.getSearchCursor('{{/if', cur);	

	var in_entry 	= find_if_in_wrapper( open_entry, close_entry, cm );
	var in_if 		= false;





	if( open_if.findPrevious() ){
		// is if. check if closed
		var open_pos  = open_if.from();

		if( close_if.findPrevious() ){
			// if closed after open then not in			
			var close_pos = close_if.from();
			if( open_pos.line > close_pos.line ){
				// open is after close - on if
				in_if = true
			}else if( open_pos.line === close_pos.line ){
				// smame line - what point?
				if( open_pos.ch > close_pos.ch ){
					//after close - in if
					in_if = true;
				}
			}

		}else{
			in_if = true;
		}
	}

	if( in_if ){
		// find what tag is open
		var close_tag 	= cm.getSearchCursor( '}}', open_pos );
		if( close_tag.findNext() ){
			var close_pos	= close_tag.from();
				start_tag	= open_entry.to();
			
			in_if = cm.getRange( start_tag, close_pos );

		}

	}

	if (!cm.state.completionActive || e.keyCode === 18){
		var token = cm.getTokenAt(cur), prefix,
		prefix = token.string.slice(0);
		if(prefix){
			if(token.type){
				var fields = {};
				//console.log( token );
				if( token.type ){
					// only show fields within the entry
					if( in_entry ){
						
						if( !in_if ){
							// dont allow closing #each if in if
							fields = {
								"/each"			:	"/each"
							};
						}

						// ADD INDEX KEY
						fields['@key'] = "@key";

						jQuery('.caldera-autocomplete-in-entry-' + token.type).each(function(){
							var field = jQuery(this);

							if( !field.hasClass('parent-' + in_entry) && !field.hasClass('parant-all') ){
								return;
							}

							fields[field.data('slug')] = field.data('label');
							//fields["#each " + field.data('slug')] = "#each " + field.data('label');
							//if( !in_if ){
								if( field.data('label').indexOf('#') < 0 ){
									fields["#if " + field.data('slug')] = "#if " + field.data('label');
								}
							//}
							//fields["#unless " + field.data('slug')] = "#unless " + field.data('label');
						});
					}else{

						jQuery('.caldera-autocomplete-out-entry-' + token.type).each(function(){
							var field = jQuery(this);
							fields[field.data('slug')] = field.data('label');
							//fields["#each " + field.data('slug')] = "#each " + field.data('label');
							//if( !in_if ){
								if( field.data('label').indexOf('#') < 0 ){
									fields["#if " + field.data('slug')] = "#if " + field.data('label');
								}
							//}
							//fields["#unless " + field.data('slug')] = "#unless " + field.data('label');
						});

					}

					if( in_if ){
						fields['else'] = 'else';
						fields['/if'] = '/if';
					}
				}
				// sort hack
				var keys = [];
				var commands = [];
				var sorted_obj = {};

				for(var key in fields){
				    if(fields.hasOwnProperty(key)){
				    	if( key.indexOf('#') < 0 && key.indexOf('/') < 0 ){
				        	keys.push(key);
				    	}else{
				    		commands.push(key);
				    	}
				    }
				}

				// sort keys
				keys.sort();
				commands.sort();
				keys = keys.concat(commands);
				// create new array based on Sorted Keys
				jQuery.each(keys, function(i, key){
				    sorted_obj[key] = fields[key];
				});
				CodeMirror.showHint(cm, CodeMirror.hint.elementfield, {fields: sorted_obj, mode: token.type});

			}
		}
	}
	return;
}