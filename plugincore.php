<?php
/**
 * @package   Caldera_Yellowstone
 * @author    David Cramer <david@digilab.co.za>
 * @license   GPL-2.0+
 * @link      
 * @copyright 2014 David Cramer
 *
 * @wordpress-plugin
 * Plugin Name: Caldera Yellowstone
 * Plugin URI:  
 * Description: Output a new base plugin
 * Version:     1.0.0
 * Author:      David Cramer
 * Author URI:  
 * Text Domain: caldera-yellowstone
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path: /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define('CYE_PATH',  plugin_dir_path( __FILE__ ) );
define('CYE_URL',  plugin_dir_url( __FILE__ ) );
define('CYE_VER',  '1.0.0' );

// load internals
require_once( CYE_PATH . 'core-class.php' );
require_once( CYE_PATH . 'includes/settings.php' );


// Register hooks that are fired when the plugin is activated or deactivated.
// When the plugin is deleted, the uninstall.php file is loaded.
register_activation_hook( __FILE__, array( 'Caldera_Yellowstone', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'Caldera_Yellowstone', 'deactivate' ) );

// Load instance
add_action( 'plugins_loaded', array( 'Caldera_Yellowstone', 'get_instance' ) );
//Caldera_Yellowstone::get_instance();