<?php

/**
 * Caldera Yellowstone.
 *
 * @package   Caldera_Yellowstone
 * @author    David Cramer <david@digilab.co.za>
 * @license   GPL-2.0+
 * @link      
 * @copyright 2014 David Cramer
 */

/**
 * Plugin class.
 * @package Caldera_Yellowstone
 * @author  David Cramer <david@digilab.co.za>
 */
class Caldera_Yellowstone {

	/**
	 * @var     string
	 */
	const VERSION = CYE_VER;
	/**
	 * @var      string
	 */
	protected $plugin_slug = 'caldera-yellowstone';
	/**
	 * @var      object
	 */
	protected static $instance = null;
	/**
	 * @var      string
	 */
	protected $plugin_screen_hook_suffix = null;
	/**
	 * Initialize the plugin by setting localization, filters, and administration functions.
	 *
	 */
	private function __construct() {

		// Load plugin text domain
		add_action( 'init', array( $this, 'load_plugin_textdomain' ) );

		// Activate plugin when new blog is added
		add_action( 'wpmu_new_blog', array( $this, 'activate_new_site' ) );

		// Load admin style sheet and JavaScript.
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_stylescripts' ) );
		
	}


	/**
	 * checks and runs a search from
	 *
	 */
	public static function run_search_form() {

			global $form;


			if( !isset( $_POST['cyetream'] ) ){
				return;
			}
			$streams = get_option( '_yellowstone_elements' );
			if( empty( $streams ) || !isset( $streams[$_POST['cyetream']]) || $streams[$_POST['cyetream']]['search_form'] != $_POST['_cf_frm_id'] ){
				return;
			}

			echo self::render_stream( array('stream' => $streams[$_POST['cyetream']]['slug'], 'noform' => true ), false, false );
			exit;
	}

	/**
	 * Return an instance of this class.
	 *
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Fired when the plugin is activated.
	 *
	 *
	 * @param    boolean    $network_wide    True if WPMU superadmin uses "Network Activate" action, false if WPMU is disabled or plugin is activated on an individual blog.
	 */
	public static function activate( $network_wide ) {
		if ( function_exists( 'is_multisite' ) && is_multisite() ) {
			if ( $network_wide  ) {
				// Get all blog ids
				$blog_ids = self::get_blog_ids();

				foreach ( $blog_ids as $blog_id ) {
					switch_to_blog( $blog_id );
					self::single_activate();
				}
				restore_current_blog();
			} else {
				self::single_activate();
			}
		} else {
			self::single_activate();
		}
	}

	/**
	 * Fired when the plugin is deactivated.
	 *
	 *
	 * @param    boolean    $network_wide    True if WPMU superadmin uses "Network Deactivate" action, false if WPMU is disabled or plugin is deactivated on an individual blog.
	 */
	public static function deactivate( $network_wide ) {
		if ( function_exists( 'is_multisite' ) && is_multisite() ) {
			if ( $network_wide ) {
				// Get all blog ids
				$blog_ids = self::get_blog_ids();

				foreach ( $blog_ids as $blog_id ) {
					switch_to_blog( $blog_id );
					self::single_deactivate();
				}
				restore_current_blog();
			} else {
				self::single_deactivate();
			}
		} else {
			self::single_deactivate();
		}
	}

	/**
	 * Fired when a new site is activated with a WPMU environment.
	 *
	 *
	 * @param	int	$blog_id ID of the new blog.
	 */
	public function activate_new_site( $blog_id ) {
		if ( 1 !== did_action( 'wpmu_new_blog' ) )
			return;

		switch_to_blog( $blog_id );
		self::single_activate();
		restore_current_blog();
	}

	/**
	 * Get all blog ids of blogs in the current network that are:
	 * - not archived
	 * - not spam
	 * - not deleted
	 *
	 *
	 * @return	array|false	The blog ids, false if no matches.
	 */
	private static function get_blog_ids() {
		global $wpdb;

		// get an array of blog ids
		$sql = "SELECT blog_id FROM $wpdb->blogs
			WHERE archived = '0' AND spam = '0'
			AND deleted = '0'";
		return $wpdb->get_col( $sql );
	}

	/**
	 * Fired for each blog when the plugin is activated.
	 *
	 */
	private static function single_activate() {
		// TODO: Define activation functionality here if needed
	}

	/**
	 * Fired for each blog when the plugin is deactivated.
	 *
	 */
	private static function single_deactivate() {
		// TODO: Define deactivation functionality here needed
	}

	/**
	 * Load the plugin text domain for translation.
	 *
	 */
	public function load_plugin_textdomain() {
		load_plugin_textdomain( $this->plugin_slug, FALSE, basename( CYE_PATH ) . '/languages');
	}
	
	/**
	 * Register and enqueue admin-specific style sheet.
	 *
	 *
	 * @return    null
	 */
	public function enqueue_admin_stylescripts() {

		$screen = get_current_screen();

		if( false !== strpos( $screen->base, 'caldera_yellowstone' ) ){

			//wp_enqueue_style( 'font-awesome', CYE_URL . '/assets/css/font-awesome.min.css' );
			wp_enqueue_style( 'caldera_yellowstone-core-style', CYE_URL . '/assets/css/styles.css' );
			wp_enqueue_style( 'caldera_yellowstone-baldrick-modals', CYE_URL . '/assets/css/modals.css' );
			wp_enqueue_script( 'caldera_yellowstone-wp-baldrick', CYE_URL . '/assets/js/wp-baldrick-full.js', array( 'jquery' ) , false, true );
			wp_enqueue_script( 'jquery-ui-autocomplete' );
			wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_script( 'wp-color-picker' );
			wp_enqueue_script( 'jquery-ui-autocomplete' );
			wp_enqueue_script( 'jquery-ui-sortable' );
						
			if( !empty( $_GET['edit'] ) ){
				wp_enqueue_style( 'caldera_yellowstone-codemirror-style', CYE_URL . '/assets/css/codemirror.css' );
				wp_enqueue_script( 'caldera_yellowstone-codemirror-script', CYE_URL . '/assets/js/codemirror.js', array( 'jquery' ) , false );
				wp_enqueue_style( 'caldera_yellowstone-dashicons-picker', CYE_URL . '/assets/css/dashicons-picker.css' );
				wp_enqueue_script( 'caldera_yellowstone-dashicons-picker-script', CYE_URL . '/assets/js/dashicons-picker.js', array( 'jquery' ) , false );

			}
			
			wp_enqueue_script( 'caldera_yellowstone-core-script', CYE_URL . '/assets/js/scripts.js', array( 'jquery' ) , false );
			
		}

	}
	
}















