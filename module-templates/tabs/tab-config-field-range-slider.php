
		<div class="{{core-slug}}-config-group">
			<label for="{{module_tab_field_id}}">
				<?php esc_html_e( '{{module_tab_field_name}}', '{{core-slug}}' ); ?>
			</label>
			<input id="{{module_tab_field_id}}" type="text" name="{{module_tab_field_input_name}}" value="{{{{module_tab_field_value}}}}" {{module_tab_field_required}}> <span id="{{module_tab_field_id}}_value">{{{{module_tab_field_value}}}}</span>{{module_tab_field_range_suffix}}
			{{module_tab_field_description}}
		</div>
		{{#script}}
			jQuery(function($){
				$('#{{module_tab_field_id}}').simpleSlider({
					target		:	$("#{{module_tab_field_id}}_value"),
					range		:	[{{module_tab_field_range_min_value}},{{module_tab_field_range_max_value}}],
					step		:	"{{module_tab_field_range_step}}",
					dragger		:	"button-primary dragger",
					highlight	:	"highlight-track button-primary",
				});
			});
		{{/script}}
