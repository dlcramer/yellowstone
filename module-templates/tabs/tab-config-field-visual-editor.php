	<div class="{{core-slug}}-config-group">
		<label for="{{module_tab_field_id}}">
			<?php _e( '{{module_tab_field_name}}', '{{core-slug}}' ); ?>
		</label>

		<?php
			remove_editor_styles();
			wp_editor( '{{{{module_tab_field_value}}}}', '{{module_tab_field_id}}' , array('textarea_name' => '{{module_tab_field_input_name}}' ) );
		?>
		{{module_tab_field_description}}
	</div>
