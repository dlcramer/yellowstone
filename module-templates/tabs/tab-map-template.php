<?php

	// Panel template for Google Maps Setup

?>
<input id="{{tab_slug}}_search" name="{{tab_slug}}[address]" value="{{{{tab_slug}}/address}}" class="controls" type="text" placeholder="Search Box" style="margin-bottom:30px; height:30px;background-color: #fff; padding: 0 11px 0 13px; width: 400px; font-size: 15px; font-weight: 300; text-overflow: ellipsis;">
<div id="{{tab_slug}}_map_canvas" style="width:100%; height:500px;"></div>
<input type="hidden" id="{{tab_slug}}_place_id" name="{{tab_slug}}[place_id]" value="{{{{tab_slug}}/place_id}}">
{{#script type="text/javascript"}}
//<script>
var {{tab_slug}}_gmap_initialize;
if( typeof map_enabled === 'undefined'){
	var map_enabled = false;
}
jQuery( function($){

	var {{tab_slug}}_map_init = false;

	{{tab_slug}}_gmap_initialize = function() {	  


		var request = {
			placeId: '{{{{tab_slug}}/place_id}}'
		};

		var map = new google.maps.Map(document.getElementById('{{tab_slug}}_map_canvas'), {
			center: ( request.placeId === '' ? new google.maps.LatLng(0.340269, -18.006598) : new google.maps.LatLng() ),
			zoom: 3
		});

		var infowindow = new google.maps.InfoWindow({ maxWidth: 1500 });
		var service = new google.maps.places.PlacesService(map);
		var markers = [];

		if( request.placeId !== '' ){
			service.getDetails(request, function(place, status) {
				if (status == google.maps.places.PlacesServiceStatus.OK) {
					var marker = new google.maps.Marker({
						map: map,
						position: place.geometry.location
					});
			var content = '<strong>' + place.name + '</strong>';//<br>' + place.adr_address;
			if( place.website ){
				content += '<br><a href="' + place.website + '" target="_blank">' + place.website + '</a>';
			}
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.setContent(content);
				infowindow.open(map, marker);
			});
			infowindow.setContent(content);

			infowindow.open(map, marker);
			markers.push( marker );

			map.fitBounds( new google.maps.LatLngBounds(place.geometry.location) );
			map.setZoom(14);        
		}
	});    
		}

	  // Create the search box and link it to the UI element.
	  var input = /** @type {HTMLInputElement} */(
	  	document.getElementById('{{tab_slug}}_search'));
	  //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

	  
	  var searchBox = new google.maps.places.SearchBox(input);
	  searchBox.bindTo('bounds', map);

	  google.maps.event.addListener(searchBox, 'places_changed', function() {
	  	for (var i = 0; i < markers.length; i++) {
	  		markers[i].setMap(null);
	  	}
	  	markers = [];

	  	var places = searchBox.getPlaces();
	  	if (places.length == 0) {
	  		return;
	  	}
		// For each place, get the icon, place name, and location.
		var bounds = new google.maps.LatLngBounds();
		for (var i = 0, place; place = places[i]; i++) {

			bounds.extend(place.geometry.location);

			service.getDetails(place, function(place, status) {
				if (status == google.maps.places.PlacesServiceStatus.OK) {
					var marker = new google.maps.Marker({
						map: map,
						position: place.geometry.location
					});
			var content = '<strong>' + place.name + '</strong>';//<br>' + place.adr_address;
			if( place.website ){
				content += '<br><a href="' + place.website + '" target="_blank">' + place.website + '</a>';
			}

			google.maps.event.addListener(marker, 'click', function() {
				infowindow.setOptions({maxWidth: 1500});
				infowindow.setContent(content);
				infowindow.open(map, this);
				jQuery('#{{tab_slug}}_place_id').val( place.place_id ).trigger('change');
				jQuery('#{{tab_slug}}_search').val( place.formatted_address );
			});


			markers.push( marker );
		}
	});
		}
		map.fitBounds(bounds);
		if( places.length === 1 ){
			map.setZoom(14);
		}
	});

}


function {{tab_slug}}_loadScript() {
	if( !map_enabled ){
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&' +
		'callback={{tab_slug}}_gmap_initialize';
		document.body.appendChild(script);
		map_enabled = true;
	}

}

{{tab_slug}}_loadScript();
$(document).on('change', '#{{core-slug}}-active-tab', function(){

	if( false === {{tab_slug}}_map_init ){
		{{tab_slug}}_gmap_initialize();
	}
});

});


{{/script}}