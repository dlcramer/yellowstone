<?php
/**
 * @author    David Cramer <david@digilab.co.za>
 * @license   GPL-2.0+
 * @link
 * @copyright 2015 David Cramer <david@digilab.co.za>
 */
?>
<input type="hidden" name="_open_{{tab_slug}}" value="{{_open_{{tab_slug}}}}">
<div class="{{core-slug}}-fixed-list">
	<div class="postbox">
			<button type="button" class="button button-small wp-baldrick {{core-slug}}-list-control-button" data-add-node="{{tab_slug}}"><?php esc_html_e( 'Add Item', '{{core-slug}}' ); ?></button>
			<h3 style="font-size: 14px;line-height: 1.4;margin: 0;padding: 8px 9pt; border-bottom: 1px solid #eee;" >
				<span><?php esc_html_e( 'List Panel', '{{core-slug}}' ); ?></span>
			</h3>
			<div style="height:auto; overflow:auto;">
				<table class="striped" style="width:100%;">
					<tbody>
					{{#each {{tab_slug}}}}
						<tr>
							<td style="{{#is @root/_open_{{tab_slug}} value=_id}}background: {{core_color}} none repeat scroll 0% 0%; color: rgb(255, 255, 255);{{/is}}">
								{{:node_point}}
								<label style="padding: 11px;text-transform: capitalize;display: block;">{{_id}}<input type="checkbox" name="_open_{{tab_slug}}" value="{{_id}}" data-live-sync="true" style="display:none;"></label>
							</td>
						</tr>
					{{/each}}
					</tbody>
				</table>
			</div>
		</div>
</div>
<div class="{{core-slug}}-fixed-center">
	{{#find @root/{{tab_slug}} @root/_open_{{tab_slug}}}}
		<h3 style="border-bottom: 1px solid rgb(207, 207, 207); margin: 8px 0px; padding: 0px 0px 11px;">
		{{_id}}
		</h3>
		<div style="overflow: auto; height: 100%;">
			<?php
			// Template Here
			?>
		</div>
	{{/find}}

</div>
