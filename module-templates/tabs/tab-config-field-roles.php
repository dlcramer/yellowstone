
		<div class="{{core-slug}}-config-group">
			<label for="{{module_tab_field_id}}">
				<?php esc_html_e( '{{module_tab_field_name}}', '{{core-slug}}' ); ?>
			</label>
			<div style="margin: -19px 0px 0px 185px;">
				<label style="width: auto; display: block;"><input type="checkbox" data-live-sync="true" name="{{module_tab_field_input_name}}[_all_]" value="1" {{#if {{module_tab_field_value}}/_all_}}checked="checked"{{/if}}>
					<?php esc_html_e( 'All Roles', '{{core-slug}}' ); ?>
				</label>
				{{#unless {{module_tab_field_value}}/_all_}}
				<?php

				global $wp_roles;
				$all_roles = $wp_roles->roles;
				$editable_roles = apply_filters( 'editable_roles', $all_roles);

				foreach($editable_roles as $role=>$role_details){
					if( 'administrator' === $role){
						continue;
					}
					?>

					<label style="width: auto; display: block;"><input type="checkbox" name="{{module_tab_field_input_name}}[<?php echo $role; ?>]" value="1" {{#if {{module_tab_field_value}}/<?php echo $role; ?>}}checked="checked"{{/if}}>
						<?php echo $role_details['name']; ?>
					</label>
					<?php
				}
				?>

				{{/unless}}
			</div>
			{{module_tab_field_description}}
		</div>
