<?php
	// Element trigger for {{tab_item_title}}
?>
{{#script type="text/html" id="{{tab_slug}}_{{tab_item_slug}}_modal_form"}}
	
	<input type="hidden" name="_{{tab_item_slug}}" value="1">
	<textarea id="html-template-code" data-mode="text/html" name="code">\{{code}}</textarea>
	\{{#script}}
		var this_editor = {{core_prefix}}_init_editor('html-template-code');
		setTimeout( function(){
			this_editor.refresh();
			this_editor.focus();
		}, 100 );
	\{{/script}}


{{/script}}