
		<div class="{{core-slug}}-config-group">
			<label for="{{module_tab_field_id}}">
				<?php esc_html_e( '{{module_tab_field_name}}', '{{core-slug}}' ); ?>
			</label>
			<textarea id="{{module_tab_field_id}}" name="{{module_tab_field_input_name}}" style="width: 373px; height: 202px;" {{module_tab_field_required}}>{{{{module_tab_field_value}}}}</textarea>
			{{module_tab_field_description}}
		</div>
		<br>
