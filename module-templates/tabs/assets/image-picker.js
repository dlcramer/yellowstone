// Uploading files
var image_picker_frame;

jQuery(function($){

	var target = null;

	$(document).on('click', '.caldera-image-remover', function( e ){
		
		e.preventDefault();
		var clicked = $( this );
		
		target = $( clicked.data('target') );

		if( !target.length ){
			alert(' No Target set');
			return;
		}

		target.val('{}').trigger('change');
	});

	$(document).on('click', '.caldera-image-picker', function( e ){
		
		e.preventDefault();
		var clicked = $( this );

		target = $( clicked.data('target') );
			
		if( !target.length ){
			
			if( !clicked.data('node') ){
				alert(' No Target set');
				return;
			}else{
				target = null	;
			}
		}

		var select_handler = function(e){
			image_picker_frame.state().get('selection').each( function( attach ){
			
				attachment = attach.toJSON();
				if( typeof attachment.sizes.medium  !== 'undefined' ){
					attachment.thumbnail = attachment.sizes.medium.url;
				}else if( typeof attachment.sizes.full !== 'undefined' ){
					attachment.thumbnail = attachment.sizes.full.url;
				}else{
					attachment.thumbnail = attachment.url;
				}				
				if( target ){
					target.val( JSON.stringify( attachment ) ).trigger('change');
					return;
				}
				var new_node = {
					image	:	{
						selection : attachment
					}
				};
				if( clicked.data('node') ){
					{{core_prefix}}_add_node( clicked.data('node'), new_node );
				}
			});

			{{core_prefix}}_rebuild_canvas();
		};
		if ( !image_picker_frame ) {

			// Create the media frame.
			image_picker_frame = wp.media({
				title: clicked.data( 'title' ),
				button: {
					text: clicked.data( 'button' ),
				},
				library: { type: 'image'},
				multiple: true
			});
			image_picker_frame.on( 'select', select_handler);
		}		
		image_picker_frame.open();
	});

})
