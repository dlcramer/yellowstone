	{{#each fields}}
		<div class="field-parent" {{#if _node_point}}id="field-{{_id}}"{{/if}}>
			<div class="field-item {{#if key}}{{#is key value="PRI"}}primary-index{{else}}index-field{{/is}}{{/if}}" value="{{field}}" {{#is ../../field value=field}}selected="selected"{{/is}}>
				{{:node_point}}
				{{#if field}}
					{{field}} - {{key}}
				{{else}}
					<input type="text" name="{{:name}}[name]" data-format="slug" value="{{name}}" data-live-sync="true">
				{{/if}}
				<span class="field-action">
					<span class="dashicons dashicons-plus wp-baldrick"
						data-add-node="{{#if _node_point}}{{_node_point}}.{{else}}{{tab_slug}}.tables.{{@root/{{tab_slug}}/table}}.{{@contextPath}}.{{/if}}fields"
						data-node-default='{"name":"{{#if field}}{{field}}{{else}}{{name}}{{/if}}_clone"}'>
					</span>
					{{#if _node_point}}
					<span class="dashicons dashicons-no" data-remove-element="#field-{{_id}}"></span>
					{{/if}}
				</span>
			</div>
			{{#if fields}}
			<div class="field-child">
				{{> {{tab_slug}}-db_field}}
			</div>
			{{/if}}
		</div>
	{{/each}}