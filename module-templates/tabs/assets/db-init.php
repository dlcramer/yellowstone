//<?php
// init stuff to get databases and tables for the filter thing
global $wpdb;

$tables = $wpdb->get_results( "SHOW TABLES", ARRAY_N );
foreach( $tables as $table ){
	if( empty( ${{core_item}}['{{tab_slug}}']['tables'][ $table[0] ] ) ){
		${{core_item}}['{{tab_slug}}']['tables'][ $table[0] ] = array(
			'name'		=>	$table[0],
			'fields'	=> array()
		);
	}
	$fields = $wpdb->get_results("SHOW COLUMNS IN " . $table[0], ARRAY_A );
	foreach( $fields as $field ){
		foreach( $field as $field_key => $field_val ){
			${{core_item}}['{{tab_slug}}']['tables'][ $table[0] ][ 'fields' ][ $field['Field'] ][ strtolower( $field_key ) ] = $field_val;
		}
	}
}
