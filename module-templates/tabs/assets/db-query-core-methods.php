//<?php
	/**
	 * pricess an entry association
	 *
	 *
	 * @param $processors
	 *
	 * @return mixed
	 */
	private static function filter_compare( $filter ){
		global $wpdb;

		switch ( $filter['compare'] ) {
			case "is":
				$return = $wpdb->prepare( " = %s ", $filter['value'] );
				break;
			case "isnot":
				$return = $wpdb->prepare( " != %s ", $filter['value'] );
				break;
			case "isin":
				$list = explode(',', $filter['value']);
				$list = array_filter( $list, 'trim' );
				$qlist = array();
				foreach( $list as $item ){
					$qlist[] = $wpdb->prepare( "%s", $item );
				}
				$return = " IN (" . implode( ",", $qlist ) .")";
				break;
			case "isnotin":
				$list = explode(',', $filter['value']);
				$list = array_filter( $list, 'trim' );
				$qlist = array();
				foreach( $list as $item ){
					$qlist[] = $wpdb->prepare( "%s", $item );
				}
				$return = " NOT IN (" . implode( ",", $qlist ) .")";
				break;
			case "greater":
				$return = $wpdb->prepare( " > %s ", $filter['value'] );
				break;
			case "greatereq":
				$return = $wpdb->prepare( " >= %s ", $filter['value'] );
				break;
			case "smaller":
				$return = $wpdb->prepare( " < %s ", $filter['value'] );
				break;
			case "smallereq":
				$return = $wpdb->prepare( " <= %s ", $filter['value'] );
				break;
			case "startswith":
				$return = $wpdb->prepare( " LIKE %s ", $filter['value'] . '%' );
				break;
			case "endswith":
				$return = $wpdb->prepare( " LIKE %s ", '%' . $filter['value'] );
				break;
			case "contains":
				$return = $wpdb->prepare( " LIKE %s ", '%' . $filter['value'] . '%' );
				break;
		}

		return "`prim`.`" . $filter['field'] . "`" . $return;
	
	}

	/**
	 * build query
	 *
	 *
	 * @param $query. the database query config
	 *
	 * @return mixed
	 */
	private static function build_database_query( $query ){
		global $wpdb;
		// parent lock
		$parent_filter = null;
		$parent_join = null;
		$search = array();
		if( !empty( $query['filters'] ) ){

			$filter_joins = array();
			foreach( $query['filters'] as $filter ){
				if( empty( $filter['field'] ) || strlen( $filter['value'] ) < 1 ){
					continue;
				}
				$keys = array();
				$keys[] = self::filter_compare( $filter );
				if( !empty( $filter['or'] ) ){
					foreach( $filter['or'] as $or_filter ){
						$keys[] = self::filter_compare( $or_filter );
					}
				}
				$search[] = '(' . implode(' OR ', $keys ) . ')';
				
			}
			
		}
		$search = implode( " AND \n", $search );

		// counter
		$query = " SELECT
		
		`prim`.*

		FROM `" . $query['table'] . "` AS `prim`
		" . $parent_join . "
		WHERE
		" . $parent_filter . "
		" . $search . "
		";

		$results = $wpdb->get_results( $query );

		// do stuf and output
		return( $results );
	}