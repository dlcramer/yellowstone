	<button type="button" class="button button-small caldera-image-picker" data-node="{{tab_slug}}" data-title="<?php echo esc_attr( __('{{tab_title}}', '{{core-slug}}') ); ?>">
		<?php esc_html_e( '{{tab_add}}', '{{core-slug}}' ); ?>
	</button>
	<input type="hidden" id="{{tab_slug}}_editing" name="{{tab_slug}}_editing" value="{{{{tab_slug}}_editing}}" data-live-sync="true">
	<br>
	<br>
	{{#unless {{tab_slug}}}}
		<p class="description">
			<?php esc_html_e( "{{tab_no_images}}", '{{core-slug}}' ); ?>
		</p>
	{{/unless}}
	<div class="{{core-slug}}-group-wrapper {{core-slug}}-group-sorter" style="width: auto; margin: -{{tab_margin}}px;">
		{{#each {{tab_slug}}}}
		<div class="node-wrapper {{#is @root/{{tab_slug}}_editing value=_id}}editing{{/is}}" style="background-color: {{tab_tile_color}};border-radius: {{tab_radius}}px {{#is @root/{{tab_slug}}_editing value=_id}}{{tab_radius}}px 0 0{{/is}}; margin: {{tab_margin}}px;width:{{tab_width}}px;height:{{tab_height}}px;border: {{tab_border_width}}px solid {{tab_border_color}};">
			{{:node_point}}
			<div class="node-image-box" style="border-radius: {{tab_radius}}px {{#is @root/{{tab_slug}}_editing value=_id}}{{tab_radius}}px 0 0{{/is}};height:{{tab_height}}px;padding:{{tab_padding}}px;">
				<div class="{{core-slug}}-config-group" style="height: 100%;">

					<div class="image-picker-content" style="height: 100%;">
						
						<label class="node-image-tool caldera-image-picker" data-target="#{{core-slug}}-{{tab_slug}}-image-{{_id}}_selection" data-title="<?php echo __('{{tab_title}}', '{{core-slug}}'); ?>" style="position: absolute; padding: {{tab_icon_padding}}px; border-radius: {{tab_image_radius}}px 0px {{tab_icon_radius}}px;background: rgba(0, 0, 0, 0.5) none repeat scroll 0% 0%; color: rgb(255, 255, 255);"><span class="dashicons dashicons-edit"></span></label>
						<label class="node-image-tool" data-confirm="<?php echo esc_attr( __( '{{tab_confirm}}', '{{core-slug}}' ) ); ?>" data-remove-parent=".node-wrapper" style="position: absolute; right:{{tab_padding}}px; padding: {{tab_icon_padding}}px; border-radius: 0px {{tab_image_radius}}px 0 {{tab_icon_radius}}px;background: rgba(0, 0, 0, 0.5) none repeat scroll 0% 0%; color: rgb(255, 255, 255);"><span class="dashicons dashicons-no"></span></label>
						<label class="node-image-tool" style="position: absolute; bottom: {{tab_padding}}px; padding: {{tab_icon_padding}}px; {{#is @root/{{tab_slug}}_editing value=_id}}border-radius: 0 {{tab_icon_radius}}px 0 0;color: rgb(0, 0, 0); background: rgb(255, 255, 255) none repeat scroll 0% 0%;{{else}}border-radius: 0 {{tab_icon_radius}}px 0 {{tab_image_radius}}px;background: rgba(0, 0, 0, 0.5) none repeat scroll 0% 0%; color: rgb(255, 255, 255);{{/is}}"><span class="dashicons dashicons-menu"></span><input style="display:none;" type="radio" id="{{tab_slug}}_editing_{{_id}}" name="{{tab_slug}}_editing" data-live-sync="true" {{#is @root/{{tab_slug}}_editing value=_id}}value="_none"{{else}} value="{{_id}}" {{/is}} {{#unless image/selection}}checked="checked"{{/unless}}></label>					

						<span class="sortable-item image-handle {{#unless image/selection/thumbnail}}dashicons dashicons-format-image{{/unless}}" 
							style="box-shadow: 0 0 20px rgba(0,0,0,0.2) inset; cursor: move; {{#if image/selection/thumbnail}}background: {{tab_contain_color}} url('{{image/selection/thumbnail}}') no-repeat scroll center center / {{tab_background_size}};{{/if}} display:block; margin: 0; border-radius: {{tab_image_radius}}px {{#is @root/{{tab_slug}}_editing value=_id}}{{tab_image_radius}}px 0 0{{/is}}; overflow: hidden; height: 100%; width: 100%; font-size: 85px; color: #d7d7d7; line-height: 151px;"></span>
					</div>
					
					<input id="{{core-slug}}-{{tab_slug}}-image-{{_id}}_selection" name="{{:name}}[image][selection]" data-live-sync="true" type="hidden" value="{{json image/selection}}">
				</div>
			</div>

			<div class="node-config-box" style="border-radius: 0 0 {{tab_radius}}px {{tab_radius}}px;padding: {{tab_box_padding}}px;margin:-{{tab_padding}}px -{{tab_border_width}}px -{{tab_border_width}}px -{{tab_border_width}}px;border: {{tab_border_width}}px solid {{tab_border_color}};border-top:0;">

				<div class="{{core-slug}}-config-group node-config-group">
					<input id="{{core-slug}}-{{tab_slug}}-title-{{_id}}" type="text" class="regular-text" name="{{:name}}[title]" value="{{#if title}}{{title}}{{else}}{{image/selection/title}}{{/if}}" required="required" style="width:100%;" placeholder="<?php echo esc_attr( __( 'Title', '{{core-slug}}' ) ); ?>">
				</div>
				<div class="{{core-slug}}-config-group node-config-group">
					<input id="{{core-slug}}-{{tab_slug}}-caption-{{_id}}" type="text" class="regular-text" name="{{:name}}[caption]" value="{{#if caption}}{{caption}}{{else}}{{image/selection/caption}}{{/if}}" style="width:100%;" placeholder="<?php echo esc_attr( __( 'Caption', '{{core-slug}}' ) ); ?>">
				</div>

				{{module_tab_config_panel_fields}}

			</div>
		
		</div>
		{{/each}}


	</div>
