<?php
/**
 * @author    David Cramer <david@digilab.co.za>
 * @license   GPL-2.0+
 * @link
 * @copyright 2015 David Cramer <david@digilab.co.za>
 */
?>
<input type="hidden" name="{{tab_slug}}[tables]" value="{{json {{tab_slug}}/tables}}">
<select name="{{tab_slug}}[table]" data-live-sync="true">
	<option></option>
	{{#each {{tab_slug}}/tables}}
		<option value="{{name}}" {{#is @root/{{tab_slug}}/table value=name}}selected="selected"{{/is}}>{{name}}</option>
	{{/each}}
</select>
{{#if {{tab_slug}}/table}}
	<div class="filter-wrapper">
		{{#each {{tab_slug}}/filters}}
		<div class="filter-line-{{_id}}">
			<div class="filter-item filter-{{type}}">
				<span class="filter-remove-line" data-remove-element=".filter-line-{{_id}}"><span class="dashicons dashicons-no-alt"></span></span>
				{{:node_point}}
				<input type="hidden" name="{{:name}}[type]" value="{{type}}">
				<span class="filter-clause">{{#is type value="and"}}{{#unless @first}}<?php esc_html_e('and', 'cf-reports'); ?>{{else}}<?php esc_html_e('where', 'cf-reports'); ?>{{/unless}}{{else}}<?php esc_html_e('or', 'cf-reports'); ?>{{/is}}</span>
				<div class="filter-field">
					<select class="filter-field-select" name="{{:name}}[field]" data-live-sync="true" required="required">
						{{#find @root/{{tab_slug}}/tables @root/{{tab_slug}}/table}}
							{{#each fields}}
								<option value="{{field}}" {{#is ../../field value=field}}selected="selected"{{/is}}>{{field}}</option>
							{{/each}}
						{{/find}}
					</select>
					<select class="filter-field-compare" name="{{:name}}[compare]">
						<option value="is" {{#is compare value="is"}}selected="selected"{{/is}}>is</option>
						<option value="isnot" {{#is compare value="isnot"}}selected="selected"{{/is}}>is not</option>
						<option value="isin" {{#is compare value="isin"}}selected="selected"{{/is}}>in</option>
						<option value="isnotin" {{#is compare value="isnotin"}}selected="selected"{{/is}}>not in</option>
						<option value="greater" {{#is compare value="greater"}}selected="selected"{{/is}}>greater than</option>
						<option value="greatereq" {{#is compare value="greatereq"}}selected="selected"{{/is}}>greater or equal</option>
						<option value="smaller" {{#is compare value="smaller"}}selected="selected"{{/is}}>smaller</option>
						<option value="smallereq" {{#is compare value="smallereq"}}selected="selected"{{/is}}>smaller or equal</option>
						<option value="startswith" {{#is compare value="startswith"}}selected="selected"{{/is}}>starts with</option>
						<option value="endswith" {{#is compare value="endswith"}}selected="selected"{{/is}}>ends with</option>
						<option value="contains" {{#is compare value="contains"}}selected="selected"{{/is}}>contains</option>
					</select>
					<input type="hidden" id="field_{{_id}}_val" value="{{value}}">
					{{#is field value="user_id"}}
						<?php wp_dropdown_users( array('id' => 'field_{{_id}}', 'name' => '{{:name}}[value]') ); ?>
					{{else}}
						<input type="text" id="field_{{_id}}" name="{{:name}}[value]" value="{{value}}">
					{{/is}}
					{{#script}}jQuery("#field_{{_id}}").val( jQuery("#field_{{_id}}_val").val() );{{/script}}
				</div>
				<div class="filter-actions">

					<button type="button" class="button filter-action-or wp-baldrick" {{#if @root/parent_id}}data-target="{{@root/id}}"{{/if}} data-add-node="{{_node_point}}.or" data-node-default='{"type":"or"}'>or</button>
					
				</div>
			</div>
			{{#each or}}

				<div class="filter-line-{{_id}}">
					<div class="filter-item filter-{{type}}">
						<span class="filter-remove-line" data-remove-element=".filter-line-{{_id}}"><span class="dashicons dashicons-no-alt"></span></span>
						{{:node_point}}
						<input type="hidden" name="{{:name}}[type]" value="{{type}}">
						<span class="filter-clause">{{#is type value="and"}}{{#unless @first}}<?php esc_html_e('and', 'cf-reports'); ?>{{else}}<?php esc_html_e('where', 'cf-reports'); ?>{{/unless}}{{else}}<?php esc_html_e('or', 'cf-reports'); ?>{{/is}}</span>
						<div class="filter-field">
							<select class="filter-field-select" name="{{:name}}[field]" data-live-sync="true" required="required">
								<option></option>
								{{#find @root/{{tab_slug}}/tables @root/{{tab_slug}}/table}}
									{{#each fields}}
										<option value="{{field}}" {{#is ../../field value=field}}selected="selected"{{/is}}>{{field}}</option>
									{{/each}}
								{{/find}}
							</select>
							<select class="filter-field-compare" name="{{:name}}[compare]">
								<option value="is" {{#is compare value="is"}}selected="selected"{{/is}}>is</option>
								<option value="isnot" {{#is compare value="isnot"}}selected="selected"{{/is}}>is not</option>
								<option value="isin" {{#is compare value="isin"}}selected="selected"{{/is}}>in</option>
								<option value="isnotin" {{#is compare value="isnotin"}}selected="selected"{{/is}}>not in</option>
								<option value="greater" {{#is compare value="greater"}}selected="selected"{{/is}}>greater than</option>
								<option value="greatereq" {{#is compare value="greatereq"}}selected="selected"{{/is}}>greater or equal</option>
								<option value="smaller" {{#is compare value="smaller"}}selected="selected"{{/is}}>smaller</option>
								<option value="smallereq" {{#is compare value="smallereq"}}selected="selected"{{/is}}>smaller or equal</option>
								<option value="startswith" {{#is compare value="startswith"}}selected="selected"{{/is}}>starts with</option>
								<option value="endswith" {{#is compare value="endswith"}}selected="selected"{{/is}}>ends with</option>
								<option value="contains" {{#is compare value="contains"}}selected="selected"{{/is}}>contains</option>
							</select>
							<input type="hidden" id="field_{{_id}}_val" value="{{value}}">
							{{#is field value="user_id"}}
								<?php wp_dropdown_users( array('id' => 'field_{{_id}}', 'name' => '{{:name}}[value]') ); ?>
							{{else}}
								<input type="text" id="field_{{_id}}" name="{{:name}}[value]" value="{{value}}">
							{{/is}}
							{{#script}}jQuery("#field_{{_id}}").val( jQuery("#field_{{_id}}_val").val() );{{/script}}
						</div>

					</div>
					{{#each or}}
						{{> filter_query}}
					{{/each}}
				</div>

			{{/each}}
		</div>
		{{/each}}
	</div>
	<label style="margin: 1px 12px 0px 0px;" class="button wp-baldrick" type="button" data-add-node="{{tab_slug}}.filters" data-node-default='{"type":"and"}'>Add Filter</label>
{{/if}}
