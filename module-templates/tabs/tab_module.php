<?php

//instruction tab for adding modules assets etc.
$assets = array(
	'tabs' => array(
		'image-gallery' => array(
			'functions'	=>	array(
				'wp_enqueue_media()'
			),
			'scripts'	=>	array(
				'image-picker.js'
			),
			'styles'	=>	array(
				'image-picker.css'
			)
		),
		'db-query' => array(
			'styles'	=>	array(
				'db-query.css'
			),
			'init_functions' => 'db-init.php',
			'core_methods'	=> 'db-query-core-methods.php'
		),
		'db-connector' => array(
			'styles'	=>	array(
				'db-connector.css'
			),
			'init_functions' => 'db-init.php',
			//'core_methods'	=> 'db-connector-core-methods.php'
			'template_partials' => array(
				'db_field'	=>	'db-field-partial.php'
			),

		),
	),
	'fields' => array(
		'image-picker' => array(
			'functions'	=>	array(
				'wp_enqueue_media()'
			),
			'scripts'	=>	array(
				'image-picker.js'
			),
			'styles'	=>	array(
				'image-picker.css'
			)
		),
		'range-slider' => array(
			'scripts'	=>	array(
				'range-slider.js'
			),
			'styles'	=> array(
				'range-slider.css'
			)
		)
	)
);
