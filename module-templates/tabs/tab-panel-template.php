	<div id="{{core-slug}}-panel-{{tab_slug}}" class="{{core-slug}}-editor-panel" {{#is _current_tab value="#{{core-slug}}-panel-{{tab_slug}}"}}{{else}} style="display:none;" {{/is}}>		
		<h4>
			<?php esc_html_e('{{tab_description}}', '{{core-slug}}') ; ?>
			<small class="description">
				<?php esc_html_e('{{tab_name}}', '{{core-slug}}') ; ?>
			</small>
		</h4>
		<?php
			/**
			 * Include the {{tab_slug}}-panel
			 */
			include {{CORE_PREFIX}}_PATH . 'includes/templates/{{admin_prefix}}{{tab_slug}}-panel.php';
		?>
	</div>
