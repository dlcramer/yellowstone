<?php

	// Panel template for My Tasks

?>

<input id="active_edit_processors" data-live-sync="true" type="hidden" value="{{active_edit_processors}}" name="active_edit_processors">

<div class="{{core-slug}}-module-side">

	
	<ul class="{{core-slug}}-module-tabs {{core-slug}}-group-wrapper" style="box-shadow: 0px 1px 0px rgb(207, 207, 207) inset;">
	{{#each processors}}
		<li class="{{_id}} {{core-slug}}-module-tab {{#is ../active_edit_processors value=_id}}active{{/is}}">
			{{:node_point}}
			{{#unless config/processors_name}}
				<a><input class="autofocus-input" data-format="key" style="width: 100%; padding: 3px 6px; margin: -3px; background: none repeat scroll 0% 0% rgb(255, 255, 255); border: 0px none; border-radius: 2px;" type="text" data-id="{{_id}}" name="{{:name}}[config][processors_name]" data-live-sync="true" data-sync=".processors_title_{{_id}}" value="{{config/processors_name}}" id="caldera_todo-processors_name-{{_id}}"></a>
			{{else}}
				<a href="#" class="sortable-item {{core-slug}}-edit-processors" data-id="{{_id}}"> <span class="processors_title_{{_id}}">{{config/processors_name}}</span></a>
			{{/unless}}

			{{#is ../active_edit_processors not=_id}}<input type="hidden" name="{{:name}}[config]" value="{{json config}}">{{/is}}
			{{#if new}}<input class="wp-baldrick" data-request="{{core_prefix}}_record_change" data-autoload="true" data-live-sync="true" type="hidden" value="{{_id}}" name="active_edit_processors">{{/if}}

		</li>
	{{/each}}
	{{#unless processors}}
		<li class="{{core-slug}}-module-tab">
			<p class="description" style="margin: 0px; padding: 9px 22px;">
				<?php esc_html_e( 'No {{tab_group_kind_plural}}', '{{core-slug}}' ); ?>
			</p>
		</li>
	{{/unless}}
		<li class="{{core-slug}}-module-tab" style="text-align: center; padding: 12px 22px; background-color: rgb(225, 225, 225); box-shadow: -1px 0 0 #cfcfcf inset, 0 1px 0 #cfcfcf inset, 0 -1px 0 #cfcfcf inset;">
			<button style="width: 100%;" class="wp-baldrick button" data-node-default='{ "new" : "true" }' data-add-node="processors" type="button">
				<?php esc_html_e( 'Add {{tab_group_kind}}', '{{core-slug}}' ); ?>
			</button>
		</li>	 
	</ul>

</div>


	{{#if config/processors_name}}
	
	<div class="{{core-slug}}-field-config-wrapper {{_id}}" style="width:580px;">

		<button style="float:right" type="button" class="button" data-confirm="<?php echo esc_attr( __( '{{tab_delete_confirm}}', '{{core-slug}}' ) ); ?>" data-remove-element=".{{_id}}" style="float: right; padding: 3px 6px;">
			<?php esc_html_e( 'Delete {{tab_group_kind}}', '{{core-slug}}' ); ?>
		</button>

		<div style="border-bottom: 1px solid rgb(209, 209, 209); margin: 0px 0px 12px; padding: 5px 0px 12px;">
			<input style="border: 0px none; background: none repeat scroll 0% 0% transparent; box-shadow: none; font-weight: bold; padding: 0px; margin: 0px; width: 450px;" type="text" name="{{:name}}[config][processors_name]" data-live-sync="true" data-sync=".processors_title_{{_id}}" data-format="key" value="{{config/processors_name}}" id="caldera_todo-processors_name-{{_id}}">
		</div>

		<!-- Add custom code here fields names are {{:name}}[config][field_name] -->
		{{module_tab_config_panel_fields}}
		
	</div>

	{{/if}}

{{/find}}



{{#script}}
jQuery('.{{core-slug}}-edit-processors').on('click', function(){
	var clicked = jQuery(this),
		if( clicked.parent().data('moved') ){
			clicked.parent().data('moved', false);
			return;
		}
		if( active.val() == clicked.data('id') ){
			active.val('').trigger('change');
		}else{
			active.val( clicked.data('id') ).trigger( 'change' );
		}
});
jQuery('.autofocus-input').focus().on('blur', function(){ 
	if( jQuery(this).val() == '' ){
		jQuery( '.' + jQuery(this).data('id') ).remove();
		{{core_prefix}}_record_change();
	}
});
{{/script}}
