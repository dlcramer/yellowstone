<label style="margin-top: 8px;"><input type="checkbox" name="{{tab_slug}}[{{_id}}][repeatable]" value="true" {{#if repeatable}}checked="checked"{{/if}}>
	<?php esc_html_e( 'Repeatable {{tab_group_type}}', '{{core-slug}}' ); ?>
</label>
