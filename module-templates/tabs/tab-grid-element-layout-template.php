					<?php
						// Element template for {{tab_item_title}}
					?>
					{{#if _{{tab_item_slug}}}}

						<div class="node-wrapper {{core-slug}}-card-item" style="min-height: 52px; display: block; width: 100%; padding-left: 20px; box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.04), 20px 0px 0px rgb(248, 248, 248) inset;" id="element_{{@key}}">
							<span style="color:#a1a1a1;" class="dashicons {{core-slug}}-card-icon"></span>
							<div class="{{core-slug}}-card-content" style="min-height: 52px;">
								<input id="{{@key}}" type="hidden" value="{{json this}}">
								<h4>{{name}}</h4>
							</div>
							<span data-data="{{@key}}" data-fragment="{{../../id}}" data-module="{{tab_item_slug}}" class="element-item-edit" style="padding: 0px; margin: 3px 0px; position: absolute; left: 1px; top: -2px;">
								<span class="dashicons dashicons-admin-generic" style="padding: 0px; margin: 0px; line-height: 23px; font-size:13px;"></span>
							</span>

							<span style="padding: 0px; margin: 3px 0px; position: absolute; left: 0px; bottom: -2px;" data-confirm="{{tab_delete_confirm}}" data-remove-parent=".node-wrapper">
								<span class="dashicons dashicons-no-alt" style="padding: 0px; margin: 0px; line-height: 23px; font-size:13px;"></span>
							</span>
						</div>

					{{/if}}

