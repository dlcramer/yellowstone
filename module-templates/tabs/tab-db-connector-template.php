<?php
/**
 * @author    David Cramer <david@digilab.co.za>
 * @license   GPL-2.0+
 * @link
 * @copyright 2015 David Cramer <david@digilab.co.za>
 */
?>
<input type="hidden" name="{{tab_slug}}[tables]" value="{{json {{tab_slug}}/tables}}">
<div class="primary-table-selector">
	<select name="{{tab_slug}}[table]" data-live-sync="true">
		<option></option>
		{{#each {{tab_slug}}/tables}}
			<option value="{{name}}" {{#is @root/{{tab_slug}}/table value=name}}selected="selected"{{/is}}>{{name}}</option>
		{{/each}}
	</select>
</div>
{{#if {{tab_slug}}/table}}
	<div class="fields-wrapper">
		{{#find @root/{{tab_slug}}/tables @root/{{tab_slug}}/table}}
			{{> {{tab_slug}}-db_field}}
		{{/find}}		
	</div>
	
{{/if}}