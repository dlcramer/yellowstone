	<button type="button" class="button button-small wp-baldrick" data-request="{{core_prefix}}_get_default_setting" data-add-node="{{tab_slug}}">
		<?php esc_html_e( '{{module_tab_field_add_text}}', '{{core-slug}}' ); ?>
	</button>
	<br>
	<br>
	{{#unless {{tab_slug}}}}
		<p class="description">
			<?php esc_html_e( "{{module_tab_fieldesc_html_empty_text}}", '{{core-slug}}' ); ?>
		</p>
	{{/unless}}
	{{#each {{tab_slug}}}}
	<div class="node-wrapper {{core-slug}}-card-item" style="display: block; width: 550px;">
		<span style="color:#a1a1a1;" class="dashicons {{module_tab_field_dashicon}} {{core-slug}}-card-icon"></span>
		<div class="{{core-slug}}-card-content">
			<input id="{{core-slug}}-{{tab_slug}}-name-{{_id}}" type="hidden" name="{{tab_slug}}[{{_id}}][_id]" value="{{_id}}">
			{{module_tab_config_panel_fields}}
		</div>
		<button type="button" class="button button-small" style="padding: 0px; margin: 3px 0px; position: absolute; left: 14px; top: 6px;" data-remove-parent=".node-wrapper"><span class="dashicons dashicons-no-alt" style="padding: 0px; margin: 0px; line-height: 23px;"></span></button>
	</div>
	{{/each}}
