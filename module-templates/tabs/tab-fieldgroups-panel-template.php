
<span class="{{core-slug}}-add-group-btn dashicons dashicons-plus-alt wp-baldrick" data-request="{{core_prefix}}_get_default_setting" type="button" data-add-node="{{tab_slug}}"></span>

<div class="{{core-slug}}-group-wrapper">
	{{#each {{tab_slug}}}}
	{{#if _id}}
	<div class="node-wrapper {{core-slug}}-field-group" style="display:block;{{#unless field}} opacity:.8;{{/unless}}">	

		<input type="hidden" name="{{tab_slug}}[{{_id}}][_id]" value="{{_id}}">
		
		<span style="float:left; margin-right:6px; cursor:move;" class="{{core-slug}}-group-handle dashicons dashicons-sort sortable-item"></span>
		
		<span style="float: right; cursor: pointer; margin-right: -4px;" data-toggle="#{{core-slug}}-group-config-{{_id}}-fields,#{{core-slug}}-group-config-{{_id}}-collapse" class="dashicons dashicons-arrow-{{#if collapse}}up{{else}}down{{/if}}"></span>
		{{#unless collapse}}
		<span style="float: right; margin: 1px 4px 1px 0; padding-right: 4px; cursor:pointer; border-right: 1px solid rgb(207, 207, 207);" class="dashicons dashicons-plus wp-baldrick" title="<?php esc_html_e( 'Add Field to {{tab_group_type}}', '{{core-slug}}' ); ?>" data-request="{{core_prefix}}_get_default_setting" type="button" data-group="{{_id}}" data-slug="{{tab_slug}}" data-script="add-field-node"></span>
		{{/unless}}
		<span style="float: right; cursor: pointer; border-right: 1px solid rgb(207, 207, 207); margin-right: 4px; padding-right: 4px;" data-toggle="#{{core-slug}}-group-config-{{_id}},#{{core-slug}}-group-config-{{_id}}-open" title="<?php esc_html_e( 'Edit {{tab_group_type}}', '{{core-slug}}' ); ?>" class="dashicons dashicons-edit"></span>
		<input style="display:none;" type="checkbox" name="{{tab_slug}}[{{_id}}][collapse]" id="{{core-slug}}-group-config-{{_id}}-collapse" value="true" {{#if collapse}}checked="checked"{{/if}}>

		<span id="{{core-slug}}-group-name-{{_id}}-text" style="display: inline-block; width: {{#unless collapse}}150px;{{else}}178px;{{/unless}}">{{#if name}}{{name}}{{else}}<?php esc_html_e( 'Untitled {{tab_group_type}}', '{{core-slug}}' ); ?>{{/if}}</span>
		
		<div class="{{core-slug}}-config-group {{core-slug}}-field-group-config" id="{{core-slug}}-group-config-{{_id}}" style="height:180px;{{#unless is_open}}{{#unless name}}display:block;{{else}}display:none;{{/unless}}{{/unless}}clear:both;">
			<input style="display:none;" type="checkbox" name="{{tab_slug}}[{{_id}}][is_open]" id="{{core-slug}}-group-config-{{_id}}-open" value="true" {{#if is_open}}checked="checked"{{/if}}>
			<input type="text" name="{{tab_slug}}[{{_id}}][name]" {{#unless name}}data-auto-focus="true"{{/unless}} value="{{#if name}}{{name}}{{else}}<?php esc_html_e( 'Untitled {{tab_group_type}}', '{{core-slug}}' ); ?>{{/if}}" placeholder="<?php esc_html_e( '{{tab_group_type}} Name', '{{core-slug}}' ); ?>" data-sync="#{{core-slug}}-group-name-{{_id}}-text,#{{core-slug}}-group-name-{{_id}}-slug" style="width: 100%;" id="{{core-slug}}-group-name-{{_id}}-input" required>
			<input type="text" name="{{tab_slug}}[{{_id}}][slug]" value="{{#if slug}}{{slug}}{{/if}}" data-master="#{{core-slug}}-group-name-{{_id}}-input" id="{{core-slug}}-group-name-{{_id}}-slug"  data-format="slug" placeholder="<?php esc_html_e( '{{tab_group_type}} Slug', '{{core-slug}}' ); ?>" style="width: 100%;">
			<textarea name="{{tab_slug}}[{{_id}}][description]" placeholder="<?php esc_html_e( '{{tab_group_type}} Description', '{{core-slug}}' ); ?>" rows="2" style="width: 100%;">{{description}}</textarea>
			
			{{module_tab_allow_repeat}}
		
			<span data-toggle="#{{core-slug}}-group-config-{{_id}},#{{core-slug}}-group-config-{{_id}}-open" style="cursor:pointer;float: left; margin: 6px 0px 0px; padding: 0 6px;"><?php esc_html_e( 'Close Edit', '{{core-slug}}' ); ?></span>
			<span data-remove-parent=".{{core-slug}}-field-group" data-confirm="<?php echo esc_attr( __( '{{tab_delete_confirm}}', '{{core-slug}}' ) ); ?>" style="cursor:pointer;float: right; margin: 6px 0px 0px; padding: 0 6px;" class="{{core-slug}}-general-delete-link">
				<?php esc_html_e( '{{tab_delete_text}}', '{{core-slug}}' ); ?>
			</span>

		</div>

		<div class="clear {{core-slug}}-fields-list" id="{{core-slug}}-group-config-{{_id}}-fields" style="margin-top:12px;{{#if collapse}}display:none;{{/if}}">
		{{#each field}}
		{{#if _id}}			
			<div class="{{core-slug}}-field-item" {{#is ../../../../open_field value="@key"}}style="box-shadow:0 0 6px rgba(0, 0, 0, 0.18);border-color:#bfbfbf;"{{/is}} >
				<input type="hidden" name="{{tab_slug}}[{{../../_id}}][field][{{_id}}][_id]" value="{{_id}}">
				<span style="float: right; margin: 1px 0 0 0;cursor:pointer;" class="dashicons dashicons-no-alt" data-remove-parent=".{{core-slug}}-field-item"></span>
				<div style="cursor:pointer;" data-toggle=".{{core-slug}}-field-open-{{_id}}">
					<span style="margin: 1px 6px 0 0;cursor:move;" class="dashicons dashicons-sort sortable-item"></span>
					<span id="{{core-slug}}-field-item-{{_id}}" class="{{core-slug}}-field-name-{{_id}}">
						{{name}}
					</span>
					<small id="{{core-slug}}-field-item-{{_id}}" style="color: rgb(159, 159, 159); font-weight: normal; font-style: italic;" class="description {{core-slug}}-field-slug-{{_id}}">
						{{slug}}
					</small>
				</div>
			</div>
		{{/if}}
		{{/each}}
		</div>


		
	</div>
	{{/if}}
	{{/each}}

</div>


<div class="{{core-slug}}-field-config-wrapper">
	{{#each {{tab_slug}}}}
	{{#if _id}}
	{{#if field}}	
	<div class="node-wrapper" style="display:block;">
		{{#each field}}
		{{#if _id}}
			<div class="{{core-slug}}-field-item" {{#is ../../../../../open_field value="@key"}}{{#unless ../../../collapse}}style="display:block;"{{else}}style="display:none;"{{/unless}}{{else}}style="display:none;"{{/is}}>
				<input style="display:none;" type="radio" name="open_field" class="{{core-slug}}-field-open-{{_id}}" value="{{_id}}" {{#is ../../../../../open_field value="@key"}}checked="checked"{{/is}}>

				<h4 style="border-bottom: 1px solid rgb(239, 239, 239); margin: 0px 0px 6px; padding-bottom: 6px;">
					<span class="{{core-slug}}-field-name-{{_id}}">
						{{name}}
					</span>
					<small style="color: rgb(159, 159, 159); font-weight: normal; font-style: italic;" class="{{core-slug}}-field-slug-{{_id}}">
						{{slug}}
					</small>
				</h4>

				<div class="{{core-slug}}-config-group">
					<label>
						<?php esc_html_e( 'Name / Label', '{{core-slug}}' ); ?>
					</label>
					<input class="regular-text" type="text" name="{{tab_slug}}[{{../../_id}}][field][{{_id}}][name]" value="{{#if name}}{{name}}{{else}}<?php esc_html_e( 'Untitled Field', '{{core-slug}}' ); ?>{{/if}}" placeholder="<?php esc_html_e( 'Field Name', '{{core-slug}}' ); ?>" data-sync=".{{core-slug}}-field-name-{{_id}}" id="{{core-slug}}-field-name-input-{{_id}}" required>
				</div>

				<div class="{{core-slug}}-config-group">
					<label>
						<?php esc_html_e( 'Slug', '{{core-slug}}' ); ?>
					</label>
					<input class="regular-text" type="text" name="{{tab_slug}}[{{../../_id}}][field][{{_id}}][slug]" value="{{#if slug}}{{slug}}{{else}}<?php esc_html_e( 'untitled_field', '{{core-slug}}' ); ?>{{/if}}" data-format="slug" placeholder="<?php esc_html_e( 'Field Slug', '{{core-slug}}' ); ?>" data-master="#{{core-slug}}-field-name-input-{{_id}}" data-sync=".{{core-slug}}-field-slug-{{_id}}" required>
				</div>

				<div class="{{core-slug}}-config-group">
					<label>
						<?php esc_html_e( 'Description', '{{core-slug}}' ); ?>
					</label>
					<input class="regular-text" type="text" name="{{tab_slug}}[{{../../_id}}][field][{{_id}}][description]" value="{{#if description}}{{description}}{{/if}}" placeholder="<?php esc_html_e( 'Field Description', '{{core-slug}}' ); ?>">
				</div>

				<div class="{{core-slug}}-config-group">
					<label style="margin-top: 0px;"></label>
					<label style="margin-top: 0px;"><input type="checkbox" name="{{tab_slug}}[{{../../_id}}][field][{{_id}}][required]" value="1" {{#if required}}checked="checked"{{/if}}>
						<?php esc_html_e( 'Required', '{{core-slug}}' ); ?>
					</label>
				</div>

				<div class="{{core-slug}}-config-group">
					<label>
						<?php esc_html_e( 'Field Type', '{{core-slug}}' ); ?>
					</label>
					<select name="{{tab_slug}}[{{../../_id}}][field][{{_id}}][type]" data-live-sync="true" style="width: 351px;">
						<option value="text" {{#is type value="text"}}selected="selected"{{/is}}>
							<?php esc_html_e( 'Text', '{{core-slug}}' ); ?>
						</option>
						<option value="textarea" {{#is type value="textarea"}}selected="selected"{{/is}}>
							<?php esc_html_e( 'Paragraph', '{{core-slug}}' ); ?>
						</option>
						<option value="color" {{#is type value="color"}}selected="selected"{{/is}}>
							<?php esc_html_e( 'Color Picker', '{{core-slug}}' ); ?>
						</option>
					</select>
				</div>

				
				{{#is type value="text"}}
				<div class="{{core-slug}}-config-group">
					<label>
						<?php esc_html_e( 'Default Value', '{{core-slug}}' ); ?>
					</label>
					<input class="regular-text" type="text" name="{{tab_slug}}[{{../../../_id}}][field][{{_id}}][default]" value="{{../default}}" placeholder="<?php esc_html_e( 'Default Value', '{{core-slug}}' ); ?>">
				</div>
				{{/is}}

				{{#is type value="textarea"}}
				<div class="{{core-slug}}-config-group">
					<label>
						<?php esc_html_e( 'Default Text', '{{core-slug}}' ); ?>
					</label>
					<textarea class="regular-text" name="{{tab_slug}}[{{../../../_id}}][field][{{_id}}][default]" placeholder="<?php esc_html_e( 'Default Value', '{{core-slug}}' ); ?>" style="width: 351px;">{{../default}}</textarea>
				</div>
				{{/is}}
				{{#is type value="color"}}
				<div class="{{core-slug}}-config-group">
					<label>
						<?php esc_html_e( 'Default Color', '{{core-slug}}' ); ?>
					</label>
					<input class="color-field" type="text" name="{{tab_slug}}[{{../../../_id}}][field][{{_id}}][default]" value="{{../default}}">
				</div>
				{{/is}}

			</div>
		{{/if}}
		{{/each}}
	</div>
	{{/if}}
	{{/if}}
	{{/each}}

</div>

<div class="clear"></div>
