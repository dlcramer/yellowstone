
		<div class="{{core-slug}}-config-group">
			<label for="{{module_tab_field_id}}">
				<?php esc_html_e( '{{module_tab_field_name}}', '{{core-slug}}' ); ?>
			</label>
			<input id="{{module_tab_field_id}}" type="text" class="color-field" name="{{module_tab_field_input_name}}" value="{{{{module_tab_field_value}}}}" {{module_tab_field_required}}>
			{{module_tab_field_description}}
		</div>
