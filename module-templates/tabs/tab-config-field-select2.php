		<div class="{{core-slug}}-config-group">
			<label for="{{module_tab_field_id}}">
				<?php esc_html_e( '{{module_tab_field_name}}', '{{core-slug}}' ); ?>
			</label>

			<select style="width:395px;" placeholder="<?php esc_html_e( '{{module_tab_field_name}}', '{{core-slug}}' ); ?>" name="{{module_tab_field_input_name}}" data-live-sync="true" id="{{module_tab_field_id}}" {{module_tab_field_required}}>
				<option value="value" {{#if {{module_tab_field_value}} value="value"}}selected="selected"{{/if}}> label </option>
			</select>

			{{module_tab_field_description}}
		</div>

		{{#script}}
			jQuery( function($){
				$("#{{module_tab_field_id}}").select2();
			});
		{{/script}}
