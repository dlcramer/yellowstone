<?php
/**
 * @author    David Cramer <david@digilab.co.za>
 * @license   GPL-2.0+
 * @link
 * @copyright 2014 David Cramer <david@digilab.co.za>
 */


	
if( defined( 'CFCORE_VER' ) ){ ?>
		<p class="description"><?php esc_html_e('Selecting a Caldera Form below will override the form\'s functality and turn it into a search form using the {{Core_Item}} query elements.', '{{core-slug}}'); ?></p><br>

		<div class="{{core-slug}}-config-group">
			<label for="{{core-slug}}-search_form"><?php esc_html_e('Search Form', '{{core-slug}}'); ?></label>
			<select id="{{core-slug}}-search_form" data-load-element="#{{core-slug}}-save-indicator" name="{{tab_slug}}[search_form]" class="wp-baldrick" data-action="{{core_slug}}_load_search_form_fields" data-target="#{{core_slug}}_{{tab_slug}}_form_fields" data-script="set-search-fields" data-callback="{{core_slug}}_get_default_setting" data-type="json" data-event="change">				
				<?php 

				$forms = get_option( '_caldera_forms' );
				if(empty($forms)){
					echo '<option value="">'.__("You don't have any Caldera Forms", '{{core-slug}}').'</option>';
				}else{
					echo '<option value="">' . __("Search Disabled", '{{core-slug}}') . '</option>';
					foreach( $forms as $form_id=>$form ){
						echo '<option value="'.$form_id.'" {{#is {{tab_slug}}/search_form value="'.$form_id.'"}}selected="selected"{{/is}}>'.$form['name'].'</option>';
					}
				}

				?>
			</select>
			{{#if {{tab_slug}}/search_form}}
			<button type="button" class="wp-baldrick button" data-for="#{{core-slug}}-search_form"><?php esc_html_e('Reload Fields', '{{core-slug}}'); ?></button>
			<p class="description" style="margin-left: 185px;"><?php esc_html_e( "If you've made changes to the form and fields are not showing, click reload fields to fetch the changes", '{{core-slug}}' ); ?></p>
			{{/if}}
			<input type="hidden" name="{{tab_slug}}[form_fields]" id="{{core_slug}}_{{tab_slug}}_form_fields" value="{{json {{tab_slug}}/form_fields}}" data-live-sync="true">
	
		</div>
	<div {{#if {{tab_slug}}/search_form}}{{#is {{tab_slug}}/search_form value=""}}style="display:none;"{{/is}}{{else}}style="display:none;"{{/if}}>

		{{#is {{tab_slug}}/search_form value=""}}
		{{else}}
		<div class="{{core-slug}}-config-group">
			<label for="{{core-slug}}-template"><?php esc_html_e( 'Template', '{{core-slug}}' ); ?></label>
			<select data-live-sync="true" name="{{tab_slug}}[params][template]" id="{{core-slug}}-template">
				<?php foreach ( $templates as $tmpl ) { ?>
				<option {{#is params/template value="<?php echo $tmpl[ 'name' ]; ?>"}}selected="selected"{{/is}} value="<?php echo $tmpl[ 'name' ]; ?>">
					<?php echo $tmpl[ 'name' ]; ?>
				</option>
				<?php } ?>
			</select>
			
		</div>
		{{/is}}

		<div class="{{core-slug}}-config-group">
			<label><?php esc_html_e('Live Search', '{{core-slug}}'); ?></label>
			<label style="width: auto;" for="{{core-slug}}-trigger_change"><input type="checkbox" name="{{tab_slug}}[trigger_change]" value="1" id="{{core-slug}}-trigger_change" {{#if trigger_change}}checked="checked"{{/if}}> <?php esc_html_e( 'Search while typing or change.', '{{core-slug}}' ); ?>
		</div>

		<div class="{{core-slug}}-config-group">
			<label></label>
			<label style="width: auto;" for="{{core-slug}}-init_search"><input type="checkbox" name="{{tab_slug}}[init_search]" value="1" id="{{core-slug}}-init_search" {{#if init_search}}checked="checked"{{/if}}> <?php esc_html_e( 'Run search on page load.', '{{core-slug}}' ); ?>
		</div>


		<div class="{{core-slug}}-config-group">
			<label for="{{core-slug}}-result_class"><?php esc_html_e( 'Results Class', '{{core-slug}}' ); ?></label>
			<input type="text" name="{{tab_slug}}[result_class]" value="{{result_class}}" id="{{core-slug}}-result_class">
			<p class="description" style="margin-left: 185px;"><?php esc_html_e( 'Custom CSS class names for results wrapper', '{{core-slug}}' ); ?></p>
		</div>

		<div class="{{core-slug}}-config-group">
			<label for="{{core-slug}}-output_selector"><?php esc_html_e( 'Output Target', '{{core-slug}}' ); ?></label>
			<input type="text" name="{{tab_slug}}[output_selector]" value="{{output_selector}}" id="{{core-slug}}-output_selector">
			<p class="description" style="margin-left: 185px;"><?php esc_html_e( 'Custom jQuery selector or Javascript Function name for search results placement.', '{{core-slug}}' ); ?></p>
		</div>
		<hr>
		<h4><?php esc_html_e('Form Fields', '{{core-slug}}'); ?> <small class="description"><?php esc_html_e('Search Form', '{{core-slug}}'); ?></small></h4>
		<div id="form-field-reference-description">
			<p class="description"><?php esc_html_e( 'Use these tags in the query builder to use the result of the from field to be the value for that part of the query.', '{{core-slug}}' ); ?></p>
			<p class="description"><?php esc_html_e( 'The name of the tag corresponds to the name of the field in your Caldera Form. To add a new tag, simply add a new field to your Caldera Form.', '{{core-slug}}' ); ?></p>

		</div>
		
		<br>
		{{#each form/form_fields}}

		<div class="{{core-slug}}-config-group {{core-slug}}-magic-tags-definitions" data-category="<?php echo esc_attr( __('Search Form', '{{core-slug}}') ); ?>" data-tag="search:{{slug}}">
			<label>&lcub;search:{{slug}}&rcub;</label>

			{{#if option}}<label style="display: inline-block; width: auto; margin: 6px;"><input type="checkbox" name="{{tab_slug}}[auto_populateesc_html_enable][{{slug}}]" value="1" data-live-sync="true" {{#find @root/{{tab_slug}}/auto_populateesc_html_enable slug}}checked="checked"{{/find}}> {{#find @root/{{tab_slug}}/auto_populateesc_html_enable slug}}<?php esc_html_e('Override field options with results from', '{{core-slug}}'); ?>{{else}}<?php esc_html_e('Override field options', '{{core-slug}}'); ?>{{/find}}</label>
			
			<select{{#find @root/{{tab_slug}}/auto_populateesc_html_enable slug}}{{else}} style="display:none;"{{/find}} name="{{tab_slug}}[auto_populate_fields][{{slug}}]" data-live-sync="true">
				<?php
					${{core_slug}}_items = \calderawp\{{core_prefix}}\options::get_registry();
					${{core_slug}}_binds = array();
					if( !empty( ${{core_slug}}_items ) ){
					?>
					<optgroup label="<?php esc_html_e('{{Core_Item}}', '{{core-slug}}'); ?>">
					<?php
						foreach( ${{core_slug}}_items as ${{core_slug}}_populate){
							if( !empty( ${{core_slug}}_populate['search_form'] ) || ${{core_slug}}_populate['id'] == ${{core_slug}}s( $_GET['edit'] ) ){
								continue;
							}
							?>
							<option value="<?php echo ${{core_slug}}_populate['slug']; ?>" {{#find @root/{{tab_slug}}/auto_populate_fields slug}}{{#is this value="<?php echo ${{core_slug}}_populate['slug']; ?>"}}selected="selected"{{/is}}{{/find}}><?php echo ${{core_slug}}_populate['name']; ?></option>
							<?php

							// add to field binds 
							$query = \calderawp\{{core_prefix}}\options::get_single( ${{core_slug}}_populate['id'] );
							${{core_slug}}_binds[${{core_slug}}_populate['slug']] = $query['{{query_filter_binding}}']['fields'];

						}
					?></optgroup>
					<?php
					}
				?>
			</select> <p class="description" {{#find @root/{{tab_slug}}/auto_populateesc_html_enable slug}}style="display:inline;"{{else}} style="display:none;"{{/find}}>

			{{#find @root/{{tab_slug}}/auto_populate_fields slug}}

					<?php esc_html_e( 'with the value from: ', '{{core-slug}}' ); ?>
					<input required type="text" name="{{tab_slug}}[auto_populate_values][{{../../slug}}]" value="{{#find @root/{{tab_slug}}/auto_populate_values ../../slug}}{{this}}{{else}}ID{{/find}}">				
					<?php esc_html_e( 'and the label from: ', '{{core-slug}}' ); ?>
					<input required type="text" name="{{tab_slug}}[auto_populate_labels][{{../../slug}}]" value="{{#find @root/{{tab_slug}}/auto_populate_labels ../../slug}}{{this}}{{else}}post_title{{/find}}">

			{{/find}}
			</p>

			{{else}}
			<?php esc_html_e('Input Field', '{{core-slug}}'); ?>
			{{/if}}
		</div>
		{{/each}}
		<br>
		<p class="description"><?php esc_html_e( 'For fields that have preset options, such as dropdowns and checkbox fields, the options can be set in one of three ways. The options may be manually set in the Caldera Forms editor, or they can be automatically populated with all items in a post type, or taxonomy using the Caldera Forms Editor. You may also select another {{Core_Item}} here to use the results of that query as the options.', '{{core-slug}}' ); ?></p>

	</div>
<?php }else{ ?>

<?php esc_html_e( 'Search features requires the Free Caldera Forms.', '{{core-slug}}' ); ?>
<?php echo '&nbsp;<a href="https://wordpress.org/plugins/caldera-forms/" target="_blank">' . __( 'Vew Details.', '{{core-slug}}' ) .'</a>'; ?>

<?php } ?>
