<?php
	// Element trigger for {{tab_item_title}}
?>
<span class="insert-item wp-modals" 

	data-element-type="{{tab_item_title}}" 
	data-element-slug="{{tab_item_slug}}" 
	data-save-text="<?php esc_html_e( '{{tab_update_text}} {{tab_item_title}}', '{{core-slug}}' ); ?>"
	data-insert-text="<?php esc_html_e( '{{tab_insert_text}} {{tab_item_title}}', '{{core-slug}}' ); ?>"
	data-modal-width="560" 
	data-modal-height="420" 
	data-request="grid_get_item_data" 
	data-template="#{{tab_slug}}_{{tab_item_slug}}_modal_form" 
	data-modal="{{tab_slug}}" 
	data-modal-title="<?php echo esc_attr( __( '{{tab_item_title}}', '{{core-slug}}' ) ); ?>" 
	data-panel="{{tab_slug}}"

></span>
