		<a class="{{#is _current_tab value="#{{core-slug}}-panel-{{tab_slug}}"}}nav-tab-active {{/is}}{{core-slug}}-nav-tab nav-tab" href="#{{core-slug}}-panel-{{tab_slug}}">
			<?php esc_html_e('{{tab_name}}', '{{core-slug}}') ; ?>
		</a>
