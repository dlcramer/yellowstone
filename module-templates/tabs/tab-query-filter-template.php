<?php
/**
 * @author    David Cramer <david@digilab.co.za>
 * @license   GPL-2.0+
 * @link
 * @copyright 2014 David Cramer <david@digilab.co.za>
 */
	
$post_types = get_post_types( array( 'public' => true ), 'objects' );

?>
<div class="{{core-slug}}-config-group">

    <h4 style="margin: 0px 0px 6px; font-size: 14px; font-weight: normal;">
    	<span style="text-transform:uppercase; color:#888888;"><?php esc_html_e('From', '{{core-slug}}'); ?></span>
    </h4>
    <select placeholder="<?php esc_html_e( 'Select Data Sources', '{{core-slug}}' ); ?>" name="{{tab_slug}}_post_type[]" multiple="multiple" data-live-sync="true" id="{{core_prefix}}-{{tab_slug}}-post_type" style="min-width:300px;max-width: 772px;">
    <?php
        foreach($post_types as $post_type){
            echo '<option value="' . $post_type->name . '" {{#find {{tab_slug}}_post_type "' . $post_type->name . '"}}selected="selected"{{/find}}>' . $post_type->label . '</option>';
        }
    ?>
    </select>

</div>
{{#script}}
    jQuery( function($){
        $("#{{core_prefix}}-{{tab_slug}}-post_type").select2();
    });
{{/script}}
<?php



// Get taxonomies and meta data for each post type for filter builder
global $wpdb;
foreach( $post_types as $post_type=>$post_object ){
	
	// setup array
	$post_meta_stuff = array();
	// get taxos / disable for now - prototype and I'm lazy
	$taxonomies = get_object_taxonomies( $post_type, 'objects' );

	if( !empty( $taxonomies ) ){
		foreach ( $taxonomies as $taxonom_name => $taxonomy) {
			//$post_meta_stuff['taxonomies'][ $taxonom_name.'.ID' ] = 'taxonomy_id.' . $taxonomy->name;
			//$post_meta_stuff['taxonomies'][ $taxonom_name.'.name' ] = 'taxonomy_name.' . $taxonomy->name;
			$post_meta_stuff['taxonomies'][ 'taxonomy.' . $taxonom_name ] = $taxonomy->name;
		}
	}
	$custom_field_keys = $wpdb->get_results( $wpdb->prepare( "SELECT 
	 	
	 	DISTINCT `" . $wpdb->postmeta . "`.`meta_key` AS `meta_key`

		FROM `" . $wpdb->postmeta . "` 
		LEFT JOIN `" . $wpdb->posts . "` ON (`" . $wpdb->postmeta . "`.`post_id` = `" . $wpdb->posts . "`.`ID`)
		
		WHERE
		`" . $wpdb->posts . "`.`post_type` = %s
		AND
		SUBSTR(`" . $wpdb->postmeta . "`.`meta_key`,1,1) != '_'", $post_type ) );

	if( !empty( $custom_field_keys ) ){

		foreach( $custom_field_keys as $custom_fields ){

			$post_meta_stuff['custom_fields'][ 'custom_field.' . $custom_fields->meta_key ] = $custom_fields->meta_key;

		}

	}
	?>
	
	
	<?php
}
?>

{{#if {{tab_slug}}_post_type}}
	{{#each {{tab_slug}}_post_type}}
		<input type="hidden" name="{{tab_slug}}[post_type][{{this}}]" value="{{this}}">
	{{/each}}
	
	{{#if {{tab_slug}}/content_meta}}
		<h4 style="margin: 0px 0px 6px; font-size: 14px; font-weight: normal;"><span style="text-transform:uppercase; color:#888888;">{{#if {{tab_slug}}/filter}}<?php esc_html_e('where', '{{core-slug}}'); ?></span>{{/if}}</h4>
		<div class="{{core-slug}}-filter-groups" id="{{core_prefix}}_builder_{{tab_slug}}">
		{{#each {{tab_slug}}/filter}}

			
			<div class="{{core-slug}}-filter-group">
				<div class="{{core-slug}}-filter-group-or"><?php esc_html_e('or', '{{core-slug}}'); ?></div>
				<span style="color:#cfcfcf;" class="dashicons dashicons-arrow-right-alt2 {{core-slug}}-filter-group-icon"></span>
				{{:node_point}}
				<div class="{{core-slug}}-filter-group-content">

					{{#each line}}
						
						<div class="{{core-slug}}-filter-line" style="clear:right;">

							<span class="{{core-slug}}-filter-group-icon" style="color: rgb(207, 207, 207); width: 40px; text-align: center; text-transform: uppercase; font-size: 14px; line-height: 31px;"><?php esc_html_e('and', '{{core-slug}}'); ?></span>

							{{:node_point}}

							<select name="{{:name}}[field]" style="margin-right: 0px; width: 216px;" required="required">
							<option></option>
								{{#if @root/{{tab_slug}}_post_type}}
                                <optgroup label="<?php esc_html_e('Taxonomies', '{{core-slug}}'); ?>">
                                    {{#each @root/{{tab_slug}}_post_type}}
                                        {{#find @root/{{tab_slug}}/content_meta this}}


                                            {{#each taxonomies}}
                                                <option value="{{@key}}" {{#is ../../../field value=@key}}selected="selected"{{/is}}>{{this}}</option>
                                            {{/each}}
                                        {{/find}}
									{{/each}}
                                </optgroup>


                                <optgroup label="<?php esc_html_e('Custom Fields', '{{core-slug}}'); ?>">
                                {{#each @root/{{tab_slug}}_post_type}}

                                    {{#find @root/{{tab_slug}}/content_meta this}}
                                        {{#each custom_fields}}
                                        <option value="{{@key}}" {{#is ../../../field value=@key}}selected="selected"{{/is}}>{{this}}</option>
                                        {{/each}}
                                    {{/find}}
                                {{/each}}
                                </optgroup>


									
								{{/if}}

							</select>
							<?php /*
							<select name="{{:name}}[type]" style="margin-right: 0px; width: 113px;">
								
								<option value="" {{#unless type}}selected="selected"{{/unless}}><?php echo __('Auto Type', '{{core-slug}}'); ?></option>

								<option value="numeric" {{#is type value="numeric"}}selected="selected"{{/is}}>NUMERIC</option>
								<option value="date" {{#is type value="date"}}selected="selected"{{/is}}>DATE</option>
								<option value="datetime" {{#is type value="datetime"}}selected="selected"{{/is}}>DATETIME</option>
								<option value="time" {{#is type value="time"}}selected="selected"{{/is}}>TIME</option>
								<option value="decimal" {{#is type value="decimal"}}selected="selected"{{/is}}>DECIMAL</option>
								<option value="signed" {{#is type value="signed"}}selected="selected"{{/is}}>SIGNED</option>
								<option value="unsigned" {{#is type value="unsigned"}}selected="selected"{{/is}}>UNSIGNED</option>

							</select>	*/ ?>					
							<select name="{{:name}}[compare]" style="margin-right: 0px; width: 113px; text-align: center;">

								<option value="is" {{#is compare value="is"}}selected="selected"{{/is}}>=</option>
								<option value="isnot" {{#is compare value="isnot"}}selected="selected"{{/is}}>!=</option>
								<option value="isin" {{#is compare value="isin"}}selected="selected"{{/is}}>IN</option>
								<option value="isnotin" {{#is compare value="isnotin"}}selected="selected"{{/is}}>NOT IN</option>
								<option value="greater" {{#is compare value="greater"}}selected="selected"{{/is}}>&gt;</option>
								<option value="greatereq" {{#is compare value="greatereq"}}selected="selected"{{/is}}>&gt;=</option>
								<option value="smaller" {{#is compare value="smaller"}}selected="selected"{{/is}}>&lt;</option>
								<option value="smallereq" {{#is compare value="smallereq"}}selected="selected"{{/is}}>&lt;=</option>
								<!--<option value="startswith" {{#is compare value="startswith"}}selected="selected"{{/is}}>=</option>
								<option value="endswith" {{#is compare value="endswith"}}selected="selected"{{/is}}>=</option>-->
								<option value="contains" {{#is compare value="contains"}}selected="selected"{{/is}}>CONTAINS</option>
							</select>
							<div style="position: relative; display: inline-block;">
								<input type="text" class="magic-tag-enabled" name="{{:name}}[value]" value="{{value}}" style="position: relative; top: 2px; width: 250px;">
							</div>				
							{{#unless @first}}
								<button type="button" class="button button-small" style="padding: 0px; margin: 5px 0px; float: right;" data-remove-parent=".{{core-slug}}-filter-line"><span class="dashicons dashicons-no-alt" style="padding: 0px; margin: 0px; line-height: 23px;"></span></button>
							{{/unless}}
						</div>


					{{/each}}


					<div class="clear"></div>
					<button type="button" class="wp-baldrick button button-small" {{#unless line}}data-autoload="true"{{/unless}} style="position: absolute; bottom: 9px;" data-request="{{core_prefix}}_get_default_setting" data-group="{{_id}}" data-add-node="{{_node_point}}.line"><?php esc_html_e('Add Filter Line', '{{core-slug}}'); ?></button>
				</div>

				<button type="button" class="button button-small" style="padding: 0px; margin: 3px 0px; position: absolute; left: 14px; bottom: 6px;" data-remove-parent=".{{core-slug}}-filter-group"><span class="dashicons dashicons-no-alt" style="padding: 0px; margin: 0px; line-height: 23px;"></span></button>

			</div>

		{{/each}}
		<br>
		<hr>
		<button class="button wp-baldrick" data-request="{{core_prefix}}_get_default_setting" data-add-node="{{tab_slug}}.filter"><?php echo __('Add Filter Group', '{{core-slug}}'); ?></button>



	{{/if}} 

{{/if}}
</div>
