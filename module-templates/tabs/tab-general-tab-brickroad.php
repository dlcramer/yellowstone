	<li class="{{#is _current_tab value="#{{core-slug}}-panel-general"}}active {{/is}}{{core-slug}}-nav-tab">
		<a href="#{{core-slug}}-panel-general">
			<?php esc_html_e('{{module_general_name}}', '{{core-slug}}') ; ?>
		</a>
	</li>
