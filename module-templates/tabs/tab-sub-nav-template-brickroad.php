		<li class="{{#is _current_tab value="#{{core-slug}}-panel-{{tab_slug}}"}}active {{/is}}{{core-slug}}-nav-tab">
			<a href="#{{core-slug}}-panel-{{tab_slug}}"
				><?php esc_html_e('{{tab_name}}', '{{core-slug}}') ; ?>
			</a>
		</li>
