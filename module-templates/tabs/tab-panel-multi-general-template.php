	<div id="{{core-slug}}-panel-general" class="{{core-slug}}-editor-panel" {{#if _current_tab}}{{#is _current_tab value="#{{core-slug}}-panel-general"}}{{else}} style="display:none;" {{/is}}{{/if}}>
		<h4>
			<?php esc_html_e( '{{module_general_description}}', '{{core-slug}}' ); ?>
			<small class="description">
				<?php esc_html_e( '{{module_general_name}}', '{{core-slug}}' ); ?>
			</small>
		</h4>
		<?php
			/**
			 * Include general settings template
			 */
			include {{CORE_PREFIX}}_PATH . 'includes/templates/general-settings.php';
		?>
	</div>
