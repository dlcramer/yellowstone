<?php
/**
 * @author    David Cramer <david@digilab.co.za>
 * @license   GPL-2.0+
 * @link
 * @copyright 2014 David Cramer <david@digilab.co.za>
 */
	
$post_types = get_post_types( array( 'public' => true ), 'objects' );

?>
		<div class="{{core-slug}}-config-group">
			<label for="{{module_tab_field_id}}">
				<?php esc_html_e( '{{module_tab_field_name}}', '{{core-slug}}' ); ?>
			</label>

			<select style="width:395px;" placeholder="<?php esc_html_e( 'Select Post Types', '{{core-slug}}' ); ?>" name="{{module_tab_field_input_name}}" data-live-sync="true" id="{{module_tab_field_id}}" {{module_tab_field_required}}>
			<?php
				foreach($post_types as $post_type){
					echo '<option value="' . $post_type->name . '" {{#if {{module_tab_field_value}} value="' . $post_type->name . '"}}selected="selected"{{/if}}>' . $post_type->label . '</option>';
				}
			?>
			</select>

			{{module_tab_field_description}}
		</div>

		{{#script}}
			jQuery( function($){
				$("#{{module_tab_field_id}}").select2();
			});
		{{/script}}
