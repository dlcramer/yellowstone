<?php
	// Element template for {{tab_item_title}}
?>
{{#script type="text/html" id="{{tab_slug}}_{{tab_item_slug}}_item"}}

	<div class="node-wrapper {{core-slug}}-card-item" style="min-height: 52px;display: block; width: 100%; padding-left: 20px; box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.04), 20px 0px 0px rgb(248, 248, 248) inset;" id="element_\{{id}}">
		<span style="color:#a1a1a1;" class="dashicons {{core-slug}}-card-icon"></span>
		<div class="{{core-slug}}-card-content" style="min-height: 52px;">
			<input id="\{{id}}" type="hidden" value="\{{json data}}">
			<h4>\{{data/name}}</h4>

			
			<!-- Custom fields and stuff here -->


		</div>
		<span data-data="\{{id}}" data-fragment="\{{fragment}}" class="element-item-edit" data-module="{{tab_item_slug}}" style="padding: 0px; margin: 3px 0px; position: absolute; left: 1px; top: -2px;"><span class="dashicons dashicons-admin-generic" style="padding: 0px; margin: 0px; line-height: 23px; font-size:13px;"></span></span>

		<span style="padding: 0px; margin: 3px 0px; position: absolute; left: 0px; bottom: -2px;" data-confirm="{{tab_delete_confirm}}" data-remove-parent=".node-wrapper"><span class="dashicons dashicons-no-alt" style="padding: 0px; margin: 0px; line-height: 23px; font-size:13px;"></span></span>

	</div>

{{/script}}

