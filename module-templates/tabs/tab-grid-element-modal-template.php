<?php
	// Element modal template for {{tab_item_title}}
?>
{{#script type="text/html" id="{{tab_slug}}_{{tab_item_slug}}_modal_form"}}
	
	<input type="hidden" name="_{{tab_item_slug}}" value="1">
	<div class="{{core-slug}}-config-group">
		<label for="modal_name">
			<?php esc_html_e( 'Name', '{{core-slug}}' ); ?>
		</label>
		<input id="modal_name" type="text" class="regular-text" name="name" value="\{{name}}" required>
	</div>
	
	<!-- Custom fields and stuff here -->

{{/script}}

