
		<div class="{{core-slug}}-config-group">
			<label for="{{module_tab_field_id}}">
				<?php esc_html_e( '{{module_tab_field_name}}', '{{core-slug}}' ); ?>
			</label>
			<textarea id="{{module_tab_field_id}}" name="{{module_tab_field_input_name}}" data-mode="text/html" style="width: 373px; height: 202px;" {{module_tab_field_required}}>{{{{module_tab_field_value}}}}</textarea>
			{{module_tab_field_description}}
		</div>
		<br>
		{{#script}}

			var this_editor = {{core_prefix}}_init_editor('{{module_tab_field_id}}');
			setTimeout( function(){
				this_editor.refresh();
				this_editor.focus();
			}, 100 );

		{{/script}}