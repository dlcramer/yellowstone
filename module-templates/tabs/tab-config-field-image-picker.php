
		<div class="{{core-slug}}-config-group">
			<label for="{{core-slug}}-{{module_tab_field_id}}">
				<?php esc_html_e( '{{module_tab_field_name}}', '{{core-slug}}' ); ?>
			</label>
			<div class="image-picker-content" style="margin-bottom: 12px;margin-left: 185px;margin-top: -12px;width: 350px;">
				<label for="{{core-slug}}-{{module_tab_field_id}}" class="image-picker-side-bar {{#unless {{module_tab_field_value}}/selection/thumbnail}}dashicons dashicons-format-image{{/unless}}" style="{{#if {{module_tab_field_value}}/selection/thumbnail}}background: url('{{{{module_tab_field_value}}/selection/thumbnail}}') no-repeat scroll center center / cover  transparent;{{/if}} float: left; margin: 0 10px 0 0; border-radius: 6px; overflow: hidden; height: 84px; width: 84px; font-size: 85px; color: #d7d7d7; border: 1px solid #d7d7d7;"></label>
				<div class="image-picker-main-content">

					<div>
						<label for="{{core-slug}}-{{module_tab_field_id}}-size" style="display: block; padding: 0px 4px;">
							<?php esc_html_e( 'Image Size', '{{core-slug}}' ); ?>
						</label>
						<select id="{{core-slug}}-{{module_tab_field_id}}-size" name="{{module_tab_field_input_name}}[size]" class="image-picker-sizer" style="width: 180px; margin-bottom: 10px;" {{#unless {{module_tab_field_value}}/selection/id}}disabled="disabled"{{/unless}}>
							{{#each {{module_tab_field_value}}/selection/sizes}}
								<option value="{{@key}}" {{#is ../{{module_tab_field_value}}/size value=@key}}selected="selected"{{/is}}>{{@key}} - {{width}} x {{height}}</option>
							{{/each}}
						</select>
					</div>


					<button id="{{core-slug}}-{{module_tab_field_id}}" style="margin-right:8px; {{#unless {{module_tab_field_value}}/selection/id}}width: 180px;{{/unless}}" class="button caldera-image-picker" data-target="#{{module_tab_field_id}}_selection" data-title="<?php echo __('Select Image', '{{core-slug}}'); ?>" data-button="<?php echo __('Use Image', '{{core-slug}}'); ?>" type="button">
						<?php echo __('Select Image', '{{core-slug}}'); ?>
					</button>
					{{#if {{module_tab_field_value}}/selection/id}}<button class="button button-primary image-picker-button caldera-image-remover" data-target="#{{module_tab_field_id}}_selection" type="button">
						<?php echo __('Remove', '{{core-slug}}'); ?>
					</button>{{/if}}
				</div>					
			</div>
			{{module_tab_field_description}}
			<input id="{{module_tab_field_id}}_selection" name="{{module_tab_field_input_name}}[selection]" data-live-sync="true" type="hidden" value="{{json {{module_tab_field_value}}/selection}}">
		</div>
