
	<textarea id="{{tab_slug}}-code-editor" class="{{core-slug}}-code-editor" data-mode="{{tab_mode}}" name="{{tab_slug}}[code]">{{{{tab_slug}}/code}}</textarea>
	
	{{#is _current_tab value="#{{core-slug}}-panel-{{tab_slug}}"}}
		{{#script}}
		{{core_prefix}}_init_editor('{{tab_slug}}-code-editor');
		{{/script}}
	{{/is}}
