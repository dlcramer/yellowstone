<button class="button add-grid-row" data-panel="{{tab_slug}}" type="button"><?php _e( 'Add Row', '{{core-slug}}' ); ?></button>
<div class="caldera-grid {{tab_slug}}-grid" data-panel="{{tab_slug}}" data-confirm="<?php echo esc_attr( __( '{{tab_delete_confirm}}', '{{core-slug}}' ) ); ?>">

{{tab_grid_elements}}

	{{#each {{tab_slug}}_json}}
	<div class="row outer-row {{tab_slug}}-grid-row">
		{{#each this}}	
		<div class="col-xs-{{size}}">
			<div class="column-resize-handle {{tab_slug}}-resize">
				<span class="dashicons dashicons-minus {{tab_slug}}-minus"></span>			
			</div>
			<div class="row-toolbar">
				<span class="dashicons dashicons-plus {{tab_slug}}-plus"></span>
				<span class="dashicons dashicons-no {{tab_slug}}-no" data-confirm="<?php echo esc_attr( __( '{{tab_delete_confirm}}', '{{core-slug}}' ) ); ?>"></span>
			</div>
			<div class="lower-body column-body" id="{{id}}">
				<div class="lower-body-rows">
					{{#each row}}
					<div class="row inner-row {{tab_slug}}-grid-row">
						{{#each this}}	
						<div class="col-xs-{{size}}">
							<div class="column-resize-handle {{tab_slug}}-resize">
								<span class="dashicons dashicons-minus {{tab_slug}}-minus"></span>			
							</div>
							<div class="row-toolbar">
								<span class="dashicons dashicons-plus {{tab_slug}}-plus"></span>
								<span class="dashicons dashicons-no {{tab_slug}}-no" data-confirm="<?php echo esc_attr( __( '{{tab_delete_confirm}}', '{{core-slug}}' ) ); ?>"></span>
							</div>
							<div class="upper-body column-body" id="{{id}}">
								{{#each item}}

									{{tab_grid_element_layout_templates}}

								{{/each}}
							</div>
							<div class="row-toolbar row-column-action">
								<span class="dashicons element-item-insert dashicons-plus-alt" data-fragment="{{id}}">

								</span>
							</div>
						</div>		
						{{/each}}
					</div>	
					{{/each}}
				</div>
				{{#each item}}

				
					{{tab_grid_element_layout_templates}}


				{{/each}}
			</div>
			<div class="row-toolbar row-column-action">
				<span class="dashicons element-item-insert dashicons-plus-alt" data-fragment="{{id}}"></span>
			</div>
			<!-- <span class="side-split dashicons dashicons-plus" data-column="{{id}}" data-panel="{{tab_slug}}"></span> -->
		</div>		
		{{/each}}
	</div>	
	{{/each}}

</div>
<input type="hidden" id="{{tab_slug}}-input" name="{{tab_slug}}" value="{{json {{tab_slug}}}}">
<input type="hidden" id="{{tab_slug}}-input-json" name="{{tab_slug}}_json" value="{{json {{tab_slug}}_json}}">

{{tab_grid_element_modal_templates}}


{{tab_grid_element_templates}}



<!-- element types selector modal -->
{{#script type="text/html" id="element_selector_modal_form"}}

	\{{#each element}}
	<div class="{{core-slug}}-card-item" style="width: 100%; height: 32px; min-height: 32px; padding-left: 32px; margin:6px 0; display:block;">
		<span class="dashicons dashicons-plus-alt {{core-slug}}-card-icon" style="width: 32px; margin-left: -34px; line-height: 32px; text-indent: 3px; font-size: 22px; background:#efefef;"></span>
		<div class="{{core-slug}}-card-content" style="min-height: 30px;">
			<button type="button" class="button element-item-insert button-small" data-module="\{{@key}}" data-fragment="\{{../fragment}}" style="float: right; margin: -3px -7px 0px 0px;">
				<?php esc_html_e('Use Element', '{{core-slug}}'); ?>
			</button>
			<h4>\{{this}}</h4>
		</div>
	</div>
	\{{/each}}

{{/script}}



<?php
// Helpers for titles & extras
?>
<span id="modal_element_selector" data-modal-height="{{tab_element_selector_height}}" data-modal-width="380" data-modal-title="<?php echo esc_attr( __('Select Element', '{{core-slug}}') ); ?>" class="wp-modals" data-request="grid_modal_get_elements" data-modal="{{tab_slug}}" data-event="activate" data-template="#element_selector_modal_form"></span>
