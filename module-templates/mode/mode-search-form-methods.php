
	/**
	 * Loads connection and returns the tables fields via AJAX
	 *
	 * @since {{core_version}}
	 *
	 * @uses 'wp_ajax_{{core_slug}}_load_search_form_fields' action
	 */
	public function load_search_form_fields(){

		$forms = get_option( '_caldera_forms' );
		if( isset( $forms[ $_POST['_value'] ] ) ){
			$form = get_option( $forms[ $_POST['_value'] ]['ID'] );

			$new_field_binding = array();
			if( !empty( $form['fields'] ) ){
				foreach( $form['fields'] as $field_id=>$field ){
					if( $field['type'] == 'button' || $field['type'] == 'html' ){
						continue;
					}

					$new_field_binding[$field_id]['slug'] = $field['slug'];
					if( isset( $field['config']['option'] ) ){
						$new_field_binding[$field_id]['option'] = true;
					}
				}
			}

			wp_send_json( $new_field_binding );

		}

		wp_send_json_error( __('Invalid form selection or form removed.', '{{core-slug}}') );

	}
