		<div class="{{core-slug}}-config-group">
			<label for="{{core_item}}-name">
				<?php esc_html_e( '{{Core_Item}} Name', '{{core-slug}}' ); ?>
			</label>
			<input type="text" name="name" value="{{name}}" data-sync="#{{core_item}}-name-title" id="{{core_item}}-name" required>
		</div>
		<div class="{{core-slug}}-config-group">
			<label for="{{core_item}}-slug">
				<?php esc_html_e( '{{Core_Item}} Slug', '{{core-slug}}' ); ?>
			</label>
			<input type="text" name="slug" value="{{slug}}" data-format="slug" data-sync=".{{core-slug}}-subline" data-master="#{{core_item}}-name" id="{{core_item}}-slug" required>
		</div>
