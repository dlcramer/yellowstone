	<h1>
		<span id="{{core_item}}-name-title">{{name}}</span>
		<span class="{{core-slug}}-subline">{{slug}}</span>
		<span class="{{core-slug}}-nav-separator"></span>
		<span class="add-new-h2 wp-baldrick" data-action="{{core_prefix}}_save_config" data-load-element="#{{core-slug}}-save-indicator" data-callback="{{core_prefix}}_handle_save" data-before="{{core_prefix}}_get_config_object" >
			<?php esc_html_e('{{core_save_button}}', '{{core-slug}}') ; ?>
		</span>
		<span class="{{core-slug}}-nav-separator"></span>

	</h1>
	<span style="position: absolute; margin-left: -18px;" id="{{core-slug}}-save-indicator">
		<span style="float: none; margin: 16px 0px -5px 10px;" class="spinner"></span>
	</span>
