
		/**
		 * Software Licensing
		 */
		add_action( 'admin_init', function( ) {
			$plugin = array(
				'name'		=>	'{{Core_Name}}',
				'slug'		=>	'{{core-slug}}',
				'url'		=>	'{{license_url}}',
				'version'	=>	{{CORE_PREFIX}}_VER,
				'key_store'	=>  '{{core_prefix}}_license',
				'file'		=>  {{CORE_PREFIX}}_CORE
			);

			new \calderawp\licensing_helper\licensing( $plugin );

		}, 0 );
