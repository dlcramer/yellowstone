	<h1 class="{{core-slug}}-main-title">
		<?php esc_html_e( '{{Core_Name}}', '{{core-slug}}' ); ?>
		<span class="{{core-slug}}-version">
			<?php echo {{CORE_PREFIX}}_VER; ?>
		</span>
		<span class="{{core-slug}}-nav-separator"></span>
			
		<span class="add-new-h2 wp-baldrick" data-action="{{core_prefix}}_save_config" data-load-element="#{{core-slug}}-save-indicator" data-callback="{{core_prefix}}_handle_save" data-before="{{core_prefix}}_get_config_object" >
			<?php esc_html_e('{{core_save_button}}', '{{core-slug}}') ; ?>
		</span>

		<span class="{{core-slug}}-nav-separator"></span>

		<a class="add-new-h2" href="?page={{core_slug}}&amp;download=<?php echo ${{core_item}}[ 'id' ]; ?>&{{core-slug}}-export=<?php echo wp_create_nonce( '{{core-slug}}' ); ?>"><?php esc_html_e('Export', '{{core-slug}}'); ?></a>

		<span class="add-new-h2 wp-baldrick" data-modal="import-{{core_slug}}" data-modal-height="auto" data-modal-width="380" data-modal-buttons='<?php esc_html_e( 'Import {{Core_Item}}', '{{core-slug}}' ); ?>|{"id":"{{core_prefix}}_import_init", "data-request":"{{core_prefix}}_create_{{core_slug}}", "data-modal-autoclose" : "import-{{core_slug}}"}' data-modal-title="<?php esc_html_e('Import {{Core_Item}}', '{{core-slug}}') ; ?>" data-request="{{core_prefix}}_start_importer" data-template="#import-{{core_slug}}-form">
			<?php esc_html_e('Import', '{{core-slug}}') ; ?>
		</span>

		<span class="{{core-slug}}-nav-separator"></span>
		
		<span style="position: absolute; top: 5px;" id="{{core-slug}}-save-indicator">
			<span style="float: none; margin: 10px 0px -5px 10px;" class="spinner"></span>
		</span>

	</h1>

