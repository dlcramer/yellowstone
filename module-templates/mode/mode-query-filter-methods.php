	/**
	 * builds the WP_Query args
	 *
	 *
	 * @since 0.0.1
	 */
	 public function build_query_args( $filter_config ){



		// base query args
		$args = array(
			'post_type'  => $filter_config['post_type'],
		);

		// params to query array
		if( !empty( $filter_config['advanced']['limit'] ) && $filter_config['advanced']['limit'] > 0 ){
			$args['posts_per_page'] = $filter_config['advanced']['limit'];
		}else{
			$args['posts_per_page'] = -1;
		}


		// ordering
		if( !empty( $filter_config['order'] ) ){
			foreach( $filter_config['order'] as $order ){
				$args['orderby'][ $order['orderby'] ] = $order['order'];
			}

		}


		// filters - 
		if( !empty( $filter_config['filter'] ) ){
			
			$meta_query = self::build_meta_query( $filter_config['filter'] );
			if( !empty( $meta_query ) ){
				$args['meta_query'] = $meta_query;
			}

			$tax_query = self::build_tax_query( $filter_config['filter'] );
			if( !empty( $tax_query ) ){
				$args['tax_query'] = $tax_query;
			}

		}

		// return new stuff!
		return $args;

	 }
	

	/**
	 * Build a meta query array ( Need to move this to its own class later )
	 *
	 * @since 0.0.1
	 *
	 * @param array $meta_fields
	 *
	 * @return    meta_query array
	 */
	static public function build_meta_query( $meta_fields ) {

		$comps = array(
			"is"			=>	'=',
			"isnot"			=>	'!=',
			"isin"			=>	'IN',
			"isnotin"		=>	'NOT IN',
			"greater"		=>	'>',
			"smaller"		=>	'<',
			"greatereq"		=>	'>=',
			"smallereq"		=>	'<=',				
			"startswith"	=>	'LIKE',
			"endswith"		=>	'LIKE',
			"contains"		=>	'LIKE',
		);


		$meta_query = array();

		foreach( $meta_fields as $meta_lines ){

			$meta_group = array();
			foreach( $meta_lines['line'] as $meta_line ){
				if( false !== strpos( $meta_line['field'], 'custom_field.' ) ){
					$field = substr($meta_line['field'], 13);
					$value = $meta_line['value'];
					$meta_args = array(
						'key'		=> $field,
						'value'		=> $value,
						'compare'	=> $comps[ $meta_line['compare'] ],
					);
					if( !empty( $meta_line['type'] ) ){
						// type assign
						$meta_args['type'] = strtoupper( $meta_line['type'] );
					}else{

						// Auto type determination
						if( is_string( $value ) && date('Y-m-d H:i:s', strtotime( $value ) ) === $value ){
							// datetime
							$meta_args['type'] = 'DATETIME';

						}elseif( is_string( $value ) && date('Y-m-d', strtotime( $value ) ) === $value ){
							// date
							$meta_args['type'] = 'DATE';
						}elseif( ( is_float( $value ) || is_string( $value ) ) && false !== strpos( $value, '.' ) && false !== floatval( $value ) ){
							// decimal
							$meta_args['type'] = 'DECIMAL';
						}elseif( is_int( $value ) || ( false !== intval( $value ) && $value === (string) intval( $value ) ) ){
							// numeric
							$meta_args['type'] = 'NUMERIC';
						}
					}

					// add meta line to query
					$meta_group[] = $meta_args;
				}
			}
			
			if( empty( $meta_group ) ){
				continue;
			}
			/// lotsa groups?
			if( count( $meta_group ) > 1 ){
				$meta_group['relation'] = 'AND'; // more = ands
			}
			$meta_query[] = $meta_group;
		}
		// empty? bye then.
		if( empty( $meta_query ) ){
			return;
		}
		// more groups?
		if( count( $meta_query ) > 1 ){
			$meta_query['relation'] = 'OR'; // more = or ; need to make this inner groups at one stage... lazy ARGH!
			return $meta_query;

		}else{
			return $meta_query[0];
		}

		
	}

	/**
	 * Build a tax query array
	 *
	 * @since 0.0.1
	 *
	 * @param array $tax_query
	 *
	 * @return    tax_query array
	 */
	static public function build_tax_query( $tax_fields ) {

		$comps = array(
			"is"			=>	'=',
			"isnot"			=>	'!=',
			"isin"			=>	'IN',
			"isnotin"		=>	'NOT IN',
			"greater"		=>	'>',
			"smaller"		=>	'<',
			"greatereq"		=>	'>=',
			"smallereq"		=>	'<=',				
			"startswith"	=>	'LIKE',
			"endswith"		=>	'LIKE',
			"contains"		=>	'LIKE',
		);


		$tax_query = array();

		foreach( $tax_fields as $tax_lines ){

			$tax_group = array();
			foreach( $tax_lines['line'] as $tax_line ){
				if( false !== strpos( $tax_line['field'], 'taxonomy.' ) ){
					$taxonomy = explode('.', $tax_line['field'] );
					$terms = explode(',', $tax_line['value']);
					foreach( $terms as &$term ){
						$term = trim($term);
						if( is_numeric( $term ) ){
							$type = 'term_id';
						}else{
							$type = 'slug';
						}
					}
				
					$tax_group[] = array(
						'taxonomy'	=> $taxonomy[1],
						'field'		=> $type,
						'terms'		=> $terms,
						'operator'	=> $comps[ $tax_line['compare'] ],
					);					
				}
			}
			/// lotsa groups?
			if( count( $tax_group ) > 1 ){
				$tax_group['relation'] = 'AND'; // more = ands
				$tax_query[] = $tax_group;
			}elseif( count( $tax_group ) === 1 ){
				$tax_query[] = $tax_group[0];
			}
			
		}

		// more groups?
		if( count( $tax_query ) > 1 ){
			$tax_query['relation'] = 'OR'; // more = or ; need to make this inner groups at one stage... lazy ARGH!
			return $tax_query;

		}elseif( count( $tax_query ) === 1 ){
			return $tax_query[0];
		}else{
			return;
		}
		
	}
