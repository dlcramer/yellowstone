

	/**
	 * Insert shortcode media button
	 *
	 * @since {{core_version}}
	 *
	 * @uses 'media_buttons' action
	 */
	public function shortcode_insert_button(){
		global $post;
		if(!empty($post)){
			echo "<a id=\"{{core_item}}-insert\" title=\"".__('{{Core_Name}}','{{core-slug}}')."\" class=\"button {{core_item}}-insert-button\" href=\"#inst\" style=\"padding-left: 10px; box-shadow: 4px 0px 0px {{core_color}} inset;\">\n";
			echo __('{{Core_Name}}', '{{core-slug}}')."\n";
			echo "</a>\n";
		}

	}

	/**
	 * Insert shortcode modal template in post editor.
	 *
	 * @since {{core_version}}
	 *
	 * @uses 'admin_footer' action
	 */
	public static function add_shortcode_inserter(){
		
		$screen = get_current_screen();

		if( $screen->base === 'post'){
			include {{CORE_PREFIX}}_PATH . 'includes/insert-shortcode.php';
		}

	}

	/**
	 * Render Shortcode
	 *
	 * @since 0.0.1
	 */
	public function render_{{core_slug}}( $atts ){
		
		${{core_object}} = options::get_registry();
		if( empty( ${{core_object}} ) ){
			${{core_object}} = array();
		}

		if( empty( $atts['id'] ) && !empty( $atts['slug'] ) ){
			foreach( ${{core_object}} as ${{core_item}}_id => ${{core_item}} ){

				if( ${{core_item}}['slug'] === $atts['slug'] ){
					${{core_item}} = options::get_single( ${{core_item}}['id'] );
					break;
				}

			}

		}elseif( !empty( $atts['id'] ) ){
			${{core_item}} = options::get_single( $atts['id'] );
		}else{
			return;
		}

		if( empty( ${{core_item}} ) ){
			return;
		}

		$output = null;

		// do stuf and output
		var_dump( ${{core_item}} );

		return $output;

	}
