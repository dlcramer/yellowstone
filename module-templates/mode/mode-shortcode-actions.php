		// queue up the shortcode inserter
		add_action( 'media_buttons', array($this, 'shortcode_insert_button' ), 11 );

		// shortcode insterter js
		add_action( 'admin_footer', array( $this, 'add_shortcode_inserter'));

		//shortcode
		add_shortcode( '{{core_item}}', array( $this, 'render_{{core_slug}}') );
