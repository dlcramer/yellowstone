<?php
/**
 * Main edit interface for admin page.
 *
 * @package   {{Core_Class}}
 * @author    {{core_author_name}}
 * @license   GPL-2.0+
 * @link
 * @copyright 2015 {{core_author_name}}
 */

${{core_item}} = \calderawp\{{core_prefix}}\options::get_single( {{module_admin_loader}} );
// simplyfy creation
if( false === ${{core_item}} ){
	${{core_item}} = array(
		'id' => {{module_mode_edit_loader}}
	);
}
?>
<div class="wrap {{core-slug}}-{{core_theme_header}}main-canvas" id="{{core-slug}}-main-canvas">
	<span class="wp-baldrick spinner" style="float: none; display: block;" data-target="#{{core-slug}}-main-canvas" data-before="{{core_prefix}}_canvas_reset" data-callback="{{core_prefix}}_canvas_init" data-type="json" data-request="#{{core-slug}}-live-config" data-event="click" data-template="#main-ui-template" data-autoload="true"></span>
</div>

<div class="clear"></div>

<input type="hidden" class="clear" autocomplete="off" id="{{core-slug}}-live-config" style="width:100%;" value="<?php echo esc_attr( json_encode(${{core_item}}) ); ?>">

<script type="text/html" id="main-ui-template">
	<?php
		/**
		 * Include main UI
		 */
		include {{CORE_PREFIX}}_PATH . 'includes/templates/{{module_admin_key}}-main-ui.php';
	?>	
</script>

{{module_mode_edit_footer}}