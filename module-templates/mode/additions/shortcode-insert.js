
jQuery(function($){


	$('body').on('click', '#{{core_item}}-insert', function(e){

		e.preventDefault();
		var modal = $('.{{core-slug}}-insert-modal'),
			clicked = $(this);

		if( clicked.data('selected') ){
			clicked.data('selected', null);
		}


		modal.fadeIn(100);

	});

	$('body').on('click', '.{{core-slug}}-modal-closer', function(e){
		e.preventDefault();
		var modal = $('.{{core-slug}}-insert-modal');
		modal.fadeOut(100);		
	});

	$('body').on('click', '.{{core-slug}}-shortcode-insert', function(e){
	 	
	 	e.preventDefault();
	 	var {{core_item}} = $('.selected-{{core_item}}-shortcode:checked'),code;

	 	if(!{{core_item}}.length){
	 		return;
	 	}

	 	code = '[{{core_item}} slug="' + {{core_item}}.val() + '"]';

	 	{{core_item}}.prop('checked', false);	 	
		window.send_to_editor(code);
		$('.{{core-slug}}-modal-closer').trigger('click');

	});

});//
