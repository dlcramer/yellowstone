<?php
/**
 *The shortcode insert modal.
 *
 * @package   {{Core_Class}}
 * @author    {{core_author_name}}
 * @license   GPL-2.0+
 * @link
 * @copyright 2015 {{core_author_name}}
 */
?>

<div class="{{core-slug}}-backdrop {{core-slug}}-insert-modal" style="display: none;"></div>
<div id="{{core_item}}_shortcode_modal" class="{{core-slug}}-modal-wrap {{core-slug}}-insert-modal" style="display: none; width: 600px; max-height: 500px; margin-left: -300px;">
	<div class="{{core-slug}}-modal-title" id="{{core_item}}_shortcode_modalTitle" style="display: block;">
		<a href="#close" class="{{core-slug}}-modal-closer" data-dismiss="modal" aria-hidden="true" id="{{core_item}}_shortcode_modalCloser">×</a>
		<h3 class="modal-label" id="{{core_item}}_shortcode_modalLable"><?php echo __('Insert {{Core_Item}}', '{{core-slug}}'); ?></h3>
	</div>
	<div class="{{core-slug}}-modal-body none" id="{{core_item}}_shortcode_modalBody">
		<div class="modal-body">


		<!-- Ye, you're gonna have to do this part ;) -->


		</div>
	</div>
	<div class="{{core-slug}}-modal-footer" id="{{core_item}}_shortcode_modalFooter" style="display: block;">
		<p class="modal-label-subtitle" style="text-align: right;">
			<button class="button {{core-slug}}-shortcode-insert" style="margin:5px 25px 0 15px;">
				<?php echo __('Insert Selected', '{{core-slug}}'); ?>
			</button>
		</p>
	</div>
</div>
