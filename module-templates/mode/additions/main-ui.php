<div class="{{core-slug}}-main-header{{core_theme_header}}">
	{{module_mode_title}}
	{{module_mode_main_nav}}
	<span class="wp-baldrick" id="{{core-slug}}-field-sync" data-event="refresh" data-target="#{{core-slug}}-main-canvas" data-before="{{core_prefix}}_canvas_reset" data-callback="{{core_prefix}}_canvas_init" data-type="json" data-request="#{{core-slug}}-live-config" data-template="#main-ui-template"></span>
</div>
{{module_tab_sub_nav_base}}
<form class="{{core_theme_header}}-main-form {{module_tab_has_sub_class}}" id="{{core-slug}}-main-form" action="?page={{core_slug}}-{{admin_prefix}}" method="POST">
	<?php wp_nonce_field( '{{core-slug}}', '{{core-slug}}-setup' ); ?>
	{{module_mode_form_start}}

	<input type="hidden" value="{{_current_tab}}" name="_current_tab" id="{{core-slug}}-active-tab">

	{{module_tab_panel}}

	{{module_mode_single_save}}	

</form>

{{#unless _current_tab}}
	{{#script}}
		jQuery(function($){
			$('.{{core-slug}}-nav-tab').first().trigger('click').find('a').trigger('click');
		});
	{{/script}}
{{/unless}}
