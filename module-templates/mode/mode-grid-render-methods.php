	/**
	 * Renders a Grid Layout
	 *
	 * @since {{core_version}}
	 *
	 */
	public function render_{{core_slug}}_grid( $grid ){
		
		$global_break = 'md';

		$out = '<div class="{{core-slug}}-grid">';
			foreach( $grid as $row_inx=>$row ){
				$out .= '<div class="row">';
					foreach( $row as $column_inx=>$column ){
						$break_point = $global_break;
						if( !empty( $column['break_point'] ) ){
							$break_point = $column['break_point'];
						}
						$out .= '<div class="col-' . $break_point . '-' . $column['size'] . '">';

							foreach( $column['item'] as $item_inx=>$item ){

								if( isset(  $item['code'] ) ){
									$out .= $item['code'];
								}else{
									// your handler here.
								}
							
							}

						$out .= '</div>';
					}
					$out .= '</div>';
				}
			$out .= '</div>';
		$out .= '</div>';

		return $out;
	}