<script type="text/javascript">
	function {{core_prefix}}_start_importer(){
		return {};
	}
	function {{core_prefix}}_create_{{core_slug}}(){
		jQuery('#{{core-slug}}-field-sync').trigger('refresh');
		jQuery('#{{core_prefix}}-save-button').trigger('click');
	}
</script>
<script type="text/html" id="import-{{core_slug}}-form">
	<div class="import-tester-config-group">
		<input id="new-{{core-slug}}-import-file" type="file" class="regular-text">
		<input id="new-{{core_item}}-import" value="" name="import" type="hidden">
	</div>
	{{#script}}
		jQuery( function($){

			$('#{{core_prefix}}_import_init').prop('disabled', true).addClass('disabled');

			$('#new-{{core-slug}}-import-file').on('change', function(){
				$('#{{core_prefix}}_import_init').prop('disabled', true).addClass('disabled');
				var input = $(this),
					f = this.files[0],
				contents;

				if (f) {
					var r = new FileReader();
					r.onload = function(e) { 
						contents = e.target.result;
						var data;
						 try{ 
						 	data = JSON.parse( contents );
						 } catch(e){};
						 
						 if( !data || ! data['{{core-slug}}-setup'] ){
						 	alert("<?php echo esc_attr( __('Not a valid {{Core_Item}} export file.', '{{core-slug}}') ); ?>");
						 	input[0].value = null;
							return false;
						 }

						$('#{{core-slug}}-live-config').val( contents );						
						$('#{{core_prefix}}_import_init').prop('disabled', false).removeClass('disabled');
					}
					if( f.type !== 'application/json' ){
						alert("<?php echo esc_attr( __('Not a valid {{Core_Item}} export file.', '{{core-slug}}') ); ?>");
						this.value = null;
						return false;
					}
					r.readAsText(f);
				} else { 
					alert("Failed to load file");
					return false;
				}
			});

		});
	{{/script}}
</script>
