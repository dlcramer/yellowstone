$this->plugin_screen_hook_suffix['{{core_slug}}'] =  add_menu_page(
	__( '{{Core_Name}}', $this->plugin_slug ),
	__( '{{Core_Short_Name}}', $this->plugin_slug )
	, 'manage_options', '{{core_slug}}',
	array( $this, 'create_admin_page' ),
	'{{core_icon}}'
);
