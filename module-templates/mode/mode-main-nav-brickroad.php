	<div class="updated_notice_box">
		<?php esc_html_e( 'Updated Successfully', '{{core-slug}}' ); ?>
	</div>
	<div class="error_notice_box">
		<?php esc_html_e( 'Could not save changes.', '{{core-slug}}' ); ?>
	</div>
	<ul class="{{core-slug}}-header-tabs {{core-slug}}-nav-tabs">
				
		{{module_tab_nav}}
				
	</ul>
