		// create new
		add_action( 'wp_ajax_{{core_prefix}}_create_{{core_item}}', array( $this, 'create_new_{{core_item}}') );

		// delete
		add_action( 'wp_ajax_{{core_prefix}}_delete_{{core_item}}', array( $this, 'delete_{{core_item}}') );
