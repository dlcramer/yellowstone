			${{core_item}}s = get_option( '_{{core_object}}_registry' );

			if( isset( $config['id'] ) && !empty( ${{core_item}}s[ $config['id'] ] ) ){
				$updated_registery = array(
					'id'	=>	$config['id'],
					'name'	=>	$config['name'],
					'slug'	=>	$config['slug']
				);
				// add search form to registery
				if( !empty( $config['search_form'] ) ){
					$updated_registery['search_form'] = $config['search_form'];
				}
				
				${{core_item}}s[$config['id']] = $updated_registery;
				update_option( '_{{core_object}}_registry', ${{core_item}}s );
			}
			update_option( $config['id'], $config );
