			wp_enqueue_style( '{{core_slug}}-core-style', {{CORE_PREFIX}}_URL . '/assets/css/styles.css' );
			wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_style( '{{core_slug}}-baldrick-modals', {{CORE_PREFIX}}_URL . '/assets/css/modals.css' );
			wp_enqueue_script( '{{core_slug}}-handlebars', {{CORE_PREFIX}}_URL . '/assets/js/handlebars.js' );
			wp_enqueue_script( '{{core_slug}}-wp-baldrick', {{CORE_PREFIX}}_URL . '/assets/js/wp-baldrick.js', array( 'jquery' ) , false, true );
			wp_enqueue_script( 'jquery-ui-autocomplete' );
			wp_enqueue_script( 'jquery-ui-sortable' );
			wp_enqueue_script( 'wp-color-picker' );
						
			if( !empty( $_GET[ 'edit' ] ) ){
				wp_enqueue_style( '{{core_slug}}-codemirror-style', {{CORE_PREFIX}}_URL . '/assets/css/codemirror.css' );
				wp_enqueue_script( '{{core_slug}}-codemirror-script', {{CORE_PREFIX}}_URL . '/assets/js/codemirror.js', array( 'jquery' ) , false );
			}

			wp_enqueue_script( '{{core_slug}}-core-script', {{CORE_PREFIX}}_URL . '/assets/js/scripts.js', array( '{{core_slug}}-wp-baldrick' ) , false );
